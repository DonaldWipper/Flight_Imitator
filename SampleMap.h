#ifndef SAMPLEMAP_H
#define SAMPLEMAP_H


#include <QtGui>
#include <QGLWidget>




// ======================================================================
class OGLMap : public QGLWidget{
    Q_OBJECT
private:

protected:
    virtual void   initializeGL   (                       );
    //virtual void   resizeGL       (int nWidth, int nHeight);
    virtual void   paintGL        (                       );

    //virtual void   timerEvent(QTimerEvent *);

signals:

public slots:


public:


    int timer_id;
    void   resizeGL       (int nWidth, int nHeight);
    //void   keyPressEvent(QKeyEvent* pe);
    //void   mousePressEvent(QMouseEvent* pe        );
    /*void   mouseMoveEvent (QMouseEvent* pe        );
    void   mouseReleaseEvent(QMouseEvent*pe       );
    void wheelEvent(QWheelEvent* pe);*/



     OGLMap(QWidget* pwgt = 0);


};

//==========================================================

class SampleMapView: public QGraphicsView {
Q_OBJECT
public:
    SampleMapView(QGraphicsScene* pScene= NULL, QWidget* pwgt = 0);

public slots:



private:


public:
    OGLMap* mpGLMap;



protected:
   void drawBackground(QPainter *p, const QRectF &r);
   virtual void	resizeEvent ( QResizeEvent * event );

};



#endif // SAMPLEMAP_H
