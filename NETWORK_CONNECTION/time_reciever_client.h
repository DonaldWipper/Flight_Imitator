
#ifndef TIME_RECIEVER_CLIENT_H
#define TIME_RECIEVER_CLIENT_H

#include <boost/date_time/posix_time/posix_time.hpp>
#include "../FLIGHT_CALCULATION/AMZGO_network_exchange.h"
#include <QApplication>
#include <QWidget>
#include <QtGui>
#include <iostream>
#include <QTcpSocket>

using namespace std;


class MY_CLIENT: public QWidget {
Q_OBJECT
private:
    boost::posix_time::ptime rec_time; // �������� ����� �� �������

    // ����������� ���������	


    bool haveConnection;              // ���������� ������ ���������� � ��� ���� �� ����� ��� ���
    int Timer_id;                     // ������ ��� �������� �� �������� ����������

    QTcpSocket *m_tcp_Socket;
    quint32 m_Next_Block_Size; // ���� ������������ ����������

    QString Server_name;
    bool isVisible;



    QString Host;
    int Port;

signals:
    void send_time_signal(TimeInfo &CurTime);
    void send_change_zones_signal(InpInfo info);
    void restart();
protected:
    virtual void timerEvent(QTimerEvent *);
public:

    QTextEdit *info_connection;
    int Year;   //
    int Month;  //
    int Day;    //
    int msDay;  // ����� ����������� � ������ ���


    void startConnection();     // ������������� ����������
    void closeConnection();     // ��������� ����������
    void checkConnection();
    bool getStatus() const;     // ������ ����������

    MY_CLIENT(const QString& strHost,
              int nPort,
              QWidget* pwgt = 0,
              QTextEdit*  _info_connection = NULL) ;

private slots:
    void set_visible();
    void slotReadyRead   (                            );
    void slotError       (QAbstractSocket::SocketError);
    void slotSendToServer(                            );
    void slotConnected   (                            );
    void reconnect(int nPort, QString strHost);
};	
	
boost::posix_time::ptime getPtime(const TimeInfo &time);
#endif // 	

	
