#ifndef DATA_SENDER_SERVER_H
#define DATA_SENDER_SERVER_H

#include <QtGui>
#include <QTcpServer>
#include <QTcpSocket>
#include "../FLIGHT_CALCULATION/AMZGO_network_exchange.h"

/***********************************************************************
 *               ����� ������ ��� ������ �������                       *
 ***********************************************************************/




class DATA_SENDER_SERVER: public QWidget
{
Q_OBJECT
public:
    DATA_SENDER_SERVER(QWidget* pwgt=0,
                       QTextEdit *_info_sending = NULL);   //

    void start();                      // �������������
    void close();                      // ���������
    void setName(const QString &name); //


     QTextEdit *info_sending; // information about sending to client
signals:
    //void SocketMessage(QString msg);  // �������� ������ � ���������� � ������ ������

private:
    QTcpServer* m_ptcpServer;         //
    quint16     Server_Port;          // ����
    bool haveConnection;              // ���������� ������ ���������� � ��� ���� �� ����� ��� ���
    int Timer_id;                     // ������ ��� �������� �� �������� ����������
    QString client_name;              //


    std::vector<QTcpSocket*> connectedClients;



protected:
    virtual void timerEvent(QTimerEvent *); //

public slots:
    void setPort(const int& Port);    //
    void SendInformationToClient(const TimeInfo& Time,
                                 const std::vector<TargetParGeo> & df
                                 );
    void reconnect(int ServerPort);

private slots:
    virtual void slotNewConnection(); //
    void closeSocketConnection();     //
};































#endif // DATA_SENDER_SERVER_H
