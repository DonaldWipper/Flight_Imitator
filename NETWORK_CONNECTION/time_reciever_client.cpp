#ifndef CLIENT_NETWORK_CPP
#define CLIENT_NETWORK_CPP



#include "time_reciever_client.h"


boost::posix_time::ptime getPtime(const TimeInfo &time) {
        return boost::posix_time::ptime(boost::gregorian::date(time.Year,time.Month,time.Day),    boost::posix_time::time_duration(0,0,0)+boost::posix_time::milliseconds(time.msDay));
    }

    bool isZERO(const TimeInfo &time) {
        if((time.Year == 0) &&
           (time.Month == 0) &&
           (time.Day == 0) &&
           (time.msDay == 0)) {
           return true;
        }
        return false;
    }






MY_CLIENT::MY_CLIENT(const QString& strHost,
                   int            nPort,
                   QWidget*       pwgt /*=0*/,
                   QTextEdit* _info_connection
                  ) : QWidget(pwgt)
                    , m_Next_Block_Size(0)
{



     m_tcp_Socket = new QTcpSocket(this);
     m_tcp_Socket->connectToHost(strHost, nPort);

     Host = strHost;
     Port = nPort;

     //������������ ����������
     connect(m_tcp_Socket, SIGNAL(connected()), SLOT(slotConnected()));

    // ������ ������ ������ �� �������
    cout << connect(m_tcp_Socket, SIGNAL(readyRead()), SLOT(slotReadyRead())) << endl;


    // �� ������ ���� �� ���� ���-�� ��������������
    connect(m_tcp_Socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,         SLOT(slotError(QAbstractSocket::SocketError))
           );

    info_connection  = _info_connection;

    startConnection();

    //������ ��� �������� �����
    Timer_id = -1;

    info_connection->setReadOnly(true);
    haveConnection = false;
    Server_name = "";
    isVisible = true;
    //m_tcp_Socket->connectToHost(strHost, nPort);

}


void MY_CLIENT::set_visible() {
    info_connection->setVisible(!isVisible);
    isVisible = !isVisible;
    info_connection->update();
}



void MY_CLIENT::reconnect(int nPort, QString strHost) {


    closeConnection();


    Port = nPort;
    Host = strHost;
    //m_tcp_Socket = new QTcpSocket(this);
    m_tcp_Socket->connectToHost(Host, Port);
    startConnection();
    m_Next_Block_Size = 0;
    //������ ��� �������� �����
    Timer_id = -1;
    Server_name = "";
   //m_tcp_Socket->connectToHost(strHost, nPort);
}


//-----------------------------------------------------------------------------

void MY_CLIENT::startConnection() {
    Timer_id = startTimer(1000);
}

void MY_CLIENT::closeConnection()
{
    killTimer(Timer_id);
    Timer_id = -1;
    m_tcp_Socket->close();
    haveConnection = false;
}


//��� ����� ��������� �����������
void MY_CLIENT::checkConnection()
{



    if ((!haveConnection) && (m_tcp_Socket->state()!=QAbstractSocket::ConnectingState) ) // ���� ����� ��� � � ������ ������ �� �������� �� ���������� �� �������� �� ����������
       {

        if (Host.isEmpty() || (Port==-1))
           {

            info_connection->append(QString::fromLocal8Bit("������ � ������� ����� ������� �/��� ����� %1.").arg(Server_name ));
            return;
           }
        else
           {


            m_tcp_Socket->connectToHost(Host, Port); // �������� ��� �������(��� IP) � ����
            m_Next_Block_Size = 0;

           }
    } else {

    }
}


//-------------------------------------------------------------------

void MY_CLIENT::timerEvent(QTimerEvent *)
{
     checkConnection();
    // qDebug() << "�������� ����������� ";
}




void MY_CLIENT::slotReadyRead()
{
    QDataStream in(m_tcp_Socket);
    in.setVersion(QDataStream::Qt_4_7);

    for (;;) {
        if (!m_Next_Block_Size) {
            if (m_tcp_Socket->bytesAvailable() < sizeof(quint32)) {
               break;
            }
           in >> m_Next_Block_Size;
        }

       if (m_tcp_Socket->bytesAvailable() < m_Next_Block_Size) {
           break;
       }
       InpInfo info;
       in >> info;





       if(isZERO(info.T_inf)) {
          emit restart();
          emit send_time_signal(info.T_inf);
          info_connection->append(QString( "�������� ����� �� ������� ")+  "ZERO");


       } else {
           boost::posix_time::ptime curTime = getPtime(info.T_inf);
           QString time =  QString::fromStdString(boost::posix_time::to_simple_string(curTime.time_of_day()));;
           emit send_time_signal(info.T_inf);
           emit send_change_zones_signal(info);
           info_connection->append(QString( "�������� ����� �� ������� ")+  time);
       }
       m_Next_Block_Size = 0;
   }
   slotSendToServer();
}


void MY_CLIENT::slotSendToServer()
{

    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_7);
    out << quint32(0) << 1;
    out.device()->seek(0);

    out << quint32(arrBlock.size() - sizeof(quint32));

    m_tcp_Socket->write(arrBlock);

}

void MY_CLIENT::slotError(QAbstractSocket::SocketError err)
{
    QString strError =
        "������: " + (err == QAbstractSocket::HostNotFoundError ?
                     "���� �� ��� ������" :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "��������� ���� ��� ������" :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "��� ����������" :
                     QString(m_tcp_Socket->errorString())
                    );
   info_connection->append(strError);
}


void MY_CLIENT::slotConnected()
{
     haveConnection = true;
     info_connection->append("������ ������ ����������");
}
#endif // CLIENT_NETWORK_CPP
