#ifndef DATA_SENDER_SERVER_CPP
#define DATA_SENDER_SERVER_CPP

#include "data_sender_server.h"




//=========================================================================
//
//=========================================================================
DATA_SENDER_SERVER::DATA_SENDER_SERVER(QWidget* pwgt, QTextEdit *_info_sending) : QWidget(pwgt)
{
    //
    m_ptcpServer = new QTcpServer(this);
    Server_Port=-1;
    connect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    //
    haveConnection=false; // ����� ���

    // ������ ��� �������� �����
    Timer_id=-1;

   // client_name = "";              //

    //
    setLayout(new QVBoxLayout(this));

    info_sending  = _info_sending;
    info_sending->setReadOnly(true);

    //this->layout()->addWidget(client_name);
}

void DATA_SENDER_SERVER::reconnect(int ServerPort) {
    if(Server_Port == ServerPort) {
        return;
    }
    this->close();
    qDebug() << "���������������";
    connect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    haveConnection=false; // ����� ���
    // ������ ��� �������� �����
    Timer_id = -1;
    start();


}



//=========================================================================
void DATA_SENDER_SERVER::start()
{
    if ((Server_Port==-1))
       {
        //emit SocketMessage(QString::fromLocal8Bit("������ � ������� ����� %1.").arg(client_name));
        info_sending->append(QString::fromLocal8Bit("������ � ������� ����� %1.").arg(client_name));
        return;
       }

    if (!m_ptcpServer->listen(QHostAddress::Any, Server_Port))  // ������ �������
       {
        //QString mess=QString::fromLocal8Bit("������ ������� �������. ���������� ��������� %1:").arg(client_name) + m_ptcpServer->errorString();
        info_sending->append(QString::fromLocal8Bit("������ ������� �������. ���������� ��������� %1:").arg(client_name) + m_ptcpServer->errorString());
        //emit SocketMessage(mess);
        m_ptcpServer->close();

        return;
       }

    Timer_id=startTimer(1000);  // ��������� ����� ��� � ���
}

//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::close()
{
   killTimer(Timer_id);

   //
   for (std::vector<QTcpSocket*>::iterator it=connectedClients.begin(), it_end=connectedClients.end();it!=it_end;++it)
        delete (*it);

   m_ptcpServer->close();

   haveConnection=false;

//   disconnect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
   delete m_ptcpServer;

//   closeSocketConnection();

   m_ptcpServer = new QTcpServer(this);
   connect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

   update();
}

//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::setName(const QString &name)
{
    client_name=name;
}

//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::timerEvent(QTimerEvent *)
{
    update();
}

//--- slots ---------------------------------------------------------------
//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::setPort(const int& Port)
{
    Server_Port=Port;
}

//=========================================================================
//              ���������� ��� ���������� ������ �������
//=========================================================================
 void DATA_SENDER_SERVER::slotNewConnection()
{
    // ������������ ���������� � ���������� ����� ����������� �������� ����� ������������ ����������� � ��������
    QTcpSocket* connectedClient = m_ptcpServer->nextPendingConnection();

    //connect(connectedClient, SIGNAL(disconnected()), connectedClient, SLOT(deleteLater()));

    connect(connectedClient, SIGNAL(disconnected()), this, SLOT(closeSocketConnection()));

    connectedClients.push_back(connectedClient);

    // ������� � ������ �������
     info_sending->append(QString::fromLocal8Bit("���������� ������ ������� � %1 �������� �����������.").arg(client_name));
    //emit SocketMessage(QString::fromLocal8Bit("���������� � %1 �����������.").arg(client_name));

    haveConnection=true;

        update();
}

//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::closeSocketConnection()
{
    QTcpSocket* curClientSocket = (QTcpSocket*)sender();

    connectedClients.erase(std::remove(connectedClients.begin(),connectedClients.end(),curClientSocket),connectedClients.end());

    if (connectedClients.empty())
        haveConnection=false;

    update();
}

//=========================================================================
//
//=========================================================================
void DATA_SENDER_SERVER::SendInformationToClient(const TimeInfo& Time,
                                            const std::vector<TargetParGeo>& pTaktData)
{
    // �� ����� �� ����� ������ ����� ������� ������� ������ arrBlock

    QByteArray  arrBlock;
    // �� ��� ������ ������� out
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_7);

    // ���������� ��� ������ � ������ ���������� 0
    out << quint32(0);

    //--- ����� ---------------
    out << Time;

    uint Planes_count=pTaktData.size();

    if (Planes_count!=0)
       {
        out << Planes_count;

        for (std::vector<TargetParGeo>::const_iterator it=pTaktData.begin(), it_end=pTaktData.end();it!=it_end; ++it)
        {
            out << (*it);
        }
       }
    else
        out << Planes_count;


    info_sending->append(QString::fromLocal8Bit("���������� ���������, ����������� � ������ ������ ") + QString::number(Planes_count));
    //--- ����� ---------------

    // ���������� ��������� � ������ �����
    out.device()->seek(0);
    // ��������� ������ ����� � ���������� � ��� �����
    out << quint32(arrBlock.size() - sizeof(quint32));

    // � ���������� ���� ���� � �����
    for (std::vector<QTcpSocket*>::iterator it_sct=connectedClients.begin(), it_sct_end=connectedClients.end();it_sct!=it_sct_end;++it_sct)
         (*it_sct)->write(arrBlock);

      // ������� � ������ �������
    info_sending->append(QString::fromLocal8Bit("������ � ����������� ��������� ���������� %1.").arg(client_name));
    //emit SocketMessage(QString::fromLocal8Bit("������ ���������� %1.").arg(client_name));

}
//--- slots ---------------------------------------------------------------












































#endif // DATA_SENDER_SERVER_CPP
