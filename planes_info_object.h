
#ifndef PLANES_INFO_OBJECT_H
#define PLANES_INFO_OBJECT_H

#endif // PLANES_INFO_OBJECT_H
#include <QtGui>
#include <QDebug>
#include <fstream>

class PlanesInfo: public QWidget
{
    Q_OBJECT

public:
    PlanesInfo();
    QTextEdit genInfo;
    QStatusBar statusBarTime;

public slots:

    void set_visible();
    void set_gen_info(QString string);
    void set_time_info(QString string);

private:
    bool isVisible;


};
