#include "GeoCoordsWidget.h"

GeoCoordsWidget::GeoCoordsWidget(int mType) {


    type = mType;
    ptxtDegree = new QLineEdit();
    ptxtMin= new QLineEdit();
    ptxtSec = new QLineEdit();
    boxType = new QComboBox();

    ptxtDegree->setMaximumWidth(21);
    ptxtMin->setMaximumWidth(28);
    ptxtSec->setMaximumWidth(35);

    QLabel *lblDegree = new QLabel(QChar(0x00B0));
    QLabel *lblMin = new QLabel("'");
    QLabel *lblSec = new QLabel("''");



    ptxtMin->setValidator(new QIntValidator(0, 59, ptxtMin));
    ptxtSec->setValidator(new QIntValidator(0, 59, ptxtSec));

    if(type == 0) { // ���� ������
        ptxtDegree->setValidator(new QIntValidator(0, 90, ptxtDegree));
        boxType->addItem("c.�.");
        boxType->addItem("�.�.");
    } else { //���� �������
        ptxtDegree->setValidator(new QIntValidator(0, 180, ptxtDegree));
        boxType->addItem("�.�.");
        boxType->addItem("�.�.");
    }

    QHBoxLayout* phbxLayout = new QHBoxLayout;
    phbxLayout->addWidget(ptxtDegree);
    phbxLayout->addWidget(lblDegree);
    phbxLayout->addWidget(ptxtMin);
    phbxLayout->addWidget(lblMin);
    phbxLayout->addWidget(ptxtSec);
    phbxLayout->addWidget(lblSec);
    phbxLayout->addWidget(boxType);
    this->setLayout(phbxLayout);


}

void GeoCoordsWidget::initWidget(double num) {
    if(type == 0) {
        if( (num < -90) || (num > 90) ) {
            QMessageBox msgBox;
            msgBox.setText("������ ����������� ���������� �� �����. ������ ������ ������ � �������� ��  -90 �� 90");
            msgBox.exec();
            return;
        }
    } else {
        if( (num < -180) || (num > 180) ) {
            QMessageBox msgBox;
            msgBox.setText("������ ����������� ���������� �� �����. ������� ������ ������ � �������� ��  -180 �� 180");
            msgBox.exec();
            return;
        }

    }

    double coord;
    if(num < 0) {
        coord = -num;
        boxType->setCurrentIndex(1);
    } else {
        coord = num;
        boxType->setCurrentIndex(0);
    }

    int d = floor (coord);
    int min = floor( (coord - d) * 60);
    int sec = floor( (coord - d - min / 60.0) * 3600);


    ptxtDegree->setText(QString::number(d));
    ptxtMin->setText(QString::number(min));
    ptxtSec->setText(QString::number(sec));
    this->update();


}

double GeoCoordsWidget::getNumber() {
    double number;
    int deg, min, sec;
    deg = ptxtDegree->text().toInt();
    min = ptxtMin->text().toInt();
    sec = ptxtSec->text().toInt();
    if(type == 0) {
        if( (deg == 90) && ((min != 0) || (sec != 0))) {
            QMessageBox msgBox;
            msgBox.setText("������ ������ ������ � �������� ��  -90 �� 90!");
            msgBox.exec();
            return -200;
        }
    } else {
        if( (deg == 180) && ((min != 0) || (sec != 0))) {
            QMessageBox msgBox;
            msgBox.setText("������ ������ ������ � �������� ��  -180 �� 180!");
            msgBox.exec();
            return -200;
        }
    }
    number = deg + min / 60.0 + sec / 3600.0;

    qDebug() << "Number " << number;

    if(boxType->currentIndex() != 0) {
        number = -number;
    }
    return number;
}
