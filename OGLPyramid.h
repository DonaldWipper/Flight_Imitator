/* ======================================================================
**  OGLPyramid.h
** ======================================================================
**
** ======================================================================
**  Copyright (c) 2007 by Max Schlee
** ======================================================================
*/

#ifndef _OGLPyramid_h_
#define _OGLPyramid_h_

#include <QGLWidget>
#include <QtGui>
#include <QDebug>
#include <fstream>
#include <time.h>



#include "NETWORK_CONNECTION/time_reciever_client.h"
#include "plane.h"
#include "drawing_map.h"






/***********************************************************
*              ����� OpenGL-���������                      *
************************************************************/



class OGLPyramid : public QGLWidget{
    Q_OBJECT
private:

    DrawingMap Map; // ����� ��� ���������
    float move_z_increment;  //���������� �����������
    float move_x_increment;
    float move_y_increment;


    vector<PLANE> planes;             // ����� ��������
    int *matrix_flying_planes; // ������ ������ ���������, ������� � ������ ������ ��������� � ������

    int *matrix_need_trip;

    int isNeedDoubleMap(float& move_x_increment);
    void setScales();
    int scenary_mode;

    ASh::GeoGraph::GeoCoord centre1;  //������ �����������, � ������� ������ ������ ��������� ���������� �����
    ASh::GeoGraph::GeoCoord centre2;
    double R; //������� ���� �����������



protected:
    virtual void   initializeGL   (                       );
    //virtual void   resizeGL       (int nWidth, int nHeight);
    virtual void   paintGL        (                       );
    virtual void   timerEvent(QTimerEvent *);

signals:
    void info_planes_changed(QString str);
    void info_time_changed(QString str);
    void send_coords(const TimeInfo& ,
                     const std::vector<TargetParGeo> &);

    void changeCoords(float, float);
    void newTripPoint(float, float);
    void change_row_trip(int num_trip);      //��� ������ � ����������� ����� �� ����� �������� ������ ������ � �������



public slots:
    void change_sectors(InpInfo _infoZones);
    void updateTimeState(TimeInfo &CurTime);
    void accept_new_trip(float a, float b);
    void recalculate();
    void set_scenary_mode(int mode);

    void setForbCirc(ASh::GeoGraph::GeoCoord cnt1,
                     ASh::GeoGraph::GeoCoord ctr2,
                     double r); //��������� ���������� ��� ����������� �������

public:

    void init_cities();

    int screen_width;
    int screen_height;

    //QValidator

    std::vector<Model_Struct::PlaneTTD> *plns_params;  // �
    std::vector<Model_Struct::FlightTask> *tasks;
    std::vector<Model_Struct::Airport>  *airports;
    std::vector<Model_Struct::Trip> *setTrip;


    Model_Struct::Trip *oneTrip;

    QString set_info_planes(int count_planes);

    void draw_names();

    void draw_number_zone(Num_zone cur_zone);
    GLuint draw_new_map_list();



    int count_planes;
    int *pntr_cnt_planes;
    void  stop();
    void restart();
    void _continue();
    void  _pause();
    int timer_id;
    void   resizeGL       (int nWidth, int nHeight);
    void   keyPressEvent(QKeyEvent* pe);
    void   mousePressEvent(QMouseEvent* pe        );
    void   mouseMoveEvent (QMouseEvent* pe        );
    void   mouseReleaseEvent(QMouseEvent*pe       );
    void   wheelEvent(QWheelEvent* pe);

    int mode;
    //0  - ������, ����� �� �� ����� ������ �������� ���������� �����
    //1  - ������, ����� �� ������ ������


     OGLPyramid(QWidget* pwgt = 0,
                Model_Struct::Trip *trip = NULL,
                std::vector <Model_Struct::Airport> *airports2 = NULL);

     OGLPyramid(QWidget* pwgt = 0, int *_count_planes = NULL,
                std::vector<Model_Struct::PlaneTTD> *plns_params2 = NULL,
                std::vector<Model_Struct::FlightTask> *tasks2 = NULL,
                std::vector<Model_Struct::Airport>  *airports2 = NULL,
                std::vector<Model_Struct::Trip> *setTrip2 = NULL);


};
#endif  //_OGLPyramid_h_
