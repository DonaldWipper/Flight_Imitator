#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#endif // MAIN_WINDOW_H

#include <QtGui>


class MAIN_WINDOW  : public QWidget {
    Q_OBJECT
private:
     QMenuBar bar; //Top bar with settings
     QMenu* pmnu;
     QPushButton* pcmdStart;
     QPushButton* pcmdStop;
     QPushButton* pcmdPause;
     QPushButton* pcmdContinue;
     QPushButton* pcmdShow1;
     QPushButton* pcmdHide1;

};
