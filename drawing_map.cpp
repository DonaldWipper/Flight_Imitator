#include "drawing_map.h"



#ifndef CALLBACK
#define CALLBACK
#endif




void CALLBACK tessErrorCB(GLenum errorCode)
{
    const GLubyte *errorStr;

    errorStr = gluErrorString(errorCode);
    std::cerr << "[ERROR]: " << errorStr << std::endl;
}



void CALLBACK tessBeginCB(GLenum which)
{
    glBegin(which);

}



void CALLBACK tessEndCB()
{
    glEnd();

}



void CALLBACK tessVertexCB(const GLvoid *data)
{
    // cast back to double type
    const GLdouble *ptr = (const GLdouble*)data;

    glVertex3dv(ptr);

}



//----------------------------------------------------------------------------------------------------------


DrawingMap::DrawingMap() {


    leftX = -24.5231;
    rightX = 53.7025;
    leftY = 34.5653;
    rightY = 80.4883;

    size_texX = rightX - leftX;
    size_texY = rightY - leftY;


    stepX = size_texX / (rightX - leftX);
    stepY = size_texY / (rightY - leftY);

    size_city = 0.25;
    scaleH = 0.0001;
    num_focus_city = -1;
}




GLuint DrawingMap::make_new_map_list() {
    FILE *file;
    file = fopen("FILES/World.map", "rb");


    GLuint id = glGenLists(1);  // create a display list
    if(!id) return 0;          // failed to create a list, return 0



    glEnable(GL_TEXTURE_2D);

    glNewList(id, GL_COMPILE);


    int k = 0;
    if (file) {
        while (! feof(file))  {
            int n;
            k++;
            /*fread(&a, sizeof(int), 1, file);
            fread(&b, sizeof(int), 1, file);*/
            fread(&n, sizeof(int), 1, file);


            //glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);

            glColor3f(0.0f, 0.0f, 0.0f);
            glBegin(GL_LINE_LOOP);
            // glLineWidth(0.0000001);

            float la, fi;


            for (int i = 1; i <= n; i++)
            {

                //  glColor3f(1.0f, 0.2f, 0.0f);
                fread(&la, sizeof(float),1,file);
                fread(&fi,sizeof(float),1,file);
                glVertex3f(-leftX + la -size_texX / 2,-leftY + fi -size_texY / 2, 0);

            }


            glEnd();



        }

    }

    glEndList();
    glDisable(GL_TEXTURE_2D);

    fclose(file);
     return id;

}




void DrawingMap::add_city(City c) {
   float g, r, b;
   r = (rand() % 10) / 10.0;
   g = (rand() % 100) / 100.0;
   b = (rand() % 1000) / 1000.0;
   c.color[0] = r;
   c.color[1] = g;
   c.color[2] = b;
   cities.push_back(c);
}	












//������ ���� ������



void DrawingMap::drawMainSector(ZNK_Geometry &zona) {
    ASh::GeoGraph::GeoCoord Stat_coord;
    Stat_coord.setFi(zona.Fi_prm);
    Stat_coord.setLa(zona.La_prm);

    glPushMatrix();
    glTranslatef(0.0, 0.0, 850);

    float La, Fi, x, y;
    double delAz = 10;
    double delD = 100;
    double Az = 0;
    double stD = 1000, endD = 3000;
    double nD = (endD - stD) / delD;
    glLineWidth(0.1);
    for(int i = 0; i < nD; i++) {
        glBegin(GL_LINE_STRIP);
        double D = stD + i * delD;
        for(int i = 0; i < 100; i++ ) {
            Az = i * delAz;
            ASh::GeoGraph::GeoCoord point = ASh::GeoGraph::AKY(Stat_coord, ASh::GeoGraph::LocationCoord(D, Az));

            La = point.La();
            Fi = point.Fi();

            float shiftX = La - leftX;
            float shiftY = Fi - leftY;

            x = -size_texX / 2 + shiftX * stepX;
            y = -size_texY / 2 + shiftY * stepY;
            glVertex3f(x, y, 0);
       }
       glEnd();

    }

    glPopMatrix();

}


void DrawingMap::drawSector(ZNK_Geometry &zona,
                            const double& Naz,
                            const int& Nkd

                            /*const ASh::GeoGraph::GeoCoord& Stat_coord,  // ���������� ������� �����������
                            const double& Azn,  // ��������� ������
                            const double& Azk,  // �������� ������
                            const double& Naz,
                            const double& Dn,
                            const double& Dk,
                            const int& Nkd*/) {


    double Azn = zona.Azmin, Azk = zona.Azmax, Dn = zona.Dmin, Dk = zona.Dmax;
    ASh::GeoGraph::GeoCoord Stat_coord;
    Stat_coord.setFi(zona.Fi_prm);
    Stat_coord.setLa(zona.La_prm);


    cur_zone.num = zona.number;

    double delAz = (Azk - Azn) / (Naz - 1);
    double delD = (Dk - Dn) / (Nkd - 1);

    //double delD = 100;
    //double delAz = 10;
    //double Naz2, Nkd2;



    double Az0, Az1, R0, R1, Az, delazN, R, delrN;

    int N1 = 100;

    switch(zona.number % 4) {
    case(0):
        glColor3f(1, 0, 0);
        break;
    case(1):
        glColor3f(0, 1, 0);
        break;
    case(2):
        glColor3f(0, 0, 1);
        break;
    case(3):
        glColor3f(0.6, 0.2, 0.5);
        break;

    }


    glLineWidth(2);
    for (int kD = 0; kD < Nkd - 1; kD++) {
        for(int kAz = 0; kAz < Naz - 1; kAz++) {

            Az0 = Azn + kAz * delAz;
            Az1 = Azn + (kAz + 1) * delAz;

            R0 = Dn + kD * delD;
            R1 = Dn + (kD + 1) * delD;


            delazN = std::abs(Az0 - Az1) / N1;
            delrN = std::abs(R0 - R1) / N1;

            glPushMatrix();
            glTranslatef(0.0, 0.0, 850);
           /* glBegin(GL_POLYGON);
             glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);*/
             glBegin(GL_LINE_STRIP);
             float La, Fi, x, y;
            // 1-2
            R = R0;

            for (int i = 0; i < N1; i++) {
                Az = Az0 + i * delazN;
                ASh::GeoGraph::GeoCoord point = ASh::GeoGraph::AKY(Stat_coord, ASh::GeoGraph::LocationCoord(R,Az));

                La = point.La();
                Fi = point.Fi();

                float shiftX = La - leftX;
                float shiftY = Fi - leftY;

                x = -size_texX / 2 + shiftX * stepX;
                y = -size_texY / 2 + shiftY * stepY;

                glVertex3f(x, y, 0);
            }

            // 2-3
            Az=Az1;

            for (int i = 0; i < N1; i++) {
                R = R0 + i * delrN;
                ASh::GeoGraph::GeoCoord point = ASh::GeoGraph::AKY(Stat_coord, ASh::GeoGraph::LocationCoord(R,Az));
                La = point.La();
                Fi = point.Fi();
                float shiftX = La - leftX;
                float shiftY = Fi - leftY;

                x = -size_texX / 2 + shiftX * stepX;
                y = -size_texY / 2 + shiftY * stepY;
                glVertex3f(x, y, 0);
            }

            // 3-4
            R=R1;

            for (int i = 0; i <= N1; i++) {
                Az = Az1 - i * delazN;
                ASh::GeoGraph::GeoCoord point = ASh::GeoGraph::AKY(Stat_coord, ASh::GeoGraph::LocationCoord(R,Az));
                La = point.La();
                Fi = point.Fi();
                float shiftX = La - leftX;
                float shiftY = Fi - leftY;

                x = -size_texX / 2 + shiftX * stepX;
                y = -size_texY / 2 + shiftY * stepY;

                glVertex3f(x, y, 0);
             }

             // 4-1
             Az = Az0;

             for (int i = 0; i <= N1; i++) {
                 R = R1 - i * delrN;
                 ASh::GeoGraph::GeoCoord point = ASh::GeoGraph::AKY(Stat_coord, ASh::GeoGraph::LocationCoord(R,Az));
                 La = point.La();
                 Fi = point.Fi();
                 if (i == N1 / 2) {
                     cur_zone.Fi = Fi;
                     cur_zone.La = La;
                 }
                 float shiftX = La - leftX;
                 float shiftY = Fi - leftY;

                 x = -size_texX / 2 + shiftX * stepX;
                 y = -size_texY / 2 + shiftY * stepY;

                 glVertex3f(x, y, 0);
             }
             glEnd();
             glPopMatrix();
        }
   }
   glLineWidth(0.1);

}

GLuint DrawingMap::draw_new_map_list() {
    float maxX = - 200, minX = -200, minY = 200, maxY = 200;

    GLuint id = glGenLists(1);  // create a display list
    if(!id) return 0;          // failed to create a list, return 0

    GLUtesselator *tess = gluNewTess(); // create a tessellator
    if(!tess) return 0;  // failed to create tessellation object, return 0

    FILE *file;
    file = fopen("evropa", "rb");

    gluTessCallback(tess, GLU_TESS_BEGIN, (void (__stdcall*)(void))tessBeginCB);
    gluTessCallback(tess, GLU_TESS_END,(void (__stdcall*)(void))tessEndCB);
    gluTessCallback(tess, GLU_TESS_ERROR, (void (__stdcall*)(void))tessErrorCB);
    gluTessCallback(tess, GLU_TESS_VERTEX, (void (__stdcall*)(void))tessVertexCB);


        // pointing same address.


    glNewList(id, GL_COMPILE);
    glColor3f(1,0,0);

    if (file) {
        gluTessBeginPolygon(tess, 0);
            while (! feof(file))  {
                int a, b, n;
                fread(&a, sizeof(int), 1, file);
                fread(&b, sizeof(int), 1, file);
                fread(&n, sizeof(int), 1, file);


                //glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);

                glColor3f(1.0f, 0.2f, 0.0f);


                float la, fi;

                GLdouble **vertex = new GLdouble*[n];
                for(int i = 0; i < n; i++) {
                    vertex[i] = new GLdouble[3];
                }
                gluTessBeginContour(tess);
                for (int i = 0; i < n; i++)
                {

                       //GLdouble vertex[3];
                    //  glColor3f(1.0f, 0.2f, 0.0f);
                    fread(&la, sizeof(float), 1, file);
                    fread(&fi,sizeof(float), 1, file);
                    if(la < minX) minX = la;
                    if(la > maxX) maxX = la;
                    if(fi < minY) minY = fi;
                    if(fi > maxX) maxY = fi;


                    vertex[i][0] = -leftX + la -size_texX / 2;
                    vertex[i][1] = -leftY + fi -size_texY / 2;
                    vertex[i][2] = 0;

                    //gluTessVertex(tess, vertex, vertex);
                    //quad1[0][0] =  -leftX + la -size_texX / 2;
                    //quad1[0][1] = -leftY + fi -size_texY / 2;
                    gluTessVertex(tess, vertex[i], vertex[i]);
                    //glVertex3f(-leftX + la -size_texX / 2,-leftY + fi -size_texY / 2, 0);

                }
                delete[] vertex;
                    gluTessEndContour(tess);
                   // break;







            }
               gluTessEndPolygon(tess);

        }
     glEndList();
     gluDeleteTess(tess);
     qDebug() << "minX " << minX;
     qDebug() << "minY " << minY;
     qDebug() << "maxX " << maxX;
     qDebug() << "maxY " << maxY;


     fclose(file);
       return id;

}


void DrawingMap::draw_city(City c) {

    float shiftX = c.La - leftX;
    float shiftY = c.Fi - leftY;


    glColor3f(c.color[0], c.color[1], c.color[2]);


    float centreX = -size_texX / 2 + shiftX * stepX;
    float centreY = -size_texY / 2 + shiftY * stepY;


    glPushMatrix();
    glTranslatef(0.0, 0.0, 850);

    glBegin(GL_POLYGON);
        for(int i = 0; i < 100; i++) {
            GLfloat step = 2 * 3.14 / 100;
            GLfloat ang = i * step;
            glVertex3f(centreX + cos(ang) * size_city, centreY + sin(ang) * size_city, 0);
        }
    glEnd();

    glPopMatrix();
}	





void DrawingMap::draw_trip_point(City c, bool focus) {

    float shiftX = c.La - leftX;
    float shiftY = c.Fi - leftY;


    glColor3f(c.color[0], c.color[1], c.color[2]);


    float centreX = -size_texX / 2 + shiftX * stepX;
    float centreY = -size_texY / 2 + shiftY * stepY;



    glPushMatrix();
    glTranslatef(centreX, centreY, 850);

    glColor3f(0.0, 0.0, 0.0);
    glBegin(GL_LINE_STRIP);
        glVertex3f(0, 0, 0);
        glVertex3f(0, size_city, 0);
        glVertex3f(0, size_city, 0);
        glVertex3f(0, 2 * size_city, 0);
        glVertex3f(size_city, 2 * size_city, 0);
        glVertex3f(size_city, size_city, 0);
        glVertex3f(0, size_city, 0);
    glEnd();




    if(focus == false)
        glColor4f(1.0, 0.0, 0.0, 0.5);
    else
        glColor4f(0.0, 0.0, 0.0, 0.5);
    glBegin(GL_POLYGON);
        glVertex3f(0, size_city, 0);
        glVertex3f(0, 2 * size_city, 0);
        glVertex3f(size_city, 2 * size_city, 0);
        glVertex3f(size_city, size_city, 0);
        glVertex3f(0, size_city, 0);
    glEnd();







    glPopMatrix();
}

void DrawingMap::draw_place(float Fi, float La, float R) {
    float x, y, z, x2, y2;
    float h = 0;
    float La2, Fi2;
    get_OGL_coords(La, Fi, h, x, y, z);



    ASh::GeoGraph::GeoCoord coord;
    ASh::GeoGraph::GeoCoord coordSt;
    coordSt.setFi(Fi);
    coordSt.setLa(La);


    float dX, dY;

    glPushMatrix();
    glTranslatef(0.0, 0.0, 850);
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    for(int i = 0; i < 100; i++) {
        GLfloat step = 2 * 3.14 / 100;
        GLfloat ang = i * step;

        dX = R * cos(ang);
        dY = R * sin(ang);
        coord = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(dX,dY),
                                        coordSt);
        get_OGL_coords(coord.La(), coord.Fi(), h, x2, y2, z);
        glVertex3f(x2, y2, 0);
    }
    glEnd();
    glPopMatrix();
}




void DrawingMap::draw_cities() {
    vector<City>::iterator c_it;

    for(c_it = cities.begin(); c_it != cities.end(); c_it++) {
       draw_city((*c_it));
    }
}	

void DrawingMap::draw_vertex(float La, float Fi, float H) {
    float shiftX = La - leftX;
    float shiftY = Fi - leftY;

    float centreX = -size_texX / 2 + shiftX * stepX;
    float centreY = -size_texY / 2 + shiftY * stepY;

    glVertex3f(centreX, centreY, 0);

}	


void DrawingMap::get_Geo_coords(float &La, float &Fi, float h, float x, float y, float z) {

    La = (x + size_texX / 2) / stepX + leftX;
    Fi = (y + size_texY / 2) / stepY + leftY;
    h = 0;
}




void   DrawingMap::get_OGL_coords(float La, float Fi, float h, float &x, float &y, float &z) {
    float shiftX = La - leftX;
    float shiftY = Fi - leftY;
    

    float centreX = -size_texX / 2 + shiftX * stepX;
    float centreY = -size_texY / 2 + shiftY * stepY;
    x = centreX;
    y = centreY;
    z = 0;
}	
	

