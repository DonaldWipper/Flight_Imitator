#include "SampleMap.h"
#include "plane.h"
#include "drawing_map.h"

GLuint id_map2 = 0;
GLuint id_map2_lines = 0;


int screen_width2 = 800;
int screen_height2 = 800;


float move_z_increment2 = -600.0;
float move_x_increment2 = 0.0;
float move_y_increment2 = 0.0;
float rotation_x2 = 0;
float rotation_x2_increment = 1;

DrawingMap Map2; // ����� ��� ���������








SampleMapView::SampleMapView(QGraphicsScene* pScene, QWidget* pwgt)
    : QGraphicsView(pScene, pwgt) {
    mpGLMap = new  OGLMap(NULL);
    setViewport(mpGLMap);


}



void SampleMapView::drawBackground(QPainter *p, const QRectF &r)
{
    mpGLMap->updateGL();
    QGraphicsView::drawBackground(p, r);
}


void	SampleMapView::resizeEvent ( QResizeEvent * event ) {

    QSize size = event->size();
    mpGLMap->resizeGL(size.width(), size.height());
}


OGLMap::OGLMap(QWidget* pwgt/*= 0*/) : QGLWidget(pwgt)

{


}


// ----------------------------------------------------------------------
/*virtual*/void OGLMap::initializeGL()
{


    glClearColor(1.0, 1.0, 1.0, 0.0); // This clear the background color to black
    glShadeModel(GL_SMOOTH); // Type of shading for the polygons
    // Viewport transformation
    glViewport(0, 0, screen_width2, screen_height2);

    // Projection transformation
    glMatrixMode(GL_PROJECTION); // Specifies which matrix stack is the target for matrix operations
    glLoadIdentity(); // We initialize the projection matrix as identity
    gluPerspective(45.0f,(GLfloat)screen_width2/(GLfloat)screen_height2,10.0f,10000.0f); // We define the "viewing volume"

    glEnable(GL_DEPTH_TEST); // We enable the depth test (also called z buffer)
       // Polygon rasterization mode (polygon filled)




    //id_map2 = draw_new_map_list();
    id_map2_lines = Map2.make_new_map_list();
}

// ----------------------------------------------------------------------
/*virtual*/void OGLMap::resizeGL(int width, int height)
{
    screen_width2 = width; // We obtain the new screen width values and store it
    screen_height2 = height; // Height value

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // We clear both the color and the depth buffer so to draw the next frame
    glViewport(0, 0, screen_width2, screen_height2); // Viewport transformation

    glMatrixMode(GL_PROJECTION); // Projection transformation
    glLoadIdentity(); // We initialize the projection matrix as identity
    gluPerspective(45.0f,(GLfloat)screen_width2/(GLfloat)screen_height2, 10.0f, 10000.0f);
    updateGL();
}



// ----------------------------------------------------------------------
/*virtual*/ void OGLMap::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // This clear the background color to dark blue
glMatrixMode(GL_MODELVIEW); // Modeling transformation

glClearColor(1.0, 1.0, 1.0, 0.0);
glLoadIdentity(); // Initialize the model matrix as identity

glTranslatef(0.0 + move_x_increment2,0.0 + move_y_increment2, -300 +  move_z_increment2); // We move the object forward (the model matrix is multiplied by the translation matrix)

glPushMatrix();
glTranslatef(0.0, 0.0, 850);
glCallList(id_map2_lines);
glPopMatrix();
glFlush(); // This force the execution of OpenGL commands

 }

