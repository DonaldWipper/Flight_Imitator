#ifndef MYVIEW_H
#define MYVIEW_H


#include <QGraphicsView>
#include "OGLPyramid.h"

#include "iostream"

using namespace std;

// ======================================================================
class MyView: public QGraphicsView {
Q_OBJECT
public:
    MyView(QGraphicsScene* pScene, int *count_planes,
                                   std::vector<Model_Struct::PlaneTTD> *plns_params2,
                                   std::vector<Model_Struct::FlightTask> *tasks,
                                   std::vector<Model_Struct::Airport>  *airports,
                                   std::vector <Model_Struct::Trip> *setMarsh,
                                   QWidget* pwgt = 0)
        : QGraphicsView(pScene, pwgt)
    {
         mpGLWidg = new OGLPyramid(NULL, count_planes,
                                         plns_params2,
                                         tasks,
                                         airports,
                                         setMarsh);
         setViewport(mpGLWidg);

         this->setDragMode(QGraphicsView::ScrollHandDrag);
         this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
         this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    }

public slots:


    void start()
    {
        mpGLWidg->restart();
        mpGLWidg->updateGL();
    }


    void recalculate() {
        mpGLWidg->recalculate();
        mpGLWidg->updateGL();
    }


    void pause()
    {
         mpGLWidg->_pause();
         mpGLWidg->updateGL();
    }


    void pause(QString str)
    {
         mpGLWidg->_pause();
         mpGLWidg->updateGL();
    }


    void stop()
    {
        mpGLWidg->stop();
        mpGLWidg->updateGL();
    }

    void _continue()
    {
        mpGLWidg->_continue();
        mpGLWidg->updateGL();
    }



protected:

    virtual void	resizeEvent ( QResizeEvent * event ) {
        QSize size = event->size();
        mpGLWidg->resizeGL(size.width(), size.height());
    }


   void keyPressEvent(QKeyEvent* pe) {
       mpGLWidg-> keyPressEvent(pe);
       QGraphicsView::keyPressEvent( pe);
   }

   void   mousePressEvent(QMouseEvent* pe ) {
       mpGLWidg->mousePressEvent(pe);

       QGraphicsView::mousePressEvent(pe);
   }

   void   mouseMoveEvent (QMouseEvent* pe        ) {
       mpGLWidg->mouseMoveEvent(pe);
       QGraphicsView::mouseMoveEvent(pe);

   }

   void   mouseReleaseEvent(QMouseEvent*pe       ) {
       mpGLWidg->mouseMoveEvent(pe);
       QGraphicsView::mouseMoveEvent(pe);

   }

   void wheelEvent(QWheelEvent*pe       ) {
       mpGLWidg->wheelEvent(pe);
       QGraphicsView::wheelEvent(pe);
   }

   void drawBackground(QPainter *p, const QRectF &r)
   {
       mpGLWidg->updateGL();
       QGraphicsView::drawBackground(p, r);
   }

   public:
      OGLPyramid* mpGLWidg;
};

#endif // MYVIEW_H
