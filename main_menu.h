#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#endif // MAIN_MENU_H


#include "OGLPyramid.h"
#include <QtGui>
#include "MyView.h"
#include "planes_info_object.h"
#include <stdlib.h>
#include <QAction>


#include "NETWORK_CONNECTION/data_sender_server.h"
#include "DIALOGS/PlaneParametersDialog.h"
#include "DIALOGS/planes_params_edit.h"
#include "DIALOGS/InitDataPlnsDlg.h"
#include "DIALOGS/InitFlightTaskDlg.h"
#include "DIALOGS/AiroportDlg.h"
#include "DIALOGS/PortsDlg.h"


//***************************************************
//*          ����� �������� ���� ���������          *
//***************************************************


class MAIN_MENU : public QWidget {
    Q_OBJECT



private:


    QIcon play_icon;                  //������ ���
    QIcon pause_icon;                 //������� ��� ���������
    QIcon stop_icon;                  //��������

    QAction *startAction;
    QAction *stopAction;


    int btnMode;



    QMenuBar* bar;
    QMenu* pmnuSets;
    QMenu* pmnuHelp;
    QToolBar *tool_bar;
    QSplitter *splitter;          //�������� ������������� ������
    QSplitter *splitter2;         //C������� ��������������� ������

    //++++++++++++++++++++++++++
    PlanesInfo *info;              //���� � �����������

    PORTS_DLG *portDlg;            //�����
    MY_CLIENT  *client;            //������
    DATA_SENDER_SERVER *server;    //������
    PARAMS_EDITOR *planesEditWgt;  //��������� �����
    INIT_PLNS_DLG *intPlnsdlgs;    //������� ���������
    EDIT_AIRPORT_DLG *airportsDlg; //������� ����������
    MyView*  pView;                //���� � OpenGL
    QTextEdit *networkTxtEdit;     //���� ���� � ������� ���������������
    //+++++++++++++++++++++++++++

    QWidget *wgt2;    // �������������� ������ ��� ������������� ���������
    QWidget *wgtInfo; // ������ � ������� �������
    QTextEdit *txtInfo;

public:
    MAIN_MENU();
    void start();


public slots:

    void clickStart() {
        switch(btnMode) {
        case(0):  //���� �� ��������
            startAction->setIcon(pause_icon);
            startAction->setText("�����");
            pView->start();
            btnMode = 1;
            break;
        case(1):
            startAction->setIcon(play_icon);
            startAction->setText("�����������");
            pView->pause();
            btnMode = 2;
            break;
        case(2):
            startAction->setIcon(pause_icon);
            startAction->setText("�����");
            pView->_continue();
            btnMode = 1;
            break;
        }
    }

    void clickStop() {
        startAction->setIcon(play_icon);
        startAction->setText("�������� ���������� ������");
        btnMode = 0;
        pView->stop();
        btnMode = 0;
    }


};




