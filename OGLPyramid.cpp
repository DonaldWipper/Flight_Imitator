
/* ======================================================================
**  OGLPyramid.cpp
** ======================================================================
**
** ======================================================================
**  Copyright (c) 2007 by Max Schlee
** ======================================================================
*/

#include "OGLPyramid.h"


using namespace::std;





int count_fly_planes = 0;




bool mouse_press = false;


float SCALE_FONT = 0; //������

int last_x = 0, last_y = 0;

float scale_h = 0.001; // ���������� �������� ��� ������

boost::posix_time::ptime CurTime;


InpInfo infoZones;

int needDoubleMap = 0;

double scaleMove;


GLuint id_map = 0;
GLuint id_map_lines = 0;
GLuint id_airport_names = 0;
GLuint id_plane2D;


GLfloat sea_water_color[] = {0.498, 0.7803, 1.0};



GLfloat rPosX, lPosX;

float shift_plan_x = 0, shift_plan_y = 0, shift_plan_z = 0;


float leftX = -24.5231;
float rightX = 53.7025;
float leftY = 34.5653;
float rightY = 80.4883;

float size_texX = rightX - leftX;
float size_texY = rightY - leftY;


#ifndef CALLBACK
#define CALLBACK
#endif



void CALLBACK tessErrorCB(GLenum errorCode)
{
    const GLubyte *errorStr;

    errorStr = gluErrorString(errorCode);
    std::cerr << "[ERROR]: " << errorStr << std::endl;
}



void CALLBACK tessBeginCB(GLenum which)
{
    glBegin(which);

}



void CALLBACK tessEndCB()
{
    glEnd();

}



void CALLBACK tessVertexCB(const GLvoid *data)
{
    // cast back to double type
    const GLdouble *ptr = (const GLdouble*)data;

    glVertex3dv(ptr);

}




/* *******************************************************
   *  ������� ��� ������������ �����                     *
   *  ���������� ����� �� ��������  ����������� �����    *
   *  ���� �������� ��������, ����������� �����������    *
   *  �����                                              *
   * *****************************************************  */

int OGLPyramid::isNeedDoubleMap(float& move_x_increment) {

    //������s ��������������
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    //***********************

    float y, z, z2;

    GLfloat rightX; // ������ ������� ����� � OPenGL �����������
    GLfloat leftX; // ����� ������� ����� � OPenGL �����������

    Map.get_OGL_coords(180, 180, 0.0, rightX, y, z);
    Map.get_OGL_coords(-180, 180, 0.0, leftX, y, z);




    GLdouble rightPosX, rightPosY, posZ;
    GLdouble leftPosX, leftPosY;
    float winY, winZ;
    winY = 1;

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
    winY = (float)viewport[3] - (float)winY;
    glReadPixels(screen_width - 1, winY, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);

    gluUnProject(screen_width - 1, screen_height - 1, winZ, modelview, projection, viewport, &rightPosX, &rightPosY, &posZ);
    gluUnProject(1, 1, winZ, modelview, projection, viewport, &leftPosX, &leftPosY, &posZ);

    Map.get_Geo_coords(lPosX, rPosX, z2, leftPosX, rightPosX, z);
    Map.get_Geo_coords(rPosX, lPosX, z2, rightPosX, leftPosX,  z);

    glBegin(GL_POLYGON);
       glVertex3f(leftPosX, leftPosY, 0);
       glVertex3f(leftPosX, rightPosY, 0);
       glVertex3f(rightPosX, rightPosX, 0);
       glVertex3f(rightPosX, leftPosX, 0);
    glEnd();


    if(rightPosX > rightX) {
        if (leftPosX < rightX) {


            return 1;
        } else {

           move_x_increment += rightX - leftX;
        }
    }

    if(leftPosX < leftX) {
        if (rightPosX > leftX) {

            return 2;
        }  else {
           move_x_increment -= rightX - leftX;
        }

    }


    return -1;



}



//������� ��� ��������� ����������� ���������




GLuint OGLPyramid::draw_new_map_list() {
    float maxX = - 200, minX = 200, minY = 200, maxY = -200;

    //������� �����
    float leftX = -24.5231;
    float rightX = 53.7025;
    float leftY = 34.5653;
    float rightY = 80.4883;


    float size_texX = rightX - leftX;
    float size_texY = rightY - leftY;



    GLuint id = glGenLists(1);  // create a display list
    if(!id) return 0;          // failed to create a list, return 0

    GLUtesselator *tess = gluNewTess(); // create a tessellator
    if(!tess) return 0;  // failed to create tessellation object, return 0


    FILE *file;
    file = fopen("FILES/World.map", "rb");

    gluTessCallback(tess, GLU_TESS_BEGIN, (void (__stdcall*)(void))tessBeginCB);
    gluTessCallback(tess, GLU_TESS_END,(void (__stdcall*)(void))tessEndCB);
    gluTessCallback(tess, GLU_TESS_ERROR, (void (__stdcall*)(void))tessErrorCB);
    gluTessCallback(tess, GLU_TESS_VERTEX, (void (__stdcall*)(void))tessVertexCB);

    gluTessProperty(tess, GLU_TESS_WINDING_RULE, GLU_TESS_WINDING_ODD);



    glNewList(id, GL_COMPILE);
    glColor3f(128 / 256.0, 128 / 256.0, 128 / 256.0);
    int k = 0;

    if (file) {

        while (! feof(file))  {
            int n;
            k++;

            fread(&n, sizeof(int), 1, file);
            float la, fi;

            GLdouble **vertex = new GLdouble*[n];
            for(int i = 0; i < n; i++) {
                vertex[i] = new GLdouble[3];
            }



            if(k == 43) {
                gluTessBeginPolygon(tess, 0);
                gluTessBeginContour(tess);
                for (int i = 0; i < n; i++)
                {
                    fread(&la, sizeof(float), 1, file);
                    fread(&fi,sizeof(float), 1, file);

                    vertex[i][0] = -(GLdouble)leftX + (GLdouble)la -(GLdouble)size_texX / 2;
                    vertex[i][1] = -(GLdouble)leftY + (GLdouble)fi -(GLdouble)size_texY / 2;
                    vertex[i][2] = 0;
                    if(i != 41)
                    gluTessVertex(tess, vertex[i], vertex[i]);

                }
                gluTessEndContour(tess);
                gluTessEndPolygon(tess);


                delete[] vertex;
                continue;

            }

            if(k == 10) {
                gluTessBeginPolygon(tess, 0);
                gluTessBeginContour(tess);
                for (int i = 0; i < n; i++)
                {
                    fread(&la, sizeof(float), 1, file);
                    fread(&fi,sizeof(float), 1, file);

                    vertex[i][0] = -(GLdouble)leftX + (GLdouble)la -(GLdouble)size_texX / 2;
                    vertex[i][1] = -(GLdouble)leftY + (GLdouble)fi -(GLdouble)size_texY / 2;
                    vertex[i][2] = 0;
                    if (i != 18)
                    gluTessVertex(tess, vertex[i], vertex[i]);

                }

                gluTessEndContour(tess);
                gluTessEndPolygon(tess);


                delete[] vertex;
                continue;

            }




            gluTessBeginPolygon(tess, 0);
            gluTessBeginContour(tess);

            for (int i = 0; i < n; i++)
            {
                fread(&la, sizeof(float), 1, file);
                fread(&fi,sizeof(float), 1, file);

                vertex[i][0] = -(GLdouble)leftX + (GLdouble)la -(GLdouble)size_texX / 2;
                vertex[i][1] = -(GLdouble)leftY + (GLdouble)fi -(GLdouble)size_texY / 2;
                vertex[i][2] = 0;
                gluTessVertex(tess, vertex[i], vertex[i]);
                if(la < minX) minX = la;
                if(la > maxX) maxX = la;
                if(fi < minY) minY = fi;
                if(fi > maxX) maxY = fi;


            }


            delete[] vertex;
            gluTessEndContour(tess);
            gluTessEndPolygon(tess);
        }
    }
    qDebug() << "minX " << minX;
    qDebug() << "minY " << minY;
    qDebug() << "maxX " << maxX;
    qDebug() << "maxY " << maxY;

    glEndList();
    gluDeleteTess(tess);
    fclose(file);
    return id;
}







//�������, ������������ ������ � ��������� � html-�������
QString  OGLPyramid ::set_info_planes(int count_planes) {
    QString str;
    str.append(" <TABLE BORDER>");
    str.append("<TR>");
    str.append("<TH>");
    str.append("ID ��������");
    str.append("</TH>");
    str.append("<TH>");
    str.append("��� ����");
    str.append("</TH>");
    str.append("<TH>");
    str.append("�������");
    str.append("</TH>");
    str.append("<TH>");
    str.append("������");
    str.append("</TH>");
    str.append("<TH>");
    str.append("������(�)");
    str.append("</TH>");
    str.append("<TH>");
    str.append("��������(��/�)");
    str.append("</TH>");
    str.append("<TH>");
    str.append("���������");
    str.append("</TH>");
    str.append("</TR>");

    for(int i = 0; i < count_planes; i++) {
        str.append("<TR>");
        str.append("<TD>");
        str.append(QString::number(i));
        str.append("</TD>");
        str.append("<TD>");
        str.append(QString::fromStdString(planes[i].PZDN.PlanePerfCharact.TargetType));
        str.append("</TD>");
        str.append("<TD>");
         str.append(QString::number(planes[i].Fi,'g',6));
        str.append("</TD>");
        str.append("<TD>");
         str.append(QString::number(planes[i].La,'g',6));
        str.append("</TD>");
        str.append("<TD>");
        str.append(QString::number(planes[i].H,'g',6));
        str.append("</TD>");
        str.append("<TD>");
        str.append(QString::number(planes[i].Plane.Speed,'g',6));
        str.append("</TD>");
        str.append("<TD>");
        str.append(QString::number(planes[i].Plane.Accel,'g',6));
        str.append("</TD>");
        str.append("</TR>");
    }
    str.append("</TABLE>");
    return str;
}



void OGLPyramid::init_cities() {
    if(mode == 1) {
        Map.cities.clear();
        for(uint i = 0; i < airports[0].size(); i++) {
            City tmpCity;
            tmpCity.Fi = airports[0][i].P_geo.Fi();
            tmpCity.La = airports[0][i].P_geo.La();
            Map.add_city(tmpCity);
        }
    } else {
        Map.cities.clear();
        City tmpCity;
        tmpCity.Fi = airports[0][oneTrip->N_airTakeOff].P_geo.Fi();
        tmpCity.La = airports[0][oneTrip->N_airTakeOff].P_geo.La();
        Map.add_city(tmpCity);
        for(uint i = 0; i < oneTrip->tripPoints.size(); i++) {
            tmpCity.Fi = oneTrip->tripPoints[i].P_geo.Fi();
            tmpCity.La = oneTrip->tripPoints[i].P_geo.La();
            Map.add_city(tmpCity);
        }
        tmpCity.Fi = airports[0][oneTrip->N_airLand].P_geo.Fi();
        tmpCity.La = airports[0][oneTrip->N_airLand].P_geo.La();

        Map.add_city(tmpCity);
    }
}





void OGLPyramid::draw_names() {
    QFont font;
    font.bold();

    glColor3f(1, 0, 51.0 / 255.0);

    if(12 + SCALE_FONT < 2) {
         font.setPixelSize(2);
    } else {
        if(move_z_increment < -590) {
            font.setPointSizeF(6.5);
        }
    }


    for(uint i = 0; i < airports[0].size(); i++) {
        float a, b,c;
        Map.get_OGL_coords(airports[0][i].P_geo.La(), airports[0][i].P_geo.Fi(),0, a,b,c);
        renderText(a, b , 0,  QString::fromStdString(airports[0][i].name), font, 2000);

    }
}



void OGLPyramid::draw_number_zone(Num_zone cur_zone) {
    glPushMatrix();

    glTranslatef(0.0, 0.0, 850);
    QFont font;
    font.bold();
    glColor3f(0.0f, 1.0f, 0.0f);
     if(14 + SCALE_FONT < 2) {
         font.setPixelSize(2);
    } else {
        font.setPixelSize(12 + SCALE_FONT);
    }
    float a,b,c;
    Map.get_OGL_coords(cur_zone.La, cur_zone.Fi,0, a,b,c);
        glColor3f(1.0, 0, 0);
    renderText(a, b , 0,  QString::number(cur_zone.num), font, 2000);

    glPopMatrix();

}


//��������� �����
void OGLPyramid::set_scenary_mode(int mode) {
    scenary_mode = mode;
}




void OGLPyramid::setForbCirc(ASh::GeoGraph::GeoCoord cnt1,
                 ASh::GeoGraph::GeoCoord ctr2,
                 double r) {
   centre1 = cnt1;
   centre2 = ctr2;
   R = r;


   qDebug() << centre1.Fi() << " " <<  centre1.La();
   qDebug() << R;
}







// ----------------------------------------------------------------------
OGLPyramid::OGLPyramid(QWidget* pwgt/*= 0*/,
                       Model_Struct::Trip *trip,
                       std::vector <Model_Struct::Airport> *airports2) : QGLWidget(pwgt)

{
   mode = 0;
   oneTrip = trip;
   airports = airports2;
   this->setMouseTracking(true);

}




OGLPyramid::OGLPyramid(QWidget* pwgt/*= 0*/, int *_count_planes,
                                             std::vector<Model_Struct::PlaneTTD> *plns_params2,
                                             std::vector<Model_Struct::FlightTask>  *tasks2,
                                             std::vector<Model_Struct::Airport>  *airports2,
                                             std::vector<Model_Struct::Trip> *setTrip2) : QGLWidget(pwgt)

{
     //glutMouseFunc(onMouseButton);
    pntr_cnt_planes = _count_planes;
    plns_params = plns_params2;
    tasks = tasks2;
    airports = airports2;
    setTrip = setTrip2;
    mode = 1;
    scenary_mode = 0;
}


void OGLPyramid::recalculate() {

    //������� ������ ������
    planes.clear();

    //�������������� ������ �� �����
    init_cities();


    count_planes = pntr_cnt_planes[0];
    qDebug() << "COUNT0 " << count_planes;




    int add_planes = 0;

    for(int i = 0; i < count_planes; i++) {

        int num_par = tasks[0][i].N_PlaneParam;
        int num_air_takeoff = setTrip[0][i].N_airTakeOff;
        int num_air_land = setTrip[0][i].N_airLand;


        PLANE pln;


        int N = tasks[0][i].cnt_planes_group; // ���������� ��������� � ������
        pln.PZDN.mode = tasks[0][i].mode;
        pln.PZDN.set_plane_params(plns_params[0][num_par]);

        //�������� ����������� �������� �������
        pln.PZDN.set_flight_task(tasks[0][i],
                                       airports[0][num_air_takeoff],
                                       airports[0][num_air_land],
                                       setTrip[0][i].tripPoints);
        pln.PZDN.id = QString::number(i).toStdString();


        planes.push_back(pln);
        if(N != 0) {
            add_planes += N;
        }

        for(int i = 0; i < N; i++) {
           // pln.set_random_color();
            pln.set_random_color();
            planes.push_back(pln);

        }

    }
    qDebug() << "OUT3 " << planes.size();



    int num = 0;
    for(int i = 0; i < count_planes; i++) {
        int N = tasks[0][i].cnt_planes_group + 1;
        if(N != 1) {
            for(int j = num + 1; j < N + num; j++) {
                planes[j].is_in_group = true;
                planes[j].shiftGroupAz =  tasks[0][i].shift[j - num - 1].first;
                planes[j].shiftGroupD =  tasks[0][i].shift[j - num - 1].second;
            }
            num += N;
        } else {
            num++;
        }


    }

    count_planes += add_planes;
    qDebug() << "COUNT " << count_planes;

    count_fly_planes = count_planes;


    //������ ����������� ���������, � ������ ������ ����������� � ������
    matrix_flying_planes = new int[count_planes];
    matrix_need_trip = new int[count_planes];
    //1-������� � �������� ������(��� ����������� � �� ��������, ������� ��-�� �������� ���� �� ��������)
    //0-������� ���




    for(int i = 0; i < count_planes; i++) {
        matrix_flying_planes[i] = 1;
        matrix_need_trip[i] = 1;
    }


    for(int i = 0; i < count_planes; i++) {
        planes[i].fill_PZDN_frag() ;
         qDebug() << "OUT3";
    }




    //����� ���������� ����������� �������
    killTimer(timer_id);
}




// ----------------------------------------------------------------------
/*virtual*/void OGLPyramid::initializeGL()
{


    glClearColor(sea_water_color[0], sea_water_color[1], sea_water_color[2], 1.0); // This clear the background color to black
    glShadeModel(GL_SMOOTH); // Type of shading for the polygons

     // Viewport transformation
    glViewport(0, 0, screen_width, screen_height);

    // Projection transformation
    glMatrixMode(GL_PROJECTION); // Specifies which matrix stack is the target for matrix operations
    glLoadIdentity(); // We initialize the projection matrix as identity
    gluPerspective(20.0f,(GLfloat)screen_width/(GLfloat)screen_height,10.0f,10000.0f); // We define the "viewing volume"

    glEnable(GL_DEPTH_TEST); // We enable the depth test (also called z buffer)

    move_z_increment = -600.0;
    move_x_increment = 0.0;
    move_y_increment = 0.0;

    R = 0;
    init_cities();
    id_map = draw_new_map_list();
    id_map_lines = Map.make_new_map_list();

    if(mode == 1) {


        screen_width = 800;
        screen_height = 800;
        CurTime = boost::posix_time::second_clock::local_time();
        count_planes = pntr_cnt_planes[0];





        id_plane2D = planes[0].make_plane_vertex_list();


        int add_planes = 0;
        for(int i = 0; i < count_planes; i++) {
            int num_par = tasks[0][i].N_PlaneParam;
            int num_air_takeoff = setTrip[0][i].N_airTakeOff;
            int num_air_land = setTrip[0][i].N_airLand;

            PLANE pln;


            int N = tasks[0][i].cnt_planes_group; //���������� ��������� � ������

            pln.PZDN.set_plane_params(plns_params[0][num_par]);
            pln.PZDN.set_flight_task(tasks[0][i],
                                    airports[0][num_air_takeoff],
                                    airports[0][num_air_land],
                                    setTrip[0][i].tripPoints);
            pln.PZDN.id = QString::number(i).toStdString();


            planes.push_back(pln);

            for(int i = 0; i < N; i++) {
                pln.set_random_color();
                planes.push_back(pln);

            }
            qDebug() << "OUT";
            add_planes += N;


        }


        int num = 0;
        for(int i = 0; i < count_planes; i++) {
            int N = tasks[0][i].cnt_planes_group + 1;
            if(N != 1) {
                for(int j = num + 1; j < N + num; j++) {
                    planes[j].is_in_group = true;
                    planes[j].shiftGroupAz =  tasks[0][i].shift[j - num - 1].first;
                    planes[j].shiftGroupD =  tasks[0][i].shift[j - num - 1].second;
                }
                num += N;
            } else {
                num++;
            }


        }
         count_planes += add_planes;

         count_fly_planes = count_planes;
         matrix_flying_planes = new int[count_planes];
         matrix_need_trip = new int[count_planes];

         for(int i = 0; i < count_planes; i++) {
             matrix_flying_planes[i] = 1;
             matrix_need_trip[i] = 1;
         }


        for(int i = 0; i < count_planes; i++) {
            planes[i].fill_PZDN_frag() ;
        }
    }




}

float getOGLfromPizelSize(int size) {
    float s;
    if(size < 0) {
        s = -size;
    } else {
        s = size;
    }
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX = 1, winY = 1, winZ;
    GLdouble posX, posY, posZ, posX2, posY2, posZ2;
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport);
     winY = (float)viewport[3] - (float)winY;
    glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
    //glReadPixels(winX + 50, winY + 100, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
    gluUnProject( winX + s, winY, winZ, modelview, projection, viewport, &posX2, &posY2, &posZ2);
    float R =  sqrt(pow(posX2- posX, 2) + pow(posY2- posY, 2));
    if(size < 0) return -R;
    else
    return R;
 }


 void OGLPyramid::setScales() {
     GLint viewport[4];
     GLdouble modelview[16];
     GLdouble projection[16];
     GLfloat winX = 1, winY = 1, winZ;
     GLdouble posX, posY, posZ, posX2, posY2, posZ2;
     glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
     glGetDoublev( GL_PROJECTION_MATRIX, projection );
     glGetIntegerv( GL_VIEWPORT, viewport);
       winY = (float)viewport[3] - (float)winY;
     glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
     gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
     //glReadPixels(winX + 50, winY + 100, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
     gluUnProject( winX + 10, winY, winZ, modelview, projection, viewport, &posX2, &posY2, &posZ2);
     float R =  sqrt(pow(posX2- posX, 2) + pow(posY2- posY, 2));

     scaleMove = 10.0 / R; //��������� 1-�� ������� � ����� OPENGL ����������

     for(int i = 0; i < count_planes; i++) {
        if(move_z_increment < -590) {
            planes[i].set_plane_size(R);
        } else {
           planes[i].set_plane_size(2 * R);
        }
     }

     if(move_z_increment > -590) {
          Map.size_city = R * 0.4;
      } else {
         Map.size_city = R * 0.2;
      }




 }


// ----------------------------------------------------------------------
/*virtual*/void OGLPyramid::resizeGL(int width, int height)
{
    //needDoubleMap = false;
    screen_width = width; // We obtain the new screen width values and store it
    screen_height = height; // Height value
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // We clear both the color and the depth buffer so to draw the next frame
    glViewport(0, 0, screen_width, screen_height); // Viewport transformation

    glMatrixMode(GL_PROJECTION); // Projection transformation
    glLoadIdentity(); // We initialize the projection matrix as identity
    gluPerspective(20.0f,  (GLfloat)screen_width/(GLfloat)screen_height, 10.0f, 5000.0f);

    updateGL();
}




// ----------------------------------------------------------------------
/*virtual*/ void OGLPyramid::paintGL()
{
    needDoubleMap = isNeedDoubleMap(move_x_increment);


    glClearColor(sea_water_color[0], sea_water_color[1], sea_water_color[2], 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // This clear the background color to dark blue
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW); // Modeling transformation



    glLoadIdentity(); // Initialize the model matrix as identity

    glTranslatef(0.0 + move_x_increment,0.0 + move_y_increment, -300 + move_z_increment); // We move the object forward (the model matrix is multiplied by the translation matrix)


    if(mode == 1) {
        /*for(int i = 0; i < count_planes; i++) {
            //planes[i].draw2D(planes[i].shiftX, planes[i].shiftY, planes[i].shiftZ);
            glColor3f(planes[i].color[0], planes[i].color[1], planes[i].color[2]);
            planes[i].draw_plane_pic(id_plane2D, planes[i].shiftX, planes[i].shiftY, planes[i].shiftZ);
            }*/

        //(7) ������ ���������� ���������
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);


        /*if(needDoubleMap != 0) {
            if(needDoubleMap == 1) {
                for(int i = 0; i < count_planes; i++) {

                    if(rPosX > 180)
                        rPosX -= 360;
                    qDebug() << "RPOS " << rPosX;
                    int num = planes[i].num_point - 1;
                    if(num < 0) continue;


                    if(planes[i].points[num].x  < rPosX) {
                            planes[i].points[num].x  += 360;
                    }

                }


            }

        }*/



        glPushMatrix();
        glTranslatef(0.0, 0.0, 850);

        for(int k = 0; k < count_planes; k++) {
            glColor3f(planes[k].color[0], planes[k].color[1], planes[k].color[2]);
            glBegin(GL_LINE_STRIP);

            for(int i = 0; i < planes[k].num_point; i++) {
                if(matrix_need_trip[k] == 1) {
                    Map.draw_vertex(planes[k].points[i].x, planes[k].points[i].y, planes[k].points[i].h);
                }
                planes[k].set_coords(planes[k].points[i].x, planes[k].points[i].y, planes[k].points[i].h);
                planes[k].Spd = planes[k].Plane.Speed;
                Map.get_OGL_coords(planes[k].points[i].x, planes[k].points[i].y,  planes[k].points[i].h, planes[k].shiftX, planes[k].shiftY, planes[k].shiftZ);
            }
            glEnd();
        }

        glPopMatrix();



       //
       for(uint i = 0; i < infoZones.N_Zones; i ++) {
            //������������ �����

       //(6) ������ ������ ��������
            draw_number_zone(Map.cur_zone);
       //(5) ������ �������
            //Map.drawMainSector(infoZones.Zones[0]);
            Map.drawSector(infoZones.Zones[i], 2, 2);
       }


       //(4) ����� �� ��� ��������
       glPushMatrix();
         glTranslatef(0.0, 0.0, 850);
         draw_names();
       glPopMatrix();
   }

      //(3) ������ ������� �������
      if(mode == 0) { // ���� ���� ��������� ���������� �����

          Map.draw_city(Map.cities[0]);
          for(uint i = 1; i < Map.cities.size() - 1; i++) {
              if(i == Map.num_focus_city) {
                  Map.draw_trip_point(Map.cities[i], true);
              }
              else
                  Map.draw_trip_point(Map.cities[i], false);
          }


             Map.draw_city(Map.cities[Map.cities.size() - 1]);
             glPushMatrix();
             glTranslatef(0.0, 0.0, 850);
             glBegin(GL_LINE_STRIP);
             for(uint i = 0; i < Map.cities.size(); i++) {
                 Map.draw_vertex(Map.cities[i].La, Map.cities[i].Fi, 0);
             }
             glEnd();
             glPopMatrix();


             Map.draw_place(centre1.Fi(), centre1.La(), R);
             Map.draw_place(centre2.Fi(), centre2.La(), R);
         } else {  // ���� �� ������� �����
             /*for(int i = 0; i < Map.cities.size(); i++) {
                 Map.draw_city(Map.cities[i]);
             }*/
         }


         float x, y, z;
         float x2, y2, z2;
         float sizeTexX = 360.0;
         float lefty = -90, righty = 90;


         Map.get_OGL_coords(-sizeTexX / 2, lefty, 0.0, x, y, z);
         Map.get_OGL_coords(sizeTexX / 2, righty, 0.0, x2, y2, z2);


         glPushMatrix();
         glTranslatef(0.0, 0.0, 850);
         //(2) ������ �� ��� �����
         glCallList(id_map_lines);
         //(1) ������ �������� �����
         glCallList(id_map);
         //������ �������� ��� ������������
         if(needDoubleMap != 0) {
             if(needDoubleMap == 1) {
                 glTranslatef(sizeTexX, 0.0, 0);



             }else
                 glTranslatef(-sizeTexX, 0.0, 0);
             glCallList(id_map_lines);
             glCallList(id_map);
         }

         //(0) ������ �������������, �� ������� ����� �����
         glColor3f(sea_water_color[0], sea_water_color[1], sea_water_color[2]);
         //glColor3f(0, 0, 0);
         glPopMatrix();
         glPushMatrix();
         glTranslatef(0, 0, 850);



         glBegin(GL_POLYGON);
            glVertex3f(10 * x, 10 * y, 0);
            glVertex3f(10 * x,  10 * y2, 0);
            glVertex3f(10 * x2, 10 * y2, 0);
            glVertex3f(10 * x2, 10 * y, 0);
         glEnd();
         glPopMatrix();


         if(mode == 1) {
             // ������������� ��������
              setScales();
             //(8) ������ ��������
             glPushMatrix();
             glTranslatef(0, 0, 0.001);
             for(int i = 0; i < count_planes; i++) {
                 //planes[i].draw2D(planes[i].shiftX, planes[i].shiftY, planes[i].shiftZ);
                 glColor3f(planes[i].color[0], planes[i].color[1], planes[i].color[2]);
                 planes[i].draw_plane_pic(id_plane2D, planes[i].shiftX, planes[i].shiftY, planes[i].shiftZ);
             }
             for(int i = 0; i < Map.cities.size(); i++) {
                Map.draw_city(Map.cities[i]);
             }
             glPopMatrix();
         } else {
             scaleMove = 1.0/ getOGLfromPizelSize(1);
         }



         if(mode == 1) {

              emit  info_planes_changed(set_info_planes(count_planes));
         }





    //glFlush(); // This force the execution of OpenGL commands
    //swapBuffers ();

}




//���������� ����� ���������� ����� ��������
void OGLPyramid::accept_new_trip(float a, float b) {
    City tmpCity;
    tmpCity.Fi = a;
    tmpCity.La = b;
    vector<City>::iterator it;
    it = Map.cities.end();
    it -= 1;
    Map.cities.insert(it, tmpCity);

}

// ----------------------------------------------------------------------
/*virtual*/void OGLPyramid::mousePressEvent(QMouseEvent* pe)
{
    mouse_press = true;
    last_x = pe->x();
    last_y = pe->y();

    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX = 0, winY = 0, winZ = 0;
    GLdouble posX, posY, posZ;
    GLfloat a,b,c;

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );

    winX = (float)last_x;
    winY = (float)viewport[3] - (float)last_y;

    glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
    Map.get_Geo_coords(a,b, c, posX, posY, 0);
    if(a > 180) {
        a -= 360;
    }

    if(a < -180) {
        a = a + 360;
    }




    if(mode == 1) {
        for(int i = 0; i <  planes.size(); i++) {
            if(sqrt(pow(planes[i].shiftX - posX, 2) + pow(planes[i].shiftY - posY, 2)) < 0.5) {
                matrix_need_trip[i] = 1 - matrix_need_trip[i] ;
            }
            qDebug() << planes[i].shiftX << " " << posX << "ryj54uj" << " " << sqrt(pow(planes[i].shiftX - posX, 2) + pow(planes[i].shiftX - posY, 2));
        }
    }




    if(mode == 0) {


    for(int i = 1; i <  Map.cities.size() - 1; i++) {
       float centreX, centreY, centreZ;
       Map.get_OGL_coords(Map.cities[i].La, Map.cities[i].Fi, 0, centreX, centreY, centreZ);
       if(sqrt(pow(centreX - posX, 2) + pow(centreY - posY, 2)) < 0.5) {
           Map.num_focus_city = i;
           emit change_row_trip(i);

       }
    }
    emit changeCoords(a,b);
    if(pe->button() == Qt::RightButton) {
        mouse_press = false;
        emit newTripPoint(a,b);
    }

    }
        updateGL();
}





// ----------------------------------------------------------------------
/*virtual*/void OGLPyramid::mouseMoveEvent(QMouseEvent* pe)
{
   if(mouse_press) {
       move_x_increment += (pe->x() - last_x) / scaleMove;
       move_y_increment -= (pe->y() - last_y) / scaleMove;
   }
       last_x = pe->x();
       last_y = pe->y();


   if(mode == 0) {
   GLint viewport[4];
   GLdouble modelview[16];
   GLdouble projection[16];
   GLfloat winX, winY, winZ;
   GLdouble posX, posY, posZ;
   GLdouble posX2, posY2, posZ2;
   GLfloat a,b,c;

   glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
   glGetDoublev( GL_PROJECTION_MATRIX, projection );
   glGetIntegerv( GL_VIEWPORT, viewport );

   winX = (float)last_x;
   winY = (float)viewport[3] - (float)last_y;


   glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT,  GL_FLOAT, &winZ);
   gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
   gluUnProject( winX + 10, winY, winZ, modelview, projection, viewport, &posX2, &posY2, &posZ2);


   Map.get_Geo_coords(a,b, c, posX, posY, 0);

   if(a > 180) {
       a = a - 360;
   }

   if(a < -180) {
       a = a + 360;
   }



   emit changeCoords(a,b);
   }



   updateGL();
}


void   OGLPyramid::mouseReleaseEvent(QMouseEvent*pe) {
  mouse_press = false;
}


 //--------------------------------------------------------
 /*virtual*/ void OGLPyramid::wheelEvent(QWheelEvent* pe) {
    if((pe->delta() > 0) && (move_z_increment < -570)) {
        move_z_increment += 10.0;
        SCALE_FONT += 3;
         updateGL();
        return;
    }
    if((pe->delta() < 0) && (move_z_increment > -800)) {
        move_z_increment -= 10.0;
        SCALE_FONT -= 3;
         updateGL();
        return;
    }


 }


/*virtual*/void   OGLPyramid::keyPressEvent(QKeyEvent* pe)  {

    switch (pe->key())
    {
    case Qt::Key_Plus:
        if(move_z_increment < -570) {
            move_z_increment += 10;
           SCALE_FONT += 3;

        }


         break;
     case Qt::Key_Minus:
         if(move_z_increment > -800) {
             move_z_increment -= 10;
             SCALE_FONT -= 3;

        }
         break;
     case Qt::Key_Up:
        move_y_increment -= 2.0;
         break;
    case Qt::Key_W:
         move_y_increment -= 2.0;

        break;
     case Qt::Key_Down:
         move_y_increment += 2.0;
         break;
     case Qt::Key_Left:
         move_x_increment += 2.0;
         break;
     case Qt::Key_Right:
          move_x_increment -= 2.0;
         break;
     }
         updateGL(); // ���������� �����������
     }


void  OGLPyramid::restart() {
    srand(time(NULL));
    for(int i = 0; i < count_planes; i++) {
        planes[i].num_point = 0;
    }
    killTimer( timer_id);
    recalculate();

    count_fly_planes  = count_planes;

    for(int i = 0; i < count_planes; i++) {
        matrix_flying_planes[i] = 1;
    }
        timer_id =  startTimer(0.01);
}


void  OGLPyramid::stop() {
    for(int i = 0; i < count_planes; i++) {
        planes[i].num_point = 0;
    }
    killTimer( timer_id);
}


void  OGLPyramid::_continue()  {
    killTimer( timer_id);
    timer_id = startTimer(0.01);
}

void  OGLPyramid::_pause()  {
    killTimer( timer_id);
}




/***************************************************************************
 *       ��������� ������� ����� � �������� ������� ���������              *
 *    � ������� ������ ��� �������, �������  �������� ���������� �������   *
 ***************************************************************************/

void OGLPyramid:: updateTimeState(TimeInfo &CurTime) {

    std::vector<TargetParGeo> dataParGeo;
    //���� ����� �������, �� ������� ��� ������� � ������ �������� ��������

    if((CurTime.Year == 0) &&
       (CurTime.Month == 0) &&
       (CurTime.Day == 0) &&
       (CurTime.msDay == 0)) {
       emit send_coords(CurTime,
                         dataParGeo);
       return;
    }




    for(int i = 0; i < count_planes; i++) {
        geo_coords gr;
        TargetParGeo pl;
        pl.TargetID = planes[i].Plane.id;
        pl.TargetType = planes[i].Plane.TargetType;
        pl.Target_Pos_Fi =  planes[i].Plane.PlaneC.Fi();
        pl.Target_Pos_La = planes[i].Plane.PlaneC.La();
        pl.Altitude = planes[i].Plane.Altitude;
        pl.Course = planes[i].Plane.Course;
        pl.Speed = planes[i].Plane.Speed;
        pl.Accel = planes[i].Plane.Accel;


        if(planes[i].GetPlanesGeoPar(getPtime(CurTime))) {
            dataParGeo.push_back(pl);
        }

        gr.x = planes[i].Plane.PlaneC.La();

        gr.y = planes[i].Plane.PlaneC.Fi();
        gr.h = planes[i].Plane.Altitude;


        if(planes[i].is_in_group == true) {

            double AZ =   planes[i].shiftGroupAz +  planes[i].Plane.Course;
            planes[i].Plane.PlaneC = ASh::GeoGraph::AKY(planes[i].Plane.PlaneC,
                                                    ASh::GeoGraph::LocationCoord( planes[i].shiftGroupD, AZ));
        }
        gr.x = planes[i].Plane.PlaneC.La();
        gr.y = planes[i].Plane.PlaneC.Fi();

        gr.time = boost::posix_time::to_simple_string(planes[i].Plane.pl_time.time_of_day());

        //���� ����� ������ ������ ���, �.� ��� ����� �������
        if(planes[i].Nkt != 0) {
            planes[i].points.push_back(gr);
        }


        planes[i].Nkt++;
    }


    //���������� ����� � ���������� �������� ������ �������

    emit send_coords(CurTime,
                     dataParGeo);


    for(uint i = 0; i < count_planes; i++) {
        if(planes[i].num_point == planes[i].points.size()) {
            planes[i].num_point = 0;
            planes[i].points.clear();
         }
         if(planes[i].Nkt != 1) {
             planes[i].num_point++;
         }
     }
    updateGL();


 }


//Recieve sectors and change
void  OGLPyramid::change_sectors(InpInfo _infoZones) {
    infoZones = _infoZones;
}



//********************************************************************************************
//���������� ������� �������, ���� �� ������� �� ���������� ������������� ����� ������� ������
//********************************************************************************************

/*virtual*/ void OGLPyramid:: timerEvent(QTimerEvent *) {
    double Tkt = 10;


    for(int i = 0; i < count_planes; i++) {
        geo_coords gr;
        if(planes[i].GetPlanesGeoPar(CurTime)) {
            gr.x = planes[i].Plane.PlaneC.La();
            gr.y = planes[i].Plane.PlaneC.Fi();
            gr.h = planes[i].Plane.Altitude;
            gr.time =boost::posix_time::to_simple_string(planes[i].Plane.pl_time.time_of_day());

            if(planes[i].is_in_group == true) {
                qDebug() << "������ " << planes[i].shiftGroupAz;
                double AZ =   planes[i].shiftGroupAz +  planes[i].Plane.Course;
                planes[i].Plane.PlaneC = ASh::GeoGraph::AKY(planes[i].Plane.PlaneC,
                                                        ASh::GeoGraph::LocationCoord( planes[i].shiftGroupD, AZ));
            }
            gr.x = planes[i].Plane.PlaneC.La();
            gr.y = planes[i].Plane.PlaneC.Fi();


            planes[i].points.push_back(gr);
            planes[i].num_point++;
        }
        if(planes[i].Nkt * planes[i].Tkt > planes[i].PZDN.T_end) {
            if(matrix_flying_planes[i] == 1) {
                planes[i].num_point = 0;
                count_fly_planes--;
                matrix_flying_planes[i] = 0;
                planes[i].points.clear();
            }
            if (count_fly_planes == 0) {
                stop();
            }
        }


        planes[i].Nkt++;
    }


    int Takt_s = floor(Tkt);
    int Takt_ms=(Tkt - Takt_s) * 1000;  // ������������ �����
    CurTime = CurTime + boost::posix_time::time_duration(0,0,Takt_s) + boost::posix_time::milliseconds(Takt_ms);

    if (count_fly_planes == 0) {
        stop();
    }

    updateGL();


}


