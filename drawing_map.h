#ifndef _drawing_map_h_
#define _drawing_map_h_


#include <vector>
#include <gl/GLU.h>
#include <gl/GL.h>
#include<cstdlib>
#include<stdio.h>
#include<ctime>
#include<QtGui>
#include "FLIGHT_CALCULATION/flight_plan.hpp"
#include "FLIGHT_CALCULATION/AMZGO_network_exchange.h"
#include "FLIGHT_CALCULATION/AMZGO_exchange.hpp"


using namespace std;


/***************************************************************
 *   ����� ��� ��������� ����� ����,                           *
 *   �������� � �� �����,                                     *
 *   ��������� ��������,                                       *
 *   �������,                                                  *
 *   ��� ��������� � �����������                               *
 ***************************************************************/



//��������� ����� ����
//���������� �����, ��� ���������� ���������� �������� ����� ����

struct Num_zone {
    float Fi;
    float La;
    uint num;
    Num_zone() {
        num = 0;
        Fi = 0;
        La = 0;
    }
};








struct City {
    float Fi; // �������
    float La; //������
    float color[3];
} ;





class DrawingMap {
    /*������ ������ ������� ����� �� ������ ���� */
    float stepX; 
    float stepY;
	
    float leftX; //���������� ����� ����� �� ��������
    float rightX;
    float leftY;
    float rightY;
	
    float size_texX;
    float size_texY;// �������� ������ ���� ����������� ����������
	

	
    float scaleH;





public:

    float size_city;
    int num_focus_city;

    Num_zone cur_zone;

    DrawingMap();

    vector<City> cities;

    void draw_test();
    GLuint make_new_map_list(); // ������ ����� �������� �����
    GLuint draw_new_map_list();

    void add_city(City c); // ��������� ����� ����� �� �����
    void draw_map();  // ������ �����
    void draw_city(City c); // ������ ����� �� ����e
    void draw_trip_point(City c, bool focus); //������ ���������� ����� �� �����
    void draw_cities(); // �������� ��� ������ �� �����

    void draw_place(float Fi, float La, float R); //����� ��� ������ ������� ��������� ���������� �����


    void drawMainSector(ZNK_Geometry &zona);

    //������ ������
    void drawSector(ZNK_Geometry &zona,
                                const double& Naz,
                                const int& Nkd);

    void draw_vertex(float Fi, float La, float H); // ������ �������

    void get_Geo_coords(float &La, float &Fi, float h,  // ������� ��������������� GL-��������� �
                        float x, float y, float z);     // ��������������

    void get_OGL_coords(float La, float Fi, float h,   // ������� �������������� �������������� ���������
                        float &x, float &y, float &z); // � GL-����

};

#endif	
	
	
    
