#include "planes_info_object.h"


using namespace std;
PlanesInfo::PlanesInfo() {
    isVisible = true;
    genInfo.setText("No information");

}


void PlanesInfo::set_gen_info(QString string) {
      genInfo.setText(string);
}

void PlanesInfo::set_time_info(QString string) {
      statusBarTime.showMessage(string);
}


void PlanesInfo::set_visible() {
    genInfo.setVisible(!isVisible);
    isVisible = !isVisible;
    //genInfo.setMaximumSize(0,0);
    update();
}
