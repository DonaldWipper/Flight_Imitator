 #include "main_menu.h"




MAIN_MENU::MAIN_MENU() {
    QGraphicsScene *scene = new QGraphicsScene(QRectF(-400, 400, 400, 400));
    //++++++++++++++++++++++++++++++++++++++++
    //+   ������, ������������ � ���������   +
    //++++++++++++++++++++++++++++++++++++++++


    QString strInfo =
            QString("��������� ��������� ������ ��������� ����������� ��������� ���������� �. �") +
            QString(" ���������� ��� ��� ������ 2013           ");

    QIcon icon_settings2 = QPixmap(":/images/SOURCE/settings2.png");
    play_icon = QPixmap(":/images/SOURCE/play.png");
    pause_icon = QPixmap(":/images/SOURCE/pause.png");
    stop_icon = QPixmap(":/images/SOURCE/stop.png");

    QIcon icon_plane = QPixmap(":/images/SOURCE/plane.png");
    QIcon icon_airport = QPixmap(":/images/SOURCE/airport.png");
    QIcon icon_settings = QPixmap(":/images/SOURCE/settings.png");
    QIcon icon_network = QPixmap(":/images/SOURCE/network.png");
    QIcon icon_help = QPixmap(":/images/SOURCE/help.png");



    networkTxtEdit = new QTextEdit();


    //���� � ���� � �����������
    pmnuSets = new QMenu("Settings");
    pmnuSets->setIcon(icon_settings2);

    //���� � ���� �� ��������
    pmnuHelp = new QMenu("Help");
    pmnuHelp->setIcon(icon_help);

    //������� ��� ������ ����
    bar = new QMenuBar();
    bar->addMenu(pmnuSets);
    bar->addMenu(pmnuHelp);


    txtInfo = new QTextEdit();
    QHBoxLayout* horizLayout = new QHBoxLayout();
    horizLayout->addWidget(txtInfo );
    txtInfo->setText(strInfo);
    wgtInfo = new QWidget();
    wgtInfo->setLayout(horizLayout);


    pmnuHelp->addAction(icon_help, "������", wgtInfo, SLOT(show()));


    //������ � ����������� ������
    portDlg = new PORTS_DLG(this);

    //������ �������
    client = new MY_CLIENT(portDlg->ip,
                         portDlg->client,
                         this,
                         networkTxtEdit);

    //������ �������
    server = new DATA_SENDER_SERVER(this, networkTxtEdit);
    server->setPort(portDlg->server);
    server->start();

    //����� � ���� � ���������
    info = new PlanesInfo();
    info->genInfo.setReadOnly(true);
    info->genInfo.setMinimumSize(490, 50);



    //������ � ��������
    tool_bar = new QToolBar();

    //������ ��� ������� �����
    planesEditWgt = new PARAMS_EDITOR(this);
    //������ ��� ������� ����������
    airportsDlg = new EDIT_AIRPORT_DLG(this);

    //������� ������ ��� ������� ���������
    intPlnsdlgs = new INIT_PLNS_DLG (this,
                              &planesEditWgt->plnsData,
                              &airportsDlg->airports,
                              &airportsDlg->airportMap);

    //������ � ����� OpenGL, � ������� ���������� ���������,
    //������ ��� ��������� ���������
    pView  = new MyView(scene,  &intPlnsdlgs->count_planes,
                                 &planesEditWgt->plnsData,
                                 &intPlnsdlgs->tasks,
                                 &airportsDlg->airports,
                                 &intPlnsdlgs->setTrip);


    btnMode = 0; // �� ������
    startAction = new QAction(play_icon, "�������� ���������� ������", this);
    connect(startAction, SIGNAL(triggered()), this, SLOT(clickStart()));
    stopAction = new QAction(stop_icon, "����������", this);
    connect(stopAction, SIGNAL(triggered()), this, SLOT(clickStop()));

    connect( intPlnsdlgs, SIGNAL(stop_motion()), this, SLOT(clickStop())); // ��������� ��������
    //������������� �������� ��������


    connect( intPlnsdlgs, SIGNAL(signal_change_mode(int)), pView->mpGLWidg, SLOT(set_scenary_mode(int)));

    tool_bar->addAction(startAction);
    tool_bar->addSeparator();
    tool_bar->addAction(stopAction);




    pmnuSets->addAction(icon_plane, "��������� ���������", planesEditWgt, SLOT(start()));
    pmnuSets->addAction(icon_airport, "���������", airportsDlg, SLOT(start()));
    pmnuSets->addAction(icon_settings, "��������� ���������", intPlnsdlgs, SLOT(start()));
    pmnuSets->addAction(icon_network, "��������� ������� ������", portDlg, SLOT(start()));




    //������� ��� ��������������� ��� ����� ������
    QObject::connect(portDlg, SIGNAL(change_client(int, QString)),
                    client,     SLOT(reconnect(int, QString))
                                      );

    QObject::connect(portDlg, SIGNAL(change_server(int)),
                    server,     SLOT(reconnect(int))
                                      );
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++



    QObject::connect(pView->mpGLWidg,SIGNAL(info_time_changed(QString)),
                    info,     SLOT(set_time_info(QString) )
                                      );

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+                ������� � �������                         +
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    // ��������� �����, ����� ���������� ������� ���������
    QObject::connect(client,   SIGNAL(send_time_signal(TimeInfo &)),
                    pView->mpGLWidg,     SLOT(updateTimeState(TimeInfo &))
                                      );


    //��������� ������ ��� ��������� ��� ����������� � ���������
    QObject::connect(client,   SIGNAL(send_change_zones_signal(InpInfo)),
                    pView->mpGLWidg,     SLOT(change_sectors(InpInfo))
                                      );

    //������� �������� ��������� �������
    QObject::connect(client,   SIGNAL(restart()),
                    pView->mpGLWidg,     SLOT(recalculate())
                                      );
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++







    QObject::connect(pView->mpGLWidg, SIGNAL(info_planes_changed(QString)),
                    info,     SLOT(set_gen_info(QString) )
                                      );

    QObject::connect(pView->mpGLWidg, SIGNAL(send_coords(TimeInfo, std::vector<TargetParGeo>)),
                    server,     SLOT(SendInformationToClient(TimeInfo,std::vector<TargetParGeo>))
                                      );

    QObject::connect(intPlnsdlgs, SIGNAL(refresh_task()),
                     pView,  SLOT(recalculate())
                    );






    QHBoxLayout* phbxmainLayout = new QHBoxLayout;
    QVBoxLayout* pvbxLayout = new QVBoxLayout;

    splitter = new QSplitter(Qt::Horizontal);
    splitter2 = new QSplitter(Qt::Vertical);

    pvbxLayout->addWidget(bar);
    pvbxLayout->addWidget(pView);

    pvbxLayout->addWidget(tool_bar);

    wgt2 = new QWidget();
    wgt2->setLayout(pvbxLayout);

    splitter2->addWidget(&info->genInfo);
    splitter2->addWidget(networkTxtEdit);

    splitter->addWidget(wgt2);
    splitter->addWidget(splitter2);

    phbxmainLayout->addWidget(splitter);

    this->setLayout(phbxmainLayout);


}


void MAIN_MENU::start() {
    this->show();
}

