#ifndef PORTSDLG_H
#define PORTSDLG_H

#include <QtGui>

class  PORTS_DLG : public QDialog {
    Q_OBJECT

private:
    QLineEdit* m_ptxtServerPort;
    QLineEdit* m_ptxtClientPort;
    QLineEdit*  m_ptxtIP;

public:
    PORTS_DLG (QWidget* pwgt = 0);
    QString serverPort() const;
    QString clientPort() const;
    QString IP() const;

    QSettings *settings;

    int client;
    int server;
    QString ip;

public slots:
    void start();
    void save_datas();
signals:
    void change_client(int port, QString);
    void change_server(int server);
};






#endif // PORTSDLG_H
