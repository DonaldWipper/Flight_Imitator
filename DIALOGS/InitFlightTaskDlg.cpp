#include "InitFlightTaskDlg.h"
#include <qDebug>


void  INIT_FLIGHT_TASK_DLG::add_new() {
    std::vector<Model_Struct::BillPoint> marsh;
    setMarsh.push_back(marsh);
    tripDlg.start(&setMarsh[setMarsh.size() - 1]);
    boxNumSetPoints.addItem(QString::number(setMarsh.size() - 1));
    reinit_files();
}

void  INIT_FLIGHT_TASK_DLG::delete_param() {
     sltNum = boxNumSetPoints.currentIndex();
     std::vector < std::vector<Model_Struct::BillPoint> >::iterator it;
     it = setMarsh.begin();
     it += sltNum;
     setMarsh.erase(it);
     reinit_files();
     boxNumSetPoints.removeItem(sltNum);
}


void  INIT_FLIGHT_TASK_DLG::edit_params() {
    sltNum = boxNumSetPoints.currentIndex();
    tripDlg.start(&setMarsh[sltNum]);
}



void INIT_FLIGHT_TASK_DLG::reinit_files() {
    std::ofstream output("FILES/LIST_COUNT_SET_&_COUNT_MARSH_POINTS_TASK.txt");

    std::ofstream output2("FILES/MARSH_POINTS_SET.txt");


    output << setMarsh.size() << std::endl;

    for(int i = 0; i < setMarsh.size(); i++) {
        output << setMarsh[i].size() << std::endl;
    }
    output.close();

    for(int i = 0; i < setMarsh.size(); i++) {
        for(int j = 0; j < setMarsh[i].size(); j++) {
            output2 << setMarsh[i][j];
        }
    }
    output2.close();
}





void INIT_FLIGHT_TASK_DLG::init_main_vector(const char *file) {
    std::ifstream input(file);
    if(!input) {
        std::string str_file_name(file);
        std::ofstream output("log_errors.txt");
        output << "Error of openning file " << str_file_name << std::endl;
        output.close();
        return;
    }


    //Count different set of path
    input >> count_params;
    int size;


    for(int i = 0; i < count_params; i++){
        input >> size;
        std::vector<Model_Struct::BillPoint> marsh(size);
        setMarsh.push_back(marsh);

    }
    input.close();
}

void INIT_FLIGHT_TASK_DLG::init_tasks_from_file(const char *file) {
    qDebug() << "In function";
    std::ifstream input(file);
    if(!input) {
        std::string str_file_name(file);
        std::ofstream output("log_errors.txt");
        output << "Error of openning file " << str_file_name << std::endl;
        output.close();
    }




        qDebug() << "--------------------";
        for(int i = 0; i < setMarsh.size(); i++) {
            for(int j = 0; j < setMarsh[i].size(); j++) {
               input >> setMarsh[i][j];
            }
        }
        qDebug() << "--------------------";


    input.close();
}





INIT_FLIGHT_TASK_DLG::INIT_FLIGHT_TASK_DLG() {





    init_main_vector("FILES/LIST_COUNT_SET_&_COUNT_MARSH_POINTS_TASK.txt");
    init_tasks_from_file("FILES/MARSH_POINTS_SET.txt");



    btnEdit.setText("Edit");
    btnDelete.setText("Delete");
    btnAdd.setText("Add new");



    //Call next dialog




    QObject::connect(&btnEdit, SIGNAL(clicked()),
                     this, SLOT(edit_params())
                     );

    QObject::connect(&btnAdd, SIGNAL(clicked()),
                     this, SLOT(add_new())
                     );


    QObject::connect(&btnDelete, SIGNAL(clicked()),
                     this, SLOT(delete_param())
                     );

    QObject::connect(&tripDlg, SIGNAL(reinit()),
                     this, SLOT(reinit_files())
                     );



    QStringList lst_num_task;

    for(int i = 0; i < count_params; i++) {
        lst_num_task << QString::number(i);
    }

    boxNumSetPoints.addItems(lst_num_task);
    sltNum = boxNumSetPoints.currentIndex();

    QHBoxLayout* phbxLayout = new QHBoxLayout;
    phbxLayout->addWidget(&boxNumSetPoints);
    phbxLayout->addWidget(&btnEdit);
    phbxLayout->addWidget(&btnDelete);
    phbxLayout->addWidget(&btnAdd);
    setLayout(phbxLayout);



}


void INIT_FLIGHT_TASK_DLG::start() {
    this->show();
}
