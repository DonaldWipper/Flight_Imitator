#ifndef INITFLIGHTTASKDLG_H
#define INITFLIGHTTASKDLG_H



#include <QtGui>
#include <vector>
#include <fstream>
#include "ListTripPointsDlg.h"
#include "../FLIGHT_CALCULATION/flight_plan.hpp"


typedef std::vector<Model_Struct::BillPoint> TRIP;


class INIT_FLIGHT_TASK_DLG : public QWidget {
    Q_OBJECT
private:

    QComboBox   boxNumSetPoints;
    QPushButton btnEdit;
    QPushButton btnDelete;
    QPushButton btnAdd;


    //Dialog with trip points list




    void init_tasks_from_file(const char *file);
    void init_main_vector(const char *file);


public:

    int sltNum;
    int count_params;

    //Vector with set of vectors, which are points path for each set

    std::vector < TRIP > setMarsh;
    INIT_FLIGHT_TASK_DLG();



public slots:


    //void setNumRow(QListWidgetItem * item);
    void delete_param();
    void edit_params();
    void add_new();
    void reinit_files();
    //void add_new();
    //void emit_signal_for_next_dialog();
    void start();
signals:



};













#endif // INITFLIGHTTASKDLG_H


