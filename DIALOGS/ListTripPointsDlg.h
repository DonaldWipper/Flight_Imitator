
#ifndef LISTTRIPPOINTSDLG_H
#define LISTTRIPPOINTSDLG_H

#include <QtGui>
#include <QDebug>
#include <vector>
#include <fstream>

#include "../FLIGHT_CALCULATION/flight_plan.hpp"
#include "../WIDGETS/GeoCoordsWidget.h"






//Dialog for add and edit trip point
class EDIT_TRIP_POINT_DLG : public QDialog {
    Q_OBJECT
private:
    QLineEdit* m_ptxtNamePoint;
    GeoCoordsWidget* m_ptxtLa;
    GeoCoordsWidget* m_ptxtFi;
    QLineEdit* m_ptxtNrot;
    QPushButton btnOk;
    QRadioButton *btnMoveLinear;
    QRadioButton *btnMoveRound;
    QLabel *lblAcceleration;
    QCheckBox *boxAcceleration;

    QLabel *lblSpeedEnd;
    QLineEdit *m_ptxtlblSpeedEnd;
    double V_max;

public:

   EDIT_TRIP_POINT_DLG(QWidget* pwgt = 0);

   Model_Struct::BillPoint *ptrPoint;

protected:



public slots:
   //void show_html_info();
   //void show_edit_info();
   void start(Model_Struct::BillPoint *ptrPoint2, double V_max2);
   void save_param();
   void setRoundMotion();
   void setLinearMotion();
   void setEnableSpeedSet(int state);
signals:
   void reinit();
   void add_new(Model_Struct::BillPoint pnt);
};

















#endif // LISTTRIPPOINTSDLG_H
