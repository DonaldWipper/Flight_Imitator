#include "PlaneParametersDialog.h"

PlaneDialog::PlaneDialog(QWidget* pwgt/*= 0*/)
     : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    this->setWindowTitle(QString("������ ����"));
    m_ptxtName =  new QLineEdit;
    m_ptxtL_razb =  new QLineEdit;
    m_ptxtV_otr  =  new QLineEdit;
    m_ptxtV_evol  =  new QLineEdit;
    m_ptxtV_cruise  =  new QLineEdit;
    m_ptxtV_climb  =  new QLineEdit;
    m_ptxtV_descend  =  new QLineEdit;
    m_ptxtV_max  =  new QLineEdit;
    m_ptxtA_dog  =  new QLineEdit;
    m_ptxtA_krug  =  new QLineEdit;
    m_ptxtA_pos  =  new QLineEdit;
    m_ptxtHmax = new QLineEdit;
}








void PlaneDialog::send_info() {

    PLN[0].TargetType = Name().toStdString();
    PLN[0].L_razb = L_razb().toDouble();
    PLN[0].V_otr = V_otr().toDouble() / 3.6;
    PLN[0].V_evol = V_evol().toDouble() / 3.6;
    PLN[0].V_cruise = V_cruise().toDouble() / 3.6;
    PLN[0].V_climb = V_climb().toDouble();
    PLN[0].V_descend = V_descend().toDouble();
    PLN[0].V_max = V_max().toDouble() / 3.6;


    PLN[0].acc_dog = acc_dog().toDouble();
    PLN[0].acc_krug = acc_krug().toDouble();
    PLN[0].acc_pos = acc_pos().toDouble();
    PLN[0].Hmax = H_max().toDouble();

    //�� ��������� => �������� 2 �������
    //������ ����������� ������ ������ �������

    emit reinit(); // ���� �� ������������� ���� => �� ����������� ���� ���������
    emit add_new(PLN[0]); // ��������� �����? �� ��� � �������



    this->accept();

}




void PlaneDialog::show_edit_table(Model_Struct::PlaneTTD *pln) {
     QLabel* plblName      = new QLabel("��� ��������");
     QLabel* plblL_razb    = new QLabel("����� ������� (�)");
     QLabel* plblV_otr     = new QLabel("�������� ������ (�� / �)");
     QLabel* plblV_evol    = new QLabel("����������� �������� (�� / �)");
     QLabel* plblL_cruise  = new QLabel("����������� �������� (�� / �)");
     QLabel* plblV_climb   = new QLabel("���������������� (� / �)");
     QLabel* plblV_descend = new QLabel("����������������� (� / �)");
     QLabel* plblV_max     = new QLabel("������������ �������� (�� / �)");
     QLabel* plblA_dog     = new QLabel("��������� ������ (� / �^2)");
     QLabel* plblA_rot     = new QLabel("�������� ��������� (� / �^2)");
     QLabel* plblA_pos     = new QLabel("��������� ��� ��������  �� ����� (� / �^2)");
     QLabel* plblH_max     = new QLabel("������������ ������ (�)");
     /*plblName->setBuddy(m_ptxtName);
      plblL_razb->setBuddy(m_ptxtL_razb);*/

     // ������������

     PLN = pln;


     QPushButton* pcmdOk     = new QPushButton("&Ok");

     connect(pcmdOk, SIGNAL(clicked()), SLOT(send_info()));





     //Layout setup

     QGridLayout* ptopLayout = new QGridLayout;
     ptopLayout->addWidget(plblName, 0, 0);
     ptopLayout->addWidget(plblL_razb, 1, 0);
     ptopLayout->addWidget( plblV_otr, 2, 0);
     ptopLayout->addWidget(plblV_evol, 3, 0);
     ptopLayout->addWidget(plblL_cruise, 4, 0);
     ptopLayout->addWidget(plblV_max , 5, 0);
     ptopLayout->addWidget(plblV_climb, 6, 0);
     ptopLayout->addWidget(plblV_descend , 7, 0);

     ptopLayout->addWidget(plblA_dog , 8, 0);
     ptopLayout->addWidget(plblA_rot  , 9, 0);
     //ptopLayout->addWidget(plblA_pos, 9, 0);
     ptopLayout->addWidget(plblH_max, 10, 0);






     m_ptxtName->setText(QString::fromStdString(pln[0].TargetType));
     m_ptxtL_razb->setText(QString::number(pln[0].L_razb));
     m_ptxtL_razb->setValidator(new QDoubleValidator());

     m_ptxtV_otr->setText(QString::number(pln[0].V_otr * 3.6));
     m_ptxtV_otr->setValidator(new QDoubleValidator());

     m_ptxtV_evol->setText(QString::number(pln[0].V_evol * 3.6));
     m_ptxtV_evol->setValidator(new QDoubleValidator());

     m_ptxtV_cruise->setText(QString::number(pln[0].V_cruise * 3.6));
     m_ptxtV_cruise->setValidator(new QDoubleValidator());

     m_ptxtV_climb->setText(QString::number(pln[0].V_climb));
     m_ptxtV_climb->setValidator(new QDoubleValidator());

     m_ptxtV_descend->setText(QString::number(pln[0].V_descend));
     m_ptxtV_descend->setValidator(new QDoubleValidator());

     m_ptxtV_max->setText(QString::number(pln[0].V_max * 3.6));
     m_ptxtV_max->setValidator(new QDoubleValidator());

     m_ptxtA_dog->setText(QString::number(pln[0].acc_dog));
     m_ptxtA_dog->setValidator(new QDoubleValidator());

     m_ptxtA_krug->setText(QString::number(pln[0].acc_krug));
     m_ptxtA_krug->setValidator(new QDoubleValidator());

     m_ptxtA_pos->setText(QString::number(pln[0].acc_pos));
     m_ptxtA_pos->setValidator(new QDoubleValidator());

     m_ptxtHmax->setText(QString::number(pln[0].Hmax));
     m_ptxtHmax->setValidator(new QDoubleValidator());




     ptopLayout->addWidget(  m_ptxtName, 0, 1);
     ptopLayout->addWidget( m_ptxtL_razb , 1, 1);
     ptopLayout->addWidget(  m_ptxtV_otr , 2, 1);
     ptopLayout->addWidget(  m_ptxtV_evol, 3, 1);
     ptopLayout->addWidget( m_ptxtV_cruise, 4, 1);
     ptopLayout->addWidget( m_ptxtV_max, 5, 1);
     ptopLayout->addWidget( m_ptxtV_climb, 6, 1);
     ptopLayout->addWidget( m_ptxtV_descend, 7, 1);

     ptopLayout->addWidget( m_ptxtA_dog, 8, 1);
     ptopLayout->addWidget( m_ptxtA_krug, 9, 1);
     //ptopLayout->addWidget( m_ptxtA_pos, 10, 1);
     ptopLayout->addWidget( m_ptxtHmax, 10, 1);

     QVBoxLayout* pvbxLayout = new QVBoxLayout;
     QHBoxLayout* phbxLayout = new QHBoxLayout;
     pcmdOk->setFixedSize(80, 20);
     phbxLayout->addWidget(pcmdOk);
     pvbxLayout->addLayout(ptopLayout);
     pvbxLayout->addLayout(phbxLayout);

     setLayout(pvbxLayout);
     this->exec();

}




void PlaneDialog::start_dialog() {
    this->exec();


}





// ----------------------------------------------------------------------
QString PlaneDialog::Name() const
{
    return  m_ptxtName->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::L_razb() const
{
    return  m_ptxtL_razb->text();
}



// ----------------------------------------------------------------------
QString PlaneDialog::V_otr() const
{
    return  m_ptxtV_otr->text();
}


// ----------------------------------------------------------------------
QString PlaneDialog::V_evol() const
{
    return  m_ptxtV_evol->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::V_cruise() const
{
    return  m_ptxtV_cruise->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::V_climb() const
{
    return  m_ptxtV_climb->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::V_descend() const
{
    return  m_ptxtV_descend->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::acc_dog() const
{
    return  m_ptxtA_dog->text();
}


// ----------------------------------------------------------------------
QString PlaneDialog::acc_krug() const
{
    return  m_ptxtA_krug->text();
}

// ----------------------------------------------------------------------
QString PlaneDialog::acc_pos() const
{
    return  m_ptxtA_pos->text();
}
//-----------------------------------------
QString PlaneDialog::V_max () const
{
    return m_ptxtV_max->text();
}


//-----------------------------------------
QString PlaneDialog::H_max () const
{
    return m_ptxtHmax->text();
}
