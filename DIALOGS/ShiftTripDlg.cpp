#include "ShiftTripDlg.h"

SHIFT_TRIP_DLG::SHIFT_TRIP_DLG(QWidget* pwgt/*= 0*/,
                               Model_Struct::FlightTask &task)
     : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
     this->setWindowTitle(QString("������ c������� ������������ �������� �������� � ������"));
     az = 0.0;
     dist = 0.0;

     ptrTask = &task;
     task2 = task;

     m_ptxtAz = new QLineEdit;
     m_ptxtAz->setFixedWidth(25);
     m_ptxtDist = new QLineEdit;
     m_ptxtDist->setFixedWidth(25);
     if(task.cnt_planes_group  != 0) {
         m_ptxtAz->setText(QString::number(task.shift[0].first));
         m_ptxtDist->setText(QString::number(task.shift[0].second));
     } else {
         m_ptxtAz->setReadOnly(true);
         m_ptxtDist->setReadOnly(true);
     }







     m_ptxtCountPlanes = new QLineEdit;
     m_ptxtCountPlanes->setText(QString::number(task.cnt_planes_group + 1));

     mBoxNumPlane = new QComboBox();
     QStringList lst;
     for(int i = 0; i < task.cnt_planes_group; i++) {
         lst << QString::number(i + 1);
     }
     mBoxNumPlane->addItems(lst);
     cur_num = 0;
     mBoxNumPlane->setCurrentIndex(cur_num);

     //��� ��������� ������ � ������� ��� ����� �������� ������
     QObject::connect(m_ptxtAz, SIGNAL(textEdited(QString)),this, SLOT(changeParams()));
     QObject::connect(m_ptxtDist, SIGNAL(textEdited(QString)),this, SLOT(changeParams()));

     //��� ��������� ���������� ���������� ������ ���������
     QObject::connect(m_ptxtCountPlanes, SIGNAL(textEdited(QString)),this, SLOT(changeCount()));
     QObject::connect(mBoxNumPlane, SIGNAL(currentIndexChanged(int)),this, SLOT(setParams()));


     mBoxNumPlane->setFixedSize(40, 20);


     QLabel* plblCounts     = new QLabel("���������� ��������� � ������");
     QLabel* plblAz     = new QLabel("����������� ������(�������)");
     QLabel* plblDist     = new QLabel("���������� ������(��)");
     QHBoxLayout* horLayout0 = new QHBoxLayout;
     QHBoxLayout* horLayout = new QHBoxLayout;
     QVBoxLayout* verLayout = new QVBoxLayout;

     horLayout->addWidget(mBoxNumPlane);
     horLayout->addWidget(plblAz);
     horLayout->addWidget(m_ptxtAz);
     horLayout->addWidget(plblDist);
     horLayout->addWidget(m_ptxtDist);


     horLayout0->addWidget(plblCounts);
     horLayout0->addWidget(m_ptxtCountPlanes);
     //horLayout0->addWidget(btnCountOk);

     verLayout->addLayout(horLayout0);
     verLayout->addLayout(horLayout);

     QPushButton* pcmdOk     = new QPushButton("OK");
     connect(pcmdOk, SIGNAL(clicked()), SLOT(apply()));
     pcmdOk->setFixedWidth(25);

     verLayout->addWidget(pcmdOk);





     setLayout(verLayout);
 }

void SHIFT_TRIP_DLG::changeCount() {
   qDebug() << "DSFSSG";

   int N;
   if(m_ptxtCountPlanes->text() != "") {
       N = m_ptxtCountPlanes->text().toInt() - 1;
       qDebug() << "N " << N;
   } else {
       return;
   }

   if(N < 0) {
       QMessageBox msgBox;
       msgBox.setText("���������� ��������� �� ����� ���� < 1");
       msgBox.exec();
       m_ptxtCountPlanes->setText(QString::number(task2.cnt_planes_group + 1));
       return;
   }
   if(N == 0) {
       m_ptxtAz->setReadOnly(true);
       m_ptxtDist->setReadOnly(true);
   } else {
       m_ptxtAz->setReadOnly(false);
       m_ptxtDist->setReadOnly(false);
   }
   mBoxNumPlane->clear();
   QStringList lst;
   for(int i = 0; i < N; i++) {
       lst << QString::number(i + 1);
   }
   std::vector< std::pair<double, double> >::iterator it;

   it = task2.shift.begin();
   if(N <= task2.cnt_planes_group) {
       it += N;
       for(int i = 0; i < task2.cnt_planes_group - N; i++) {
           task2.shift.erase(it);
       }
   } else {
       for(int i = 0; i < N - task2.cnt_planes_group; i++) {
           std::pair<double, double> tmp;
           tmp.first = 0;
           tmp.second = 0;
           task2.shift.push_back(tmp);
       }
   }
   task2.cnt_planes_group = N;
   mBoxNumPlane->addItems(lst);
   if(N == 0) {
       m_ptxtDist->setText("");
       m_ptxtAz->setText("");
   }
   qDebug() << "HELP";
}



void SHIFT_TRIP_DLG::changeParams() {
    cur_num = mBoxNumPlane->currentIndex();
    task2.shift[cur_num].first = Az().toDouble();
    task2.shift[cur_num].second = Dist().toDouble();
}

void SHIFT_TRIP_DLG::setParams() {
    cur_num = mBoxNumPlane->currentIndex();
    qDebug() << "SECOND " << task2.shift[cur_num].second;
    m_ptxtAz->setText(QString::number(task2.shift[cur_num].first));
    m_ptxtDist->setText(QString::number(task2.shift[cur_num].second));
     qDebug() <<  m_ptxtDist->text();

}




void SHIFT_TRIP_DLG::apply() {
    az = Az().toDouble();
    dist = Dist().toDouble();
    ptrTask[0] = task2;
    this->accept();
}

void SHIFT_TRIP_DLG::start_dialog() {
    this->exec();
}

QString SHIFT_TRIP_DLG::Az()  {
    return m_ptxtAz->text();
}

QString SHIFT_TRIP_DLG::Dist()  {
    return m_ptxtDist->text();
}

