

#include "PortsDlg.h"

PORTS_DLG::PORTS_DLG (QWidget* pwgt /*= 0*/)
    : QDialog(pwgt)
{

     this->setWindowTitle(QString("������ ip-����� � �����"));

    settings =  new QSettings("settings.ini",QSettings::IniFormat);
    client =  settings->value("client", 54325).toInt();
    server =  settings->value("server", 54321).toInt();
    ip = settings->value("ip", "localhost").toString();


    m_ptxtServerPort =  new QLineEdit;
    m_ptxtClientPort =  new QLineEdit;
    m_ptxtIP = new QLineEdit;

    //���������� ��������� ��� ip
    QRegExp HostNameReg("(\\b(([01]?\\d?\\d|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d?\\d|2[0-4]\\d|25[0-5])\\b|localhost)");
    m_ptxtIP->setValidator(new QRegExpValidator(HostNameReg, this));




    QLabel* plblServerPort    = new QLabel("���");
    QLabel* plblClientPort    = new QLabel("����");


    QPushButton* pcmdOk     = new QPushButton("&Ok");
    QPushButton* pcmdCancel = new QPushButton("&Cancel");

    QObject::connect(pcmdOk, SIGNAL(clicked()),
                    this,     SLOT(save_datas())
                                      );

    QObject::connect(pcmdCancel, SIGNAL(clicked()),
                    this,     SLOT(reject())
                                      );


    QGridLayout* ptopLayout = new QGridLayout;


    ptopLayout->addWidget(plblServerPort, 0, 0);
    ptopLayout->addWidget(m_ptxtServerPort, 1, 0);
    ptopLayout->addWidget(plblClientPort, 2, 0);
    ptopLayout->addWidget(m_ptxtIP, 3, 0);
    ptopLayout->addWidget(m_ptxtClientPort, 3, 1);



    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    QHBoxLayout* phbxLayout = new QHBoxLayout;

    phbxLayout->addWidget(pcmdOk);
    phbxLayout->addWidget(pcmdCancel);
    pvbxLayout->addLayout(ptopLayout);
    pvbxLayout->addLayout(phbxLayout);

    setLayout(pvbxLayout);
}


void PORTS_DLG::save_datas() {
    client = m_ptxtClientPort->text().toInt();
    server = m_ptxtServerPort->text().toInt();
    ip = m_ptxtIP->text();
    settings->setValue("client", client);
    settings->setValue("server", server);
    settings->setValue("ip", ip);
    emit change_client(client, ip);
    emit change_server(server);
    this->accept();

}


void PORTS_DLG::start() {
    m_ptxtServerPort->setText(QString::number(server));
    m_ptxtClientPort->setText(QString::number(client));
    m_ptxtIP->setText(ip);
    this->exec();
}
