#ifndef PLANES_PARAMS_EDIT_H
#define PLANES_PARAMS_EDIT_H

#endif // PLANES_PARAMS_EDIT_H


#include <QtGui>
#include <QDebug>
#include <vector>
#include <iostream>
#include <fstream>

#include "../FLIGHT_CALCULATION/flight_plan.hpp"



/**************************************************************************
 *     ����� ��� ����������, �������� � �������������� ���������� �����   *
 **************************************************************************/


class PARAMS_EDITOR : public QDialog {
    Q_OBJECT
private:
    QListWidget listNameParams;



    QToolBar* bar;




    void init_plns_data_from_file(QString file_name);  //���������������� ������� �� �����
    void change_params_file(QString file_name);        //������������ ����, ����� ��������� ���������


public:
    int sltRow;
    int count_params;


    std::vector<Model_Struct::PlaneTTD> plnsData;

    PARAMS_EDITOR(QWidget* pwgt = 0);
public slots:
    void setNumRow(QListWidgetItem * item);
    void delete_param();
    void edit_params();
    void add_new(Model_Struct::PlaneTTD plnData);
    void add_new();
    void emit_signal_for_next_dialog();
    void start();
signals:
    void send_sgnl_dlg_info(Model_Struct::PlaneTTD);

};
