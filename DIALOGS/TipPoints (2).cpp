
#include "TripPointsDlg.h"
#include <QDebug>



void TRIP_POINTS_DLG::set_new_airport_takeoff(int num_air) {
    tmpTrip.N_airTakeOff = num_air;
     pViewMap->init_cities();
}


void TRIP_POINTS_DLG::set_new_airport_land(int num_air) {
    tmpTrip.N_airLand = num_air;
    pViewMap->init_cities();
}

void TRIP_POINTS_DLG::set_row_trip(int num_trip) {
     listNameTripPoints.setCurrentRow(num_trip - 1);
}


void TRIP_POINTS_DLG::reinit_list_widget() {
    int num = listNameTripPoints.currentRow();
    listNameTripPoints.item(num)->setText(QString::fromStdString(tmpTrip.tripPoints[num].name));
    listNameTripPoints.update();
}

void TRIP_POINTS_DLG::add_new( Model_Struct::BillPoint tmpPnt) {
    tmpTrip.tripPoints.push_back(tmpPnt);
    listNameTripPoints.addItem(QString::fromStdString(tmpPnt.name));
    emit draw_trip_point(tmpPnt.P_geo.Fi(), tmpPnt.P_geo.La());
    pViewMap->init_cities();
}



void TRIP_POINTS_DLG::set_on_the_map() {
    pViewMap->setVisible(true);
    lblCoords->setVisible(true);
}

//--------------------------------------------------------------------------------
std::string TRIP_POINTS_DLG::get_trip_name_from_string(std::string full_name) {
    int pos = full_name.find(".txt");
    if(pos == std::string::npos) {
        return "";
    }
    char c = 'a';
    std::string substr = "";
    pos--;
    c = full_name[pos];
    while (c != '/') {
       substr = c + substr;
       pos--;
       c = full_name[pos];
    }
    return substr;
}
//-----------------------------------------------------------------------------------





void TRIP_POINTS_DLG::edit_point() {
    if(tmpTrip.tripPoints.size() == 0) return;
    int num = listNameTripPoints.currentRow();
    EDIT_TRIP_POINT_DLG editTrpPoint(this);
    QObject::connect(&editTrpPoint, SIGNAL(reinit()), this, SLOT(reinit_list_widget()));
    editTrpPoint.start(&tmpTrip.tripPoints[num]);

}


void TRIP_POINTS_DLG::add_point() {
    Model_Struct::BillPoint tmpPnt;
    EDIT_TRIP_POINT_DLG editTrpPoint(this);
    QObject::connect(&editTrpPoint, SIGNAL(add_new(Model_Struct::BillPoint)), this, SLOT(add_new(Model_Struct::BillPoint)));
     editTrpPoint.start(&tmpPnt);

}


void TRIP_POINTS_DLG::add_point(float La, float Fi) {
    Model_Struct::BillPoint tmpPnt;
    tmpPnt.P_geo.setFi(Fi);
    tmpPnt.P_geo.setLa(La);
    EDIT_TRIP_POINT_DLG editTrpPoint(this);
    QObject::connect(&editTrpPoint, SIGNAL(add_new(Model_Struct::BillPoint)), this, SLOT(add_new(Model_Struct::BillPoint)));
     editTrpPoint.start(&tmpPnt);
}



void TRIP_POINTS_DLG::delete_point() {
    if(tmpTrip.tripPoints.size() == 0) return;
    int sltRow = listNameTripPoints.currentRow();

    std::vector<Model_Struct::BillPoint>::iterator it_v;
    it_v = tmpTrip.tripPoints.begin();
    it_v += sltRow;
    tmpTrip.tripPoints.erase(it_v);
    listNameTripPoints.takeItem(sltRow);
    pViewMap->init_cities();
}


//-----------------------------------------------------


void TRIP_POINTS_DLG::apply_datas() {
    QString file_name = "FILES/trips/" + QString::fromStdString(tmpName) + ".txt";
    QByteArray ba = file_name.toLatin1();
    char *Fname = ba.data();
    std::ofstream output(Fname);
    if(!output) {
        std::string str_file_name(Fname);
        std::ofstream output2("log_errors.txt", std::ios::app);
        output2 << "Error of openning file " << str_file_name << std::endl;
        output2.close();
        return;
    } else {
        output << tmpTrip;
    }
    trip[0] = tmpTrip;
    name[0] = tmpName;
    emit reinit_datas();
}

//-----------------------------------------------------


void TRIP_POINTS_DLG::save_trip() {
    QString file_name;
    file_name = QFileDialog::getSaveFileName(this, "Enter trip's file's name", "FILES/trips", "*.txt");
    const char *name = file_name.toStdString().c_str();
    if(file_name == "") {
        return;
    }

    std::ofstream output(name);
    output << tmpTrip;
    output.close();

}


void TRIP_POINTS_DLG::load_trip() {
    QString file_name;
    file_name = QFileDialog::getOpenFileName(this, "Select trip's file's name", "FILES/trips", "*.txt");
    qDebug() << file_name;



    if(file_name == "") {
        return;
    }

    std::string short_name = get_trip_name_from_string(file_name.toStdString());
    nameTrip->setText(QString::fromStdString(short_name));
    tmpName = short_name;

    const char *name = file_name.toStdString().c_str();


    std::ifstream input(name);
    if(!input) {
        std::string str_file_name(name);
        std::ofstream output("log_errors.txt", std::ios::app);
        output << "Error of openning file " << str_file_name << std::endl;
        output.close();
        return;
    } else {
        tmpTrip.tripPoints.clear();
        input >> tmpTrip;
        init_from_trip(tmpTrip);
        boxChoseAirTakeOff->setCurrentIndex(tmpTrip.N_airTakeOff);
        boxChoseAirLand->setCurrentIndex(tmpTrip.N_airLand);
        input.close();

    }
     pViewMap->init_cities();
}


void TRIP_POINTS_DLG::setCurCoords(float La, float Fi) {
    QString info;
    info = "������� ���������� ������  ";
    info += QString::number(La);
    info += ", �������  ";
    info += QString::number(Fi);
    lblCoords->setText(info);
}




void TRIP_POINTS_DLG::init_from_trip(Model_Struct::Trip trip) {
    QStringList lstAirports;
    for(int i = 0; i < airports[0].size(); i++) {
        lstAirports << QString::fromStdString(airports[0][i].name);
    }
    listNameTripPoints.clear();


    for(int i = 0; i < trip.tripPoints.size(); i++) {
        listNameTripPoints.addItem(QString::fromStdString(trip.tripPoints[i].name));
    }


    QComboBox* boxChoseAirTakeOff = new QComboBox();
    QComboBox* boxChoseAirLand = new QComboBox();

    boxChoseAirTakeOff->setCurrentIndex(trip.N_airTakeOff);
    boxChoseAirLand->setCurrentIndex(trip.N_airLand);
    listNameTripPoints.setCurrentRow(0);
    listNameTripPoints.update();
}





TRIP_POINTS_DLG::TRIP_POINTS_DLG(QWidget* pwgt,
                                 std::vector <Model_Struct::Airport> *_airports,
                                 std::string *_name,
                                 Model_Struct::Trip *_trip)
    : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{

    this->setWindowTitle(QString("������ �������"));

    name = _name;
    airports = _airports;
    trip = _trip;


    tmpTrip = trip[0];
    tmpName = name[0];


    nameTrip = new QLabel(QString::fromStdString(tmpName));



    QStringList lstAirports;
    for(int i = 0; i < airports[0].size(); i++) {
        lstAirports << QString::fromStdString(airports[0][i].name);
    }


    for(int i = 0; i < tmpTrip.tripPoints.size(); i++) {
        listNameTripPoints.addItem(QString::fromStdString(tmpTrip.tripPoints[i].name));
    }




    bar = new QToolBar;

    QIcon icon_add = QPixmap(":/images/SOURCE/Add.png");
    QIcon icon_save = QPixmap(":/images/SOURCE/save.png");
    QIcon icon_load = QPixmap(":/images/SOURCE/open.png");
    QIcon icon_edit = QPixmap(":/images/SOURCE/edit.png");
    QIcon icon_delete = QPixmap(":/images/SOURCE/delete.png");

    bar->addAction(icon_add, "�������� ����� ����� �������", this, SLOT(add_point()));
    bar->addSeparator();


    bar->addAction( icon_edit, "������������� ���������� �����", this, SLOT(edit_point()));
    bar->addSeparator();

    bar->addAction(icon_delete, "������� ���������� �����", this, SLOT(delete_point()));
    bar->addSeparator();


    bar->addAction( icon_save, "��������� �������", this, SLOT(save_trip()));
    bar->addSeparator();

    bar->addAction( icon_load, "��������� �������", this, SLOT(load_trip()));






    pViewMap  = new OGLPyramid(this, &tmpTrip, airports);
    pViewMap->setMinimumSize(300, 200);
    pViewMap->setVisible(false);


   // QPushButton *btn


    boxChoseAirTakeOff = new QComboBox();
    boxChoseAirLand = new QComboBox();

    listNameTripPoints.setCurrentRow(0);


    QLabel *lblAirportTakeOff = new QLabel("�������� ������");
    QLabel *lblAirportLand = new QLabel("�������� �������");

    lblCoords = new QLabel;
    lblCoords->setVisible(false);

    btnOk = new QPushButton("��������� ���������");
    btnLblMap = new QPushButton("�������� �� �����");


    QObject::connect(btnOk, SIGNAL(clicked()),
                     this,    SLOT(apply_datas())
                    );

    QObject::connect(btnOk, SIGNAL(clicked()),
                     this,    SLOT(close())
                    );

    QObject::connect(btnLblMap, SIGNAL(clicked()),
                     this,    SLOT(set_on_the_map())
                    );

    btnOk->setMaximumSize(200, 50);



    boxChoseAirTakeOff->addItems(lstAirports);
    boxChoseAirLand->addItems(lstAirports);

    boxChoseAirTakeOff->setCurrentIndex(tmpTrip.N_airTakeOff);
    boxChoseAirLand->setCurrentIndex(tmpTrip.N_airLand);


    QObject::connect(boxChoseAirTakeOff, SIGNAL(currentIndexChanged(int)), this, SLOT(set_new_airport_takeoff(int)));
    QObject::connect(boxChoseAirLand, SIGNAL(currentIndexChanged(int)), this, SLOT(set_new_airport_land(int)));


    QObject::connect(pViewMap, SIGNAL(changeCoords(float, float)), this, SLOT(setCurCoords(float, float)));
    QObject::connect(pViewMap, SIGNAL(newTripPoint(float, float)), this, SLOT(add_point(float, float)));
    QObject::connect(this, SIGNAL(draw_trip_point(float,float)), pViewMap, SLOT(accept_new_trip(float,float)));
    QObject::connect(pViewMap, SIGNAL(change_row_trip(int)), this, SLOT(set_row_trip(int)));

    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    QVBoxLayout* pvbxLayout2 = new QVBoxLayout;
    QHBoxLayout* phbxLayout = new QHBoxLayout;
    QHBoxLayout* phbxMainLayout = new QHBoxLayout;
    QGridLayout* ptopLayout = new QGridLayout;


    wgt1 = new QWidget();
    wgt2 = new QWidget();
    wgt3 = new QWidget();

    QSplitter *splitter = new QSplitter(Qt::Horizontal);
    QSplitter *splitter2 = new QSplitter(Qt::Vertical);

    ptopLayout->addWidget(lblAirportTakeOff, 0, 0);
    ptopLayout->addWidget(boxChoseAirTakeOff, 0, 1);
    ptopLayout->addWidget(lblAirportLand, 1, 0);
    ptopLayout->addWidget(boxChoseAirLand, 1, 1);


    pvbxLayout->addWidget(bar);
    pvbxLayout->addWidget(nameTrip);
    pvbxLayout->addLayout(ptopLayout);
    pvbxLayout->addWidget(&listNameTripPoints);

    phbxLayout->stretch(1);
    phbxLayout->addWidget(btnLblMap);
    phbxLayout->addWidget(btnOk);
    phbxLayout->stretch(1);

    splitter2->addWidget(pViewMap);
    splitter2->addWidget(lblCoords);


    pvbxLayout->addLayout(phbxLayout);
    wgt1->setLayout(pvbxLayout);
    splitter->addWidget(wgt1);
    splitter->addWidget(splitter2);
    phbxMainLayout->addWidget(splitter);
    //phbxMainLayout->addLayout(pvbxLayout);
    //phbxMainLayout->addLayout(pvbxLayout2);
    setLayout(phbxMainLayout);
}

void TRIP_POINTS_DLG::start() {
    this->exec();
}
