#include "AiroportDlg.h"



//---------------------------------------------------------------------

EDIT_AIRPORT::EDIT_AIRPORT(QWidget* pwgt/*= 0*/)
    : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint) {
    this->setWindowTitle(QString("������������� a��������"));
    QLabel* plblName  = new QLabel("��� ");
    QLabel* plblLa   = new QLabel("�������");
    QLabel* plblFi   =  new QLabel("������");
    QLabel* plblAz   =  new QLabel("������ ���");



    m_ptxtName =  new QLineEdit;
    m_ptxtLa =  new  GeoCoordsWidget(1);
    m_ptxtFi  =  new GeoCoordsWidget(0);
    m_ptxtAz = new QLineEdit;


    btnOk.setText("OK");
    QGridLayout* ptopLayout = new QGridLayout;




    QObject::connect(&btnOk, SIGNAL(clicked()),
                     this, SLOT(save_param())
                     );

    ptopLayout->addWidget(plblName, 0, 0);
    ptopLayout->addWidget(m_ptxtName, 0, 1);
    ptopLayout->addWidget(plblFi, 1, 0);
    ptopLayout->addWidget(m_ptxtFi , 1, 1);
    ptopLayout->addWidget(plblLa, 2, 0);
    ptopLayout->addWidget(m_ptxtLa, 2, 1);
    ptopLayout->addWidget(plblAz, 3, 0);
    ptopLayout->addWidget(m_ptxtAz , 3, 1);
    ptopLayout->addWidget(&btnOk, 4, 0);
    setLayout(ptopLayout);

}

//�������� ��������, ���� � ��� ����� ��������
void EDIT_AIRPORT::set_name_for_new() {
     this->setWindowTitle(QString("����� ��������"));
}


void EDIT_AIRPORT::start(Model_Struct::Airport *airport2) {
    airport = airport2;
    m_ptxtName->setText(QString::fromStdString(airport[0].name));
    m_ptxtLa->initWidget(airport[0].P_geo.La());
    m_ptxtFi->initWidget(airport[0].P_geo.Fi());
    m_ptxtAz->setText(QString::number(airport[0].Az));
    this->exec();
}


void EDIT_AIRPORT::save_param() {
    airport[0].name = m_ptxtName->text().toStdString();
    airport[0].P_geo.setFi(m_ptxtFi->getNumber());
    airport[0].P_geo.setLa(m_ptxtLa->getNumber());
    airport[0].Az = m_ptxtAz->text().toDouble();
    emit reinit();
    emit add_new(airport[0]);
    this->accept();
}


//--------------------------------------------------------------------------------






void EDIT_AIRPORT_DLG::reinit_datas(char *file) {
    std::ofstream output(file);
    if(!output) {
        time_t t = time(NULL);
        std::string str_file_name(file);
        std::ofstream output2("log_errors.txt", std::ios::app);
        output2 << "������ � �������� ����� " << str_file_name
        << " " <<  ctime(&t) <<  std::endl;
        output2.close();
        QMessageBox msgBox;
        msgBox.setText("������ � �������� ����� "
                       + QString::fromStdString(str_file_name));
        msgBox.exec();
    }
    for(int i = 0; i < airports.size(); i++) {
        output << airports[i];
    }
    output.close();
}







void EDIT_AIRPORT_DLG::init_airports(const char *file) {
    std::ifstream input(file);
    if(!input) {
        time_t t = time(NULL);
        std::string str_file_name(file);
        std::ofstream output2("log_errors.txt", std::ios::app);
        output2 << "������ � �������� ����� " << str_file_name
        << " " << ctime(&t) <<  std::endl;
        output2.close();
        QMessageBox msgBox;
        msgBox.setText("������ � �������� ����� "
                       + QString::fromStdString(str_file_name));
        msgBox.exec();
    }


    Model_Struct::Airport airport;

    input >> airport;
    airportMap[airport.name] = airport;


    while(!input.eof()) {
        airports.push_back(airport);
        input >> airport;
        airportMap[airport.name] = airport;

    }


    input.close();
}


void EDIT_AIRPORT_DLG::reinit_list_widget() {
    int num = listNamesAirports.currentRow();

    lstAirports.removeAt(num); //������ ������ ������ �� ������
    lstAirports << QString::fromStdString(airports[num].name); // ������� ����������
    qSort(lstAirports.begin(), lstAirports.end());          // ��������� ������ QString
    listNamesAirports.clear();                              // ������� ������ ����������
    listNamesAirports.addItems(lstAirports);                // ������� � ���� ����� ������ �����
    listNamesAirports.update();
    qSort(airports.begin(), airports.end());
    reinit_datas("FILES/AIRPORTS.txt");
}

void EDIT_AIRPORT_DLG::add_new( Model_Struct::Airport tmpAirport) {
    if(airportMap.find(tmpAirport.name) != airportMap.end()) {
        QMessageBox msgBox;
        msgBox.setText("�������� � ������ "
                       + QString::fromStdString(tmpAirport.name) + " ��� ����. ���������� ��� ���");
        msgBox.exec();
        return;
    }
    if(tmpAirport.name == "") {
        QMessageBox msgBox;
        msgBox.setText("�� �� ������ ������ �������� � ������ ������. ���������� ��� ���");
        msgBox.exec();
        return;
    }



    airports.push_back(tmpAirport);
    lstAirports << QString::fromStdString(tmpAirport.name); // ������� �������� � ������
    qSort(lstAirports.begin(), lstAirports.end());          // ��������� ������ QString
    listNamesAirports.clear();                              // ������� ������ ����������
    listNamesAirports.addItems(lstAirports);                // ������� � ���� ����� ������ �����
    listNamesAirports.update();
    qSort(airports.begin(), airports.end());
    airportMap[tmpAirport.name] = tmpAirport;
    //listNamesAirports.addItem(QString::fromStdString(tmpAirport.name));
    reinit_datas("FILES/AIRPORTS.txt");
}




void EDIT_AIRPORT_DLG::edit_params()  {
    int num = listNamesAirports.currentRow();
    EDIT_AIRPORT editAirport(this);
    QObject::connect(&editAirport, SIGNAL(reinit()), this, SLOT(reinit_list_widget()));
    editAirport.start(&airports[num]);


}


void EDIT_AIRPORT_DLG::add_new() {
    Model_Struct::Airport tmpAirport;
    EDIT_AIRPORT editAirport(this);
    editAirport.set_name_for_new();
    QObject::connect(&editAirport, SIGNAL(add_new(Model_Struct::Airport)), this, SLOT(add_new(Model_Struct::Airport)));
     editAirport.start(&tmpAirport);
}




void EDIT_AIRPORT_DLG::delete_param() {
    int sltRow = listNamesAirports.currentRow();
    std::vector <Model_Struct::Airport>::iterator it_v;


    it_v = airports.begin();
    it_v += sltRow;


    airports.erase(it_v);
    airportMap.erase(airports[sltRow].name);
    listNamesAirports.takeItem(sltRow);
    reinit_datas("FILES/AIRPORTS.txt");
}




EDIT_AIRPORT_DLG::EDIT_AIRPORT_DLG(QWidget* pwgt/*= 0*/)
    : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint) {

    //FILL DATA
    this->setWindowTitle(QString("������ ���������"));

    init_airports("FILES/AIRPORTS.txt");



    //������������� ���������� ����������
    count_params = airports.size();
    qSort(airports.begin(), airports.end());
    // Set names for buttons



    QIcon icon_add = QPixmap(":/images/SOURCE/Add.png");
    QIcon icon_delete = QPixmap(":/images/SOURCE/delete.png");
    QIcon icon_edit = QPixmap(":/images/SOURCE/edit.png");


    bar = new QToolBar("Options");



    bar->addAction(icon_add, "�������� a�������", this, SLOT(add_new()));
    bar->addSeparator();

    bar->addAction(icon_delete, "������� ��������", this, SLOT(delete_param()));
    bar->addSeparator();



    bar->addAction(icon_edit, "������������� ��������", this, SLOT(edit_params()));



    count_params = airports.size();

    for(int i = 0; i < count_params; i++) {
        lstAirports << QString::fromStdString(airports[i].name);
    }
    listNamesAirports.addItems(lstAirports);

    listNamesAirports.setCurrentRow(0);

    // Set layout
    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(bar);
    pvbxLayout->addWidget(&listNamesAirports);
    setLayout(pvbxLayout);
}

void EDIT_AIRPORT_DLG::start() {
    this->exec();
}




