
#include "PlaneParametersDialog.h"
#include "planes_params_edit.h"


void PARAMS_EDITOR::setNumRow(QListWidgetItem * item) {
    sltRow = listNameParams.row(item);
}



void PARAMS_EDITOR::emit_signal_for_next_dialog() {
    PlaneDialog plnDlg(this);
}


//��������� ����� ��� ����
void PARAMS_EDITOR::add_new(Model_Struct::PlaneTTD plnData) {
    plnsData.push_back(plnData);
    count_params++;
    sltRow = count_params - 1;
    listNameParams.addItem(QString::fromStdString(plnData.TargetType));
    listNameParams.item(sltRow)->setText(QString::fromStdString(plnsData[sltRow].TargetType)); //E
    change_params_file("FILES/PAR_PLNS.txt");

}


void PARAMS_EDITOR::add_new() {
    Model_Struct::PlaneTTD plnData;
    PlaneDialog plnDlg(this);
    QObject::connect(&plnDlg, SIGNAL(add_new(Model_Struct::PlaneTTD)), this, SLOT(add_new(Model_Struct::PlaneTTD)));
    plnDlg.show_edit_table(&plnData);
}


void  PARAMS_EDITOR::delete_param() {
    if(count_params <= 1) {
        QMessageBox msgBox;
        msgBox.setText("�� ���� ������� ��������� ��������� �������");
        msgBox.exec();
        return;
    }
    count_params--;
    std::vector<Model_Struct::PlaneTTD>::iterator it_v;
    it_v = plnsData.begin();
    it_v += sltRow;
    plnsData.erase(it_v);
    change_params_file("FILES/PAR_PLNS.txt");

    // Delete selected item from ListWidget
    listNameParams.takeItem(sltRow);
    if(sltRow != 0)
       sltRow--;
}


void  PARAMS_EDITOR::edit_params() {
    PlaneDialog plnDlg(this);
    qDebug() << "EDIT TABLE";
    plnDlg.show_edit_table(&plnsData[sltRow]);
    listNameParams.item(sltRow)->setText(QString::fromStdString(plnsData[sltRow].TargetType));
    change_params_file("FILES/PAR_PLNS.txt");
}

void PARAMS_EDITOR::change_params_file(QString file_name) {
    std::string file = file_name.toStdString();
    std::ofstream output(file.c_str(), std::ios::trunc);

    if(!output) {
        time_t t = time(NULL);
        std::ofstream err("log_errors.txt",  std::ios::app);
        err << "������ �������� ����� " << file << " "
        << ctime(&t) << std::endl;
        err.close();
        QMessageBox msgBox;
        msgBox.setText("������ �������� ����� "
                        +  file_name);
        msgBox.exec();
        return;
    }

    output << count_params << std::endl;

    for(int i = 0; i < count_params; i++) {
        output << plnsData[i];
    }

    output.close();

}

void PARAMS_EDITOR::init_plns_data_from_file(QString file_name) {
    std::string file = file_name.toStdString();
    std::ifstream input(file.c_str());
    if(!input) {
        time_t t = time(NULL);
        std::ofstream err("log_errors.txt",  std::ios::app);
        err << "������ �������� ����� " << file << " "
        << ctime(&t) << std::endl;
        err.close();
        QMessageBox msgBox;
        msgBox.setText("������ �������� ����� "
                        +  file_name);
        msgBox.exec();
        return;
    }

    Model_Struct::PlaneTTD tmpPlnData;
    input >> count_params; // C�������� ���������� ����������
    if(count_params == 0) { // ���� � ��� ��� ���������� ���������
        return;
    }

    for(int i = 0; i < count_params; i++) {
        input >> tmpPlnData;
        plnsData.push_back(tmpPlnData);

    }
}



PARAMS_EDITOR::PARAMS_EDITOR(QWidget* pwgt/*= 0*/)
    : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
 {
    // Init names of planes and know how much they are

    this->setWindowTitle(QString("������ ����"));


    init_plns_data_from_file("FILES/PAR_PLNS.txt");


    for(int i = 0; i < count_params; i++) {
        listNameParams.addItem(QString::fromStdString(plnsData[i].TargetType));
    }


    sltRow = 0;





    QIcon icon_add = QPixmap(":/images/SOURCE/Add.png");
    QIcon icon_delete = QPixmap(":/images/SOURCE/delete.png");
    QIcon icon_edit = QPixmap(":/images/SOURCE/edit.png");


    bar = new QToolBar("Options");



    bar->addAction(icon_add, "�������� ����� ����", this, SLOT(add_new()));
    bar->addSeparator();

    bar->addAction(icon_delete, "������� ����", this, SLOT(delete_param()));
    bar->addSeparator();

    bar->addAction(icon_edit, "������������� ����", this, SLOT(edit_params()));


    listNameParams.setCurrentRow(0);
    //Connect signals to set num row for click
    //And set another signal to send data for clicked item

    QObject::connect(&listNameParams, SIGNAL(itemClicked(QListWidgetItem *)),
                     this, SLOT(setNumRow(QListWidgetItem *))
                     );




    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(bar);
    pvbxLayout->addWidget(&listNameParams, 0, 0);
    setLayout(pvbxLayout);

}



void PARAMS_EDITOR::start(){
    this->exec();
}
