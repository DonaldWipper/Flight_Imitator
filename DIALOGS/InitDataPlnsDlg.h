#ifndef INITDATAPLNSDLG_H
#define INITDATAPLNSDLG_H

#endif // INITDATAPLNSDLG_H


#include <QtGui>
#include <QDebug>
#include <time.h>

#include "ShiftTripDlg.h"
#include "../FLIGHT_CALCULATION/flight_plan.hpp"


/*********************************************************************
 * ����� ��� ������� ��������, ��� ���������� ��� �������� �� �����  *
 ********************************************************************/























class INIT_PLNS_DLG : public QDialog {
    Q_OBJECT
private:



    QString *names_planes;
    QString *names_tasks;
    QStringList names_scens;


    //ComboBox for each item
    //With option to chose plane's parameters

    /*( + New)*/




    // This is QMaps, set relationships QComboBox with number




    QMap <QComboBox*, int> mapBoxTblItem;
    QMap <QLineEdit*, int> mapTextHmax;
    QMap <QLineEdit*, int> mapTextTbeg;


    QMap <QPushButton*, int> mapBtnGroupEdit;

    QMap <QPushButton*, int> mapBtnTripEdit;
    QMap <QPushButton*,  int> mapDeleteItem;
    QMap <QCheckBox*, int> mapCBoxMode;




    QTableWidget tblShowAllPrmtrs;



    int slt_num_plane;
    int slt_num_par;
    int slt_num_task;


    //******Structure of table row*********//

    QComboBox* boxsChoseParameter;
    QPushButton* btnsTrip;
    QPushButton* btnsGroup;

    QString *namesTrips;
    QLineEdit *lnsTextTbeg;
    QLineEdit *lnsTextHmax;

    //*************************************//

    QPushButton *btnsDelete;
    QCheckBox *checkModeScenary;

    QCheckBox *bxsModeScenary;

    //*******************************************//

    QString scenaryName;





    std::vector<Model_Struct::PlaneTTD> *plnsData;
    std::vector <Model_Struct::Airport> *airports;
    std::map<std::string, Model_Struct::Airport> *airportMap;


   void init_trip_from_file(QString file, Model_Struct::Trip &trip);

   void init_set_trips();



    void init_flight_data(QString file);
    void reinit_flight_task(QString file);
    void init_saved_scenaries(QString file);

public:
    std::vector<Model_Struct::FlightTask> tasks;
    std::vector<Model_Struct::FlightTask> tmpTasks;

    std::vector <Model_Struct::Trip> setTrip; // init set of trips from file





    int rows, cols;
    int count_planes;
    int addit_count; // ���������� ��������� ��������
    int count_sets_trip;

    INIT_PLNS_DLG();
    INIT_PLNS_DLG(QWidget* pwgt = 0,
                  std::vector<Model_Struct::PlaneTTD> *plnsData = 0,
                  std::vector <Model_Struct::Airport> *airports2 = 0,
                  std::map<std::string, Model_Struct::Airport> *airportMap2 = 0);




signals:
    void refresh_task();
    void stop_motion();
    void signal_change_mode(int);

public slots:
    void start();
    void new_init_flight_data();

    //void set_count_planes();

    void load_trip_dialog();


    //-----------load & save scenaries--------------//
    void load_scenary();
    void save_scenary();



    //----------------------------------------------//

    void refresh_scenary();

    //Slot is called when we change comboBox with params in table
    void add_new_plane();
    void set_new_plane_param(int num_par);
    //void set_new_airport_takeoff(int num_air);
    //void set_new_airport_land(int num_air);
    void delete_plane();
    void change_Tbeg();
    void change_maxH();

    void change_table_param(int row, int col);
    void dublicate_last_plane();
    void start_shift_dlg();
    //void shift_last_plane();    //����� ������� �� ������� � ������������
    void change_all_mode(int state);
    void change_mode_params(int state);

};
