#ifndef PLANEPARAMETERSDIALOG_H
#define PLANEPARAMETERSDIALOG_H


#include <QDialog>
#include <QtGui>
#include <iostream>
#include <fstream>
#include <string>

#include "../FLIGHT_CALCULATION/flight_plan.hpp"

class QLineEdit;
// ======================================================================
class PlaneDialog : public QDialog {
    Q_OBJECT



private:
    QLineEdit* m_ptxtName;
    QLineEdit* m_ptxtL_razb;
    QLineEdit* m_ptxtV_otr;
    QLineEdit* m_ptxtV_evol;
    QLineEdit* m_ptxtV_cruise;
    QLineEdit* m_ptxtV_climb;
    QLineEdit* m_ptxtV_descend;
    QLineEdit* m_ptxtV_max;

    QLineEdit* m_ptxtA_dog;
    QLineEdit* m_ptxtA_krug;
    QLineEdit* m_ptxtA_pos;
    QLineEdit* m_ptxtHmax;



    Model_Struct::PlaneTTD *PLN;

public slots:
    void send_info();
    void start_dialog();
    void show_edit_table(Model_Struct::PlaneTTD *pln);
signals:
    void reinit();
    void add_new(Model_Struct::PlaneTTD pln);

public:
    PlaneDialog(QWidget* pwgt = 0);
    void init_from_file (const char *file);
    QString Name() const;
    QString L_razb () const;
    QString V_otr () const;
    QString V_evol () const;
    QString V_cruise () const;
    QString V_climb () const;
    QString V_descend () const;
    QString V_max () const;

    QString acc_dog() const;
    QString acc_krug() const;
    QString acc_pos() const;
    QString H_max()  const;

};



#endif  //_PlaneDialog_h_
