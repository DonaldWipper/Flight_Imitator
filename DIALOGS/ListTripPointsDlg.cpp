#include "ListTripPointsDlg.h"




//===================SMALL DIALOG==============================================//


EDIT_TRIP_POINT_DLG::EDIT_TRIP_POINT_DLG(QWidget* pwgt/*= 0*/)
    : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint) {
    this->setWindowTitle(QString("������������� ����������� �����"));
    QLabel* plblName  = new QLabel("��� ����������� �����");
    QLabel* plblLa   = new QLabel("�������");
    QLabel* plblFi   =  new QLabel("������");
    QLabel* plblNrot   =  new QLabel("����� ������");
    btnMoveLinear = new QRadioButton("�������� ��������");
    btnMoveRound = new QRadioButton("����� �� ����");
    m_ptxtNamePoint =  new QLineEdit;
    m_ptxtLa =  new GeoCoordsWidget(1);
    m_ptxtFi  =  new GeoCoordsWidget(0);
    m_ptxtNrot =  new QLineEdit;

    m_ptxtNrot->setValidator(new QIntValidator(0, 50, m_ptxtNrot));


    lblAcceleration = new QLabel("������ ���������");
    boxAcceleration = new QCheckBox();

    lblSpeedEnd = new QLabel("�������� ��������");
    m_ptxtlblSpeedEnd = new QLineEdit;
    m_ptxtlblSpeedEnd->setValidator(new QDoubleValidator());

    btnOk.setText("OK");
    QGridLayout* ptopLayout = new QGridLayout;




    QObject::connect(&btnOk, SIGNAL(clicked()),
                     this, SLOT(save_param())
                     );
    QObject::connect(btnMoveLinear, SIGNAL(clicked()),
                     this, SLOT(setLinearMotion())
                     );


    QObject::connect(btnMoveRound, SIGNAL(clicked()),
                     this, SLOT(setRoundMotion())
                     );
    QObject::connect(boxAcceleration, SIGNAL(stateChanged(int)),
                     this, SLOT(setEnableSpeedSet(int))
                     );

    ptopLayout->addWidget(plblName, 0, 0);
    ptopLayout->addWidget(m_ptxtNamePoint, 0, 1);
    ptopLayout->addWidget(plblFi, 1, 0);
    ptopLayout->addWidget(m_ptxtFi , 1, 1);
    ptopLayout->addWidget(plblLa, 2, 0);
    ptopLayout->addWidget(m_ptxtLa, 2, 1);

    ptopLayout->addWidget(plblNrot , 3, 0);
    ptopLayout->addWidget(m_ptxtNrot , 3, 1);
    ptopLayout->addWidget(btnMoveLinear, 4, 0);
    ptopLayout->addWidget(btnMoveRound, 5, 0);
    ptopLayout->addWidget(lblSpeedEnd , 5, 1);
    ptopLayout->addWidget(m_ptxtlblSpeedEnd, 5, 2);


    ptopLayout->addWidget(lblAcceleration, 4, 1);
    ptopLayout->addWidget(boxAcceleration, 4, 2 );
    ptopLayout->addWidget(&btnOk, 6, 0);
    setLayout(ptopLayout);
}

void EDIT_TRIP_POINT_DLG::setLinearMotion() {
    if(m_ptxtNrot->isEnabled() == true) {
       m_ptxtNrot->setEnabled(false);
    }
    ptrPoint[0].PointType = 4;
}

void EDIT_TRIP_POINT_DLG::setRoundMotion() {
    if(m_ptxtNrot->isEnabled() == false) {
       m_ptxtNrot->setEnabled(true);
    }
    ptrPoint[0].PointType = 6;
}

void EDIT_TRIP_POINT_DLG::setEnableSpeedSet(int state) {
    if(boxAcceleration->isChecked() == true) {
        m_ptxtlblSpeedEnd->setEnabled(true);
    } else {
       m_ptxtlblSpeedEnd->setEnabled(false);
    }
}



void EDIT_TRIP_POINT_DLG::save_param() {
    ptrPoint[0].name = m_ptxtNamePoint->text().toStdString();
    double la = m_ptxtFi->getNumber();
    double fi = m_ptxtLa->getNumber();
    if((la == -200) || (fi == -200)) {
        return;
    }



    ptrPoint[0].P_geo.setFi(m_ptxtFi->getNumber());
    ptrPoint[0].P_geo.setLa(m_ptxtLa->getNumber());
    ptrPoint[0].N_rot =  m_ptxtNrot->text().toInt();
    ptrPoint[0].isNeedAcc = boxAcceleration->isChecked();

    if(ptrPoint[0].isNeedAcc) { // ���� �� ������ ���������
        if(m_ptxtlblSpeedEnd->text().toDouble() / 3.6 <= V_max)  { //���������� �������� ��������
            ptrPoint[0].V_end = m_ptxtlblSpeedEnd->text().toDouble() / 3.6;
            emit reinit();
            emit add_new(ptrPoint[0]);
            this->accept();
        } else { //������ ������������
            QMessageBox msgBox;
            msgBox.setText("�������� �������� �� ����� ���� ������ ������������ "
                            + QString::number(V_max * 3.6) + " �� / � ");
            msgBox.exec();
        }
    } else {
        emit reinit();
        emit add_new(ptrPoint[0]);
        this->accept();
    }
}


void EDIT_TRIP_POINT_DLG::start(Model_Struct::BillPoint *ptrPoint2, double V_max2) {
    V_max = V_max2;
    ptrPoint = ptrPoint2;
    if(ptrPoint[0].PointType == 4) {
        m_ptxtNrot->setEnabled(false);
        btnMoveLinear->setChecked(true);
    } else {
        btnMoveRound->setChecked(true);
    }
    m_ptxtNamePoint->setText(QString::fromStdString(ptrPoint[0].name));
    m_ptxtLa->initWidget(ptrPoint[0].P_geo.La());
    m_ptxtFi->initWidget(ptrPoint[0].P_geo.Fi());
    m_ptxtNrot ->setText(QString::number(ptrPoint[0].N_rot));
    m_ptxtlblSpeedEnd->setText(QString::number(ptrPoint[0].V_end * 3.6));
    boxAcceleration->setChecked(ptrPoint[0].isNeedAcc);
    if(ptrPoint[0].isNeedAcc == false) {
        m_ptxtlblSpeedEnd->setEnabled(false);
    } else {
        if(ptrPoint[0].V_end > V_max) {
            QMessageBox msgBox;
            QString text = "� ����������� �������� �������� �������� ������ ������������" + QString::number(V_max * 3.6) + " �� / � \n";
            text.append("�������� �������� ����� ���������� ������������");
            msgBox.setText(text);
            msgBox.exec();
            ptrPoint[0].V_end = V_max;
        }
    }

    this->exec();
}

//=============================================================================//








