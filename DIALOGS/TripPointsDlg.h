
#ifndef TRIPPOINTSDLG_H
#define TRIPPOINTSDLG_H



#include <QtGui>
#include <QDebug>
#include <fstream>


#include "../FLIGHT_CALCULATION/flight_plan.hpp"
#include "ListTripPointsDlg.h"
#include "../OGLPyramid.h"

class TRIP_POINTS_DLG : public  QDialog {
    Q_OBJECT
private:

    QLabel *nameTrip;

    QToolBar *bar;
    QComboBox* boxChoseAirTakeOff;
    QComboBox* boxChoseAirLand;
    QListWidget listNameTripPoints;
    QPushButton *btnLblMap;
    QPushButton *btnOk;

    //Current coords
    QLabel *lblCoords;


    QDialog *wgt;

    QWidget *wgt1;
    QWidget *wgt2;
    QWidget *wgt3;
    OGLPyramid*  pViewMap;

    Model_Struct::Trip *trip;
    std::vector <Model_Struct::Airport> *airports;
    std::map<std::string, Model_Struct::Airport> *airportsMap;


    Model_Struct::Trip tmpTrip;
    std::vector <Model_Struct::Airport> tmpAirports;
    std::string tmpName;


    std::string *name;
    void init_from_trip(Model_Struct::Trip trip);

    //Recognize short trip's name from path
    std::string get_trip_name_from_string(std::string full_name);

    int getIndexByName(Model_Struct::Airport air);

    ASh::GeoGraph::GeoCoord centre1;  //������ �����������, � ������� ������ ������ ��������� ���������� �����
    ASh::GeoGraph::GeoCoord centre2;
    double R; //������� ���� �����������

    double V_cruise; // ����������� ��������.����� ��� ����, ����� ������� ���� ������ ������� ���������� �����
    double a_krug;

    bool getForbAreas(ASh::GeoGraph::GeoCoord point) ;

public:
    int sltRow;
    TRIP_POINTS_DLG(QWidget* pwgt,
                    std::vector <Model_Struct::Airport> *airport,
                    std::map<std::string, Model_Struct::Airport> *airportMap,
                    std::string *name,
                    Model_Struct::Trip *_trip,
                    double V_cruise,
                    double a_krug);

public slots:
    void start();
    void edit_point();
    void add_point();
    void add_point(float La, float Fi);
    void delete_point();


    void save_trip();
    void load_trip();
    void apply_datas(); // Apply datas changes in current file

    void set_row_trip(int num_trip);
    void set_new_airport_takeoff(int num_air);
    void set_new_airport_land(int num_air);
    void set_on_the_map();
    void setCurCoords(float La, float Fi);

    void reinit_list_widget();
    void add_new( Model_Struct::BillPoint tmpPnt);

signals:
   void draw_trip_point(float, float);
   void reinit_datas();
   void stop_motion();


};


































#endif // TRIPPOINTSDLG_H
