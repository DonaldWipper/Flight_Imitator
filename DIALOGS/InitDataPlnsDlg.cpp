#include "InitDataPlnsDlg.h"
#include "TripPointsDlg.h"




void INIT_PLNS_DLG::load_trip_dialog() {
   QPushButton* btn = (qobject_cast<QPushButton*> (sender()));
   int num = mapBtnTripEdit.value(btn);
   TRIP_POINTS_DLG trpDialog(this,
                             airports,
                             airportMap,
                             &tasks[num].tripName,
                             &setTrip[num],
                             plnsData[0][tasks[num].N_PlaneParam].V_cruise,
                             plnsData[0][tasks[num].N_PlaneParam].acc_krug);

   QObject::connect(&trpDialog, SIGNAL(reinit_datas()), this, SLOT(start()));
   trpDialog.start();

}





void INIT_PLNS_DLG::refresh_scenary() {
    int ret;
    for(int i = 0; i < setTrip.size(); i++) {
        if(namesTrips[i] == "noname") {
            ret = QMessageBox::warning(this, tr("Warning"),
                                            tr("�������� ��������� �������� ������� <noname> \n"
                                               "X����� �������� ��������, ������� ����� ���������� ������ �� ���������?"),
                                                QMessageBox::Ok | QMessageBox::Cancel);
            break;


        }
    }
    if(ret == QMessageBox::Cancel)
        return;


    reinit_flight_task(scenaryName);
    refresh_task();

    this->accept();

}
//******************************************************************************************

void INIT_PLNS_DLG::load_scenary() {
    QFileDialog fileDlg;
    QString file_name;
    file_name = fileDlg.getOpenFileName(0, "�������� �������� �����, ����������� �������� ", "FILES/scenaries", "*.txt");


    if(file_name != "") {
        tasks.clear();
        init_flight_data(file_name);
        scenaryName = file_name;
        start();
    }
}





void INIT_PLNS_DLG::save_scenary() {
    QFileDialog fileDlg;
    QString file_name;
    file_name = fileDlg.getSaveFileName(0, "������� ��� ����� �������� ", "FILES/scenaries", "*.txt");






    reinit_flight_task(file_name);
    scenaryName = file_name;
    start();


}

//*********************************************************************************************









void INIT_PLNS_DLG::init_flight_data(QString _file) {
    std::string file = _file.toStdString();
    std::ifstream input(file.c_str());




    if(!input) {
        time_t t = time(NULL);
        std::ofstream output("log_errors.txt",  std::ios::app);
        output << "������ �������� ����� " << file << " "
        << ctime(&t) << std::endl;
        output.close();
    }

    // (+new<10.07.13>)    input count_planes

    input >> count_planes;



    if(count_planes <= 0) {
        return;
    }





    Model_Struct::FlightTask tmpFltData;

    for(int i = 0; i < count_planes; i++) {
        input >> tmpFltData;
        tasks.push_back(tmpFltData);
    }




    init_set_trips();
    input.close();
}


void INIT_PLNS_DLG::new_init_flight_data() {
    init_flight_data(scenaryName);
}


//�������� �������� �� �����
void INIT_PLNS_DLG::init_trip_from_file(QString _file, Model_Struct::Trip &trip) {
    std::string file = _file.toStdString();
    std::ifstream input(file.c_str());
    if(!input) {
        time_t t = time(NULL);
        std::ofstream output("log_errors.txt",  std::ios::app);
        output << "������ �������� ����� " << file << " "
        <<  ctime(&t) << std::endl;
        output.close();
        //������ ���������� ������ � ������� �� ���������
        trip.N_airLand = 0;
        trip.N_airTakeOff = 0;
        return;
    } else {
        input >> trip;
    }
}



void INIT_PLNS_DLG::init_set_trips() {

    setTrip.clear();
    for(int i = 0; i < tasks.size(); i++) {
        QString name =  QString::fromStdString(tasks[i].tripName);
        name = "FILES/trips/" + name + ".txt";
        Model_Struct::Trip tmpTrip;
        init_trip_from_file(name, tmpTrip);
        qDebug() << "Luter2.5";
        tmpTrip.V_max = plnsData[0][tasks[i].N_PlaneParam].V_max;
        setTrip.push_back(tmpTrip);
    }




}

//************************************************************************************


void INIT_PLNS_DLG::reinit_flight_task(QString _file) {
    std::string file = _file.toStdString();
    std::ofstream output(file.c_str());

    if(!output) {
        time_t t = time(NULL);
        std::ofstream output("log_errors.txt",  std::ios::app);
        output << "������ �������� ����� " << file << " "
        << ctime(&t) << std::endl;
        output.close();
        return;
    }

    // (+new<10.07.13>)    input count_planes

    output << count_planes << std::endl;


    for(int i = 0; i < tasks.size(); i++) {
        output << tasks[i];
    }

    output.close();
}











//------------------��������� ��� ����---------------------------------
void INIT_PLNS_DLG::set_new_plane_param(int num_par) {
    int num_plane;
    //Define what object sended signal

    QComboBox* box = (qobject_cast<QComboBox*> (sender()));

    //Define from what row signal was sended
    num_plane = mapBoxTblItem.value(box);
    //redefine number of parameter to plane
    tasks[num_plane].N_PlaneParam = num_par;

    setTrip[num_plane].V_max = plnsData[0][num_par].V_max; //��������� ���������� ������ �� ��������� ������������ ��������
}





//�������� ������ ���������
void INIT_PLNS_DLG::change_all_mode(int state) {
   for(int i = 0; i < tasks.size(); i++) {
       bxsModeScenary[i].setChecked(state);
       tasks[i].mode = state;
   }
   tblShowAllPrmtrs.update();

}


void INIT_PLNS_DLG::change_mode_params(int state) {
    int num_plane;
    QCheckBox* box = (qobject_cast<QCheckBox*> (sender()));
    num_plane = mapCBoxMode.value(box);
    tasks[num_plane].mode = state;
}


/*void INIT_PLNS_DLG::set_new_airport_takeoff(int num_air) {
    int num_plane;
    QComboBox* box = (qobject_cast<QComboBox*> (sender()));
    num_plane = mapBoxAirTakeoffItem.value(box);
    tasks[num_plane].N_AirTakeoff = num_air;


}


void INIT_PLNS_DLG::set_new_airport_land(int num_air) {
    int num_plane;
    QComboBox* box = (qobject_cast<QComboBox*> (sender()));
    num_plane = mapBoxAirLandItem.value(box);
    tasks[num_plane].N_AirLand = num_air;

}*/


//****************************************************************************

void INIT_PLNS_DLG::add_new_plane() {
    rows += 1;
    count_planes++;

    Model_Struct::FlightTask task;
    tasks.push_back(task);
    Model_Struct::Trip tmpTrip;
    setTrip.push_back(tmpTrip);
    start();

}


//��������� ��������� �������
void INIT_PLNS_DLG::dublicate_last_plane() {
    if(count_planes <= 0) {
        return;
    }
    rows += 1;
    count_planes++;
    Model_Struct::FlightTask task = tasks[count_planes - 2];
    tasks.push_back(task);
    Model_Struct::Trip tmpTrip = setTrip[count_planes - 2];
    setTrip.push_back(tmpTrip);
    start();
}



void INIT_PLNS_DLG::start_shift_dlg() {
    int num_plane;
    QPushButton* line = (qobject_cast< QPushButton*> (sender()));
    num_plane = mapBtnGroupEdit.value(line);

    SHIFT_TRIP_DLG shiftDlg(this, tasks[num_plane]);
    shiftDlg.start_dialog();
    start();
}



/*//��������� ����� �� ������� ������������ ����������
void INIT_PLNS_DLG::shift_last_plane() {
    if(count_planes <= 0) {
        return;
    }

    SHIFT_TRIP_DLG shiftDlg(this);
    shiftDlg.start_dialog();
    double az = shiftDlg.az,  dist = shiftDlg.dist;
    if((az == 0) && (dist == 0)) {
        return;
    }
    rows += 1;
    count_planes++;

    Model_Struct::FlightTask task = tasks[count_planes - 2];
    tasks.push_back(task);
    Model_Struct::Trip tmpTrip = setTrip[count_planes - 2];

    for(int i = 0; i < tmpTrip.tripPoints.size(); i++) {
        ASh::GeoGraph::GeoCoord pntPrev;
        if(i == 0) {
            pntPrev = airports[0][tmpTrip.N_airLand].P_geo;
        } else {
            pntPrev = tmpTrip.tripPoints[i - 1].P_geo;
        }

        tmpTrip.tripPoints[i].shift(dist, az, pntPrev);
    }
    setTrip.push_back(tmpTrip);


    int num =  count_planes - 1;
    tasks[num].tripName += "_";
    TRIP_POINTS_DLG trpDialog(this,
                              airports,
                              airportMap,
                              &tasks[num].tripName,
                              &setTrip[num],
                              plnsData[0][tasks[num].N_PlaneParam].V_cruise,
                              plnsData[0][tasks[num].N_PlaneParam].acc_krug);
    trpDialog.apply_datas();


    start();
}*/


void INIT_PLNS_DLG::change_Tbeg() {
    int num_plane;
    QLineEdit* line = (qobject_cast< QLineEdit*> (sender()));
    num_plane = mapTextTbeg.value(line);
    double Tbeg = line->text().toDouble();
    tasks[num_plane].T_beg = Tbeg;
}


void INIT_PLNS_DLG::change_maxH() {
    int num_plane;
    QLineEdit* line = (qobject_cast< QLineEdit*> (sender()));
    num_plane = mapTextHmax.value(line);
    int num_par = boxsChoseParameter[num_plane].currentIndex(); //������� ����� ��������� �����������
    double Hmax = line->text().toDouble();
    if(Hmax > plnsData[0][num_par].Hmax) { // ���� ��������� ������������ ������
        QMessageBox msgBox;
        msgBox.setText("������ �� ����� ���� ������ ������������ ������ ��� �������� " + QString::fromStdString(plnsData[0][num_par].TargetType)
                       + " "  + QString::number(plnsData[0][num_par].Hmax) + " � ");
        msgBox.exec();
        line->setText(QString::number(tasks[num_plane].H_max));
        return;
    }
    tasks[num_plane].H_max = Hmax;
}

void INIT_PLNS_DLG::change_table_param(int row, int col) {
    QTableWidgetItem *item = tblShowAllPrmtrs.item(row, col);

    double Hmax;
    int num  = tasks[row].N_PlaneParam;
    switch(col) {
    /*case(1):
        Hmax = item->text().toDouble();
        if(Hmax > plnsData[0][num].Hmax) {
            QMessageBox msgBox;
            msgBox.setText("������ �� ����� ���� ������ ������������ ������ ��� �������� " + QString::fromStdString(plnsData[0][num].TargetType)
                           + " "  + QString::number(plnsData[0][num].Hmax) + " � / � ");
            msgBox.exec();
            item->setText(QString::number(tasks[row].H_max));
            return;
        } else {
            tasks[row].H_max = Hmax;
        }
        break;*/
    case(2):
        tasks[row].T_beg = item->text().toDouble();
        break;
    }


}




void INIT_PLNS_DLG::delete_plane() {
    int num_plane;
    QPushButton* btn = (qobject_cast<QPushButton*> (sender()));
    num_plane = mapDeleteItem.value(btn);
    count_planes--;


    std::vector<Model_Struct::FlightTask>::iterator it_tsk;

    it_tsk = tasks.begin();
    it_tsk += num_plane;

    tasks.erase(it_tsk);
    rows--;

    start();
}




//------------------------------------------------------------------------------------------------

INIT_PLNS_DLG::INIT_PLNS_DLG () {



}
INIT_PLNS_DLG::INIT_PLNS_DLG(QWidget* pwgt,
                             std::vector<Model_Struct::PlaneTTD> *plnsData2,
                             std::vector <Model_Struct::Airport> *airports2,
                             std::map<std::string, Model_Struct::Airport> *airportMap2): QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint) {



    this->setWindowTitle(QString("������ ��������"));

    // Start input count planes
    plnsData = plnsData2;
    airports = airports2;
    airportMap = airportMap2;


    //set count cols in table



    QString str = "FILES/FLIGHT_TASK_DATAS.txt";
    scenaryName  = str;

    init_flight_data(scenaryName);


    cols = 7;
    rows = count_planes;





    boxsChoseParameter = NULL;
    btnsTrip = NULL;
    btnsDelete = NULL;
    bxsModeScenary = NULL;
    btnsGroup = NULL;
    lnsTextTbeg = NULL;
    lnsTextHmax = NULL;





    QIcon icon_add = QPixmap(":/images/SOURCE/Add.png");
    QIcon icon_save = QPixmap(":/images/SOURCE/save.png");
    QIcon icon_load = QPixmap(":/images/SOURCE/open.png");
    QIcon icon_update = QPixmap(":/images/SOURCE/update.png");
    QIcon icon_dublicate = QPixmap(":/images/SOURCE/dublicate.png");
    QIcon icon_margin = QPixmap(":/images/SOURCE/margin.png");







    QToolBar* bar = new QToolBar("Options");
    QCheckBox *checkModeScenary = new QCheckBox("������ ��� ������ � ������� ��� ���� ���������");


    bar->addAction(icon_add, "�������� ����� �������", this, SLOT(add_new_plane()));
    bar->addSeparator();


    bar->addAction(icon_dublicate, "����������� ��������� �������", this, SLOT(dublicate_last_plane()));
    bar->addSeparator();




    bar->addAction( icon_save, "��������� ��������", this, SLOT(save_scenary()));
    bar->addSeparator();

    bar->addAction( icon_load, "��������� ��������", this, SLOT(load_scenary()));
    bar->addSeparator();

    bar->addAction(icon_update, "��������� ������� ��������", this, SLOT(refresh_scenary()));
    bar->addSeparator();




    QObject::connect(this, SIGNAL(rejected()), this, SLOT(new_init_flight_data()));

    QObject::connect(checkModeScenary, SIGNAL(stateChanged(int)), this, SLOT(change_all_mode(int)));

    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(bar);
    pvbxLayout->addWidget(&tblShowAllPrmtrs);
    pvbxLayout->addWidget(checkModeScenary);

    //----------------table settings-------------------
    tblShowAllPrmtrs.setMinimumSize(790, 200);

    //-------------------------------------------------

    setLayout(pvbxLayout);




}

void INIT_PLNS_DLG::start() {


    //count planes's parameters
    emit stop_motion(); //������������� ������ ��������
                        //� ��� ������-�� ���������� ��������� ��� ��������

    rows = count_planes;
    int count_params = plnsData[0].size();


    names_planes = new QString[count_params];
    tblShowAllPrmtrs.clear();


    tblShowAllPrmtrs.setColumnCount(cols);
    tblShowAllPrmtrs.setRowCount(rows);



    if(boxsChoseParameter != NULL) {
        delete[] boxsChoseParameter;
    }


    if(btnsGroup != NULL) {
        delete[] btnsGroup;
    }



    if(btnsTrip != NULL) {
        delete[] btnsTrip;
    }


    if(btnsDelete != NULL) {
        delete[] btnsDelete;
    }

    if(bxsModeScenary != NULL) {
        delete[] bxsModeScenary;
    }

    if(lnsTextHmax != NULL) {
        delete[] lnsTextHmax;
    }

    if(lnsTextTbeg != NULL) {
        delete[] lnsTextTbeg;
    }



    init_set_trips();
    boxsChoseParameter = new QComboBox[rows];
    btnsTrip = new QPushButton[rows];
    btnsDelete = new QPushButton[rows];
    btnsGroup = new QPushButton[rows];
    namesTrips = new QString[rows];
    bxsModeScenary = new QCheckBox[rows];
    lnsTextHmax = new QLineEdit[rows];
    lnsTextTbeg = new QLineEdit[rows];

    QStringList llst2;
    QStringList lst;
    QStringList lst_names;
    QStringList lst_numbers;

     qDebug() << "Luter6";


    QStringList lst_airports;
    for(int i = 0; i < airports[0].size(); i++) {
        lst_airports << QString::fromStdString(airports[0][i].name);
    }

    for(int i = 0; i < count_params; i++) {
        names_planes[i] = QString::fromStdString(plnsData[0][i].TargetType);

    }






    lst << "���  ����"  << "������������ ������(�)"  << "����� ��������(�)" << "�������� � ������" << "�������" << "��� ������ \n � �������" << "";
    tblShowAllPrmtrs.setHorizontalHeaderLabels(lst);
    QTableWidgetItem* item2 = tblShowAllPrmtrs.horizontalHeaderItem(5);
    QFont font;
    font.setPixelSize(10);
    item2->setFont(font);
    int num = 1;
    for(int i = 0; i < rows; i++) {
         llst2 << "";
       if(tasks[i].cnt_planes_group != 0) {

           QString str;
           str = "������(";
           str += QString::number(tasks[i].cnt_planes_group + 1);
           str += ")";
           btnsGroup[i].setText(str);
           btnsGroup[i].setIcon(QPixmap(":/images/SOURCE/margin.png"));
       } else {
           btnsGroup[i].setText("���������");
           btnsGroup[i].setIcon(QPixmap(":/images/SOURCE/plane.png"));
       }

    }





    tblShowAllPrmtrs.setVerticalHeaderLabels(llst2);

    for(int i = 0; i < count_params; i++) {
        lst_names << names_planes[i];
    }




    mapBoxTblItem.clear();


    QIcon icon_view =  QPixmap(":/images/SOURCE/view.png");

    for(int i = 0; i < rows; i++) {

        namesTrips[i] = QString::fromStdString(tasks[i].tripName);
        btnsTrip[i].setText(namesTrips[i]);
        btnsTrip[i].setIcon(icon_view);


        boxsChoseParameter[i].addItems(lst_names);
        boxsChoseParameter[i].setCurrentIndex(tasks[i].N_PlaneParam);

        bxsModeScenary[i].setChecked(tasks[i].mode);


        mapBoxTblItem.insert( &boxsChoseParameter[i], i);
        mapDeleteItem.insert(&btnsDelete[i], i);
        mapTextTbeg.insert(&lnsTextTbeg[i], i);
        mapTextHmax.insert(&lnsTextHmax[i], i);
        mapBtnTripEdit.insert(&btnsTrip[i], i);
        mapCBoxMode.insert(&bxsModeScenary[i], i);


        QObject::connect(&boxsChoseParameter[i], SIGNAL(currentIndexChanged(int)), this, SLOT(set_new_plane_param(int)));

        QObject::connect(&btnsDelete[i], SIGNAL(clicked()), this, SLOT(delete_plane()));
        QObject::connect(&btnsTrip[i], SIGNAL(clicked()), this, SLOT(load_trip_dialog()));
        QObject::connect(&bxsModeScenary[i], SIGNAL(stateChanged(int)), this, SLOT(change_mode_params(int)));
    }


     qDebug() << "Make some noise 5";


    QIcon icon_delete =  QPixmap(":/images/SOURCE/delete.png");
    for(int i = 0; i < rows; i++) {
        QTableWidgetItem* item = new QTableWidgetItem();
        lst_numbers << QString::number(i);

        tblShowAllPrmtrs.setCellWidget(i, 0, &boxsChoseParameter[i]);
        tblShowAllPrmtrs.setCellWidget(i, 1, &lnsTextHmax[i]);
        tblShowAllPrmtrs.setCellWidget(i, 2, &lnsTextTbeg[i]);


        QWidget wgt;




        //tblShowAllPrmtrs.setItem(i, 0, item);



        tblShowAllPrmtrs.setCellWidget(i, 4, &btnsTrip[i]);





        tblShowAllPrmtrs.setCellWidget(i, 5, &bxsModeScenary[i]);
        tblShowAllPrmtrs.setCellWidget(i, 6, &btnsDelete[i]);
        tblShowAllPrmtrs.setCellWidget(i, 3, &btnsGroup[i]);


        btnsDelete[i].setIcon(icon_delete);



        mapBtnGroupEdit.insert(&btnsGroup[i], i);
        lnsTextHmax[i].setText(QString::number(tasks[i].H_max));
        lnsTextTbeg[i].setText(QString::number(tasks[i].T_beg));




        lnsTextHmax[i].setValidator(new QDoubleValidator(0.0, 40000.0, 6, &lnsTextHmax[i]));
        lnsTextTbeg[i].setValidator(new QDoubleValidator(0.0, 40000.0, 6, &lnsTextTbeg[i]));

        QObject::connect(&btnsGroup[i], SIGNAL(clicked()), this, SLOT(start_shift_dlg() ));
        QObject::connect(&lnsTextTbeg[i], SIGNAL(textChanged(QString)), this, SLOT(change_Tbeg()));
        QObject::connect(&lnsTextHmax[i], SIGNAL(textChanged(QString)), this, SLOT(change_maxH()));


        //tblShowAllPrmtrs.setItem(i, 0, boxsChoseParameter[i]);
    }
    qDebug() << "Make some noise 6ml;mkll; kk,";


    tblShowAllPrmtrs.setColumnWidth(6, 20);
    tblShowAllPrmtrs.setColumnWidth(5, 55);
    tblShowAllPrmtrs.setColumnWidth(3, 130);
    tblShowAllPrmtrs.setColumnWidth(1, 149);
    tblShowAllPrmtrs.setColumnWidth(2, 120);


    QObject::connect(&tblShowAllPrmtrs, SIGNAL(cellChanged (int, int) ),
                     this, SLOT(change_table_param(int, int)) );

    qDebug() << "Make some noise 7";
    this->exec();

}

