#ifndef AIROPORTDLG_H
#define AIROPORTDLG_H


#include <QtGui>
#include <QDebug>
#include "../WIDGETS/GeoCoordsWidget.h"

#include "../FLIGHT_CALCULATION/flight_plan.hpp"





/*******************************************************************************************************
 *                    ����� ����������, ��������, �������������� ����������                            *
 *******************************************************************************************************/




//������ ��� �������������� ���������� ���������


class EDIT_AIRPORT : public QDialog {
    Q_OBJECT
private:
    QLineEdit* m_ptxtName;
    GeoCoordsWidget* m_ptxtLa;
    GeoCoordsWidget* m_ptxtFi;
    QLineEdit* m_ptxtAz;
    QPushButton btnOk;

    Model_Struct::Airport *airport;





    public:
    void set_name_for_new();
   EDIT_AIRPORT(QWidget* pwgt/*= 0*/);



public slots:
   void start(Model_Struct::Airport *airport);
   void save_param();
signals:
   void reinit();
   void add_new(Model_Struct::Airport airport);
};











//����� �� ������� ���� ����������

class EDIT_AIRPORT_DLG : public  QDialog {
    Q_OBJECT
private:



    QListWidget listNamesAirports;
    QToolBar* bar;
    QStringList lstAirports; // ������ ���� ����������
    void init_airports(const char* file);
    void reinit_datas(char *file);
    //void reinit();


public:

    int sltRow;
    int count_params;

    std::vector <Model_Struct::Airport> airports;            //c����� ����������
    std::map<std::string, Model_Struct::Airport> airportMap; // �� ����� ����� ���������� ��������

    EDIT_AIRPORT_DLG(QWidget* pwgt/*= 0*/);

public slots:
    void start();
    void delete_param();
    void edit_params();
    void add_new();
    void reinit_list_widget();
    void add_new( Model_Struct::Airport tmpAirport);
signals:
};



#endif // AIROPORTDLG_H
