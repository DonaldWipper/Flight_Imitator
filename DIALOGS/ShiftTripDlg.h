#ifndef SHIFTTRIPDLG_H
#define SHIFTTRIPDLG_H

#include <QtGui>
#include <QDialog>
#include "../FLIGHT_CALCULATION/flight_plan.hpp"

class SHIFT_TRIP_DLG : public QDialog {
    Q_OBJECT
private:
    QLineEdit* m_ptxtAz;
    QLineEdit* m_ptxtDist;
    QLineEdit* m_ptxtCountPlanes;
    QComboBox* mBoxNumPlane;

public:

    SHIFT_TRIP_DLG(QWidget* pwgt/*= 0*/,
                   Model_Struct::FlightTask &task);


    double az, dist, count;
    Model_Struct::FlightTask *ptrTask;
    Model_Struct::FlightTask task2;
    QString Az();
    QString Dist();
    QString Count();
    int cur_num;

public slots:
    void changeCount();
    void setParams();
    void changeParams();
    void apply();
    void start_dialog();

};












#endif // SHIFTTRIPDLG_H
