/****************************************************************************
** Meta object code from reading C++ file 'time_reciever_client.h'
**
** Created: Thu 15. May 15:02:43 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../NETWORK_CONNECTION/time_reciever_client.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'time_reciever_client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MY_CLIENT[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   11,   10,   10, 0x05,
      52,   47,   10,   10, 0x05,
      86,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      96,   10,   10,   10, 0x08,
     110,   10,   10,   10, 0x08,
     126,   10,   10,   10, 0x08,
     166,   10,   10,   10, 0x08,
     185,   10,   10,   10, 0x08,
     215,  201,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MY_CLIENT[] = {
    "MY_CLIENT\0\0CurTime\0send_time_signal(TimeInfo&)\0"
    "info\0send_change_zones_signal(InpInfo)\0"
    "restart()\0set_visible()\0slotReadyRead()\0"
    "slotError(QAbstractSocket::SocketError)\0"
    "slotSendToServer()\0slotConnected()\0"
    "nPort,strHost\0reconnect(int,QString)\0"
};

const QMetaObject MY_CLIENT::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MY_CLIENT,
      qt_meta_data_MY_CLIENT, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MY_CLIENT::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MY_CLIENT::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MY_CLIENT::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MY_CLIENT))
        return static_cast<void*>(const_cast< MY_CLIENT*>(this));
    return QWidget::qt_metacast(_clname);
}

int MY_CLIENT::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: send_time_signal((*reinterpret_cast< TimeInfo(*)>(_a[1]))); break;
        case 1: send_change_zones_signal((*reinterpret_cast< InpInfo(*)>(_a[1]))); break;
        case 2: restart(); break;
        case 3: set_visible(); break;
        case 4: slotReadyRead(); break;
        case 5: slotError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 6: slotSendToServer(); break;
        case 7: slotConnected(); break;
        case 8: reconnect((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void MY_CLIENT::send_time_signal(TimeInfo & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MY_CLIENT::send_change_zones_signal(InpInfo _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MY_CLIENT::restart()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
