/****************************************************************************
** Meta object code from reading C++ file 'TripPointsDlg.h'
**
** Created: Thu 15. May 15:03:28 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/TripPointsDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TripPointsDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TRIP_POINTS_DLG[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   17,   16,   16, 0x05,
      48,   16,   16,   16, 0x05,
      63,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,   16,   16,   16, 0x0a,
      85,   16,   16,   16, 0x0a,
      98,   16,   16,   16, 0x0a,
     116,  110,   16,   16, 0x0a,
     139,   16,   16,   16, 0x0a,
     154,   16,   16,   16, 0x0a,
     166,   16,   16,   16, 0x0a,
     178,   16,   16,   16, 0x0a,
     201,  192,   16,   16, 0x0a,
     227,  219,   16,   16, 0x0a,
     256,  219,   16,   16, 0x0a,
     282,   16,   16,   16, 0x0a,
     299,  110,   16,   16, 0x0a,
     325,   16,   16,   16, 0x0a,
     353,  346,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TRIP_POINTS_DLG[] = {
    "TRIP_POINTS_DLG\0\0,\0draw_trip_point(float,float)\0"
    "reinit_datas()\0stop_motion()\0start()\0"
    "edit_point()\0add_point()\0La,Fi\0"
    "add_point(float,float)\0delete_point()\0"
    "save_trip()\0load_trip()\0apply_datas()\0"
    "num_trip\0set_row_trip(int)\0num_air\0"
    "set_new_airport_takeoff(int)\0"
    "set_new_airport_land(int)\0set_on_the_map()\0"
    "setCurCoords(float,float)\0"
    "reinit_list_widget()\0tmpPnt\0"
    "add_new(Model_Struct::BillPoint)\0"
};

const QMetaObject TRIP_POINTS_DLG::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TRIP_POINTS_DLG,
      qt_meta_data_TRIP_POINTS_DLG, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TRIP_POINTS_DLG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TRIP_POINTS_DLG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TRIP_POINTS_DLG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TRIP_POINTS_DLG))
        return static_cast<void*>(const_cast< TRIP_POINTS_DLG*>(this));
    return QDialog::qt_metacast(_clname);
}

int TRIP_POINTS_DLG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: draw_trip_point((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 1: reinit_datas(); break;
        case 2: stop_motion(); break;
        case 3: start(); break;
        case 4: edit_point(); break;
        case 5: add_point(); break;
        case 6: add_point((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 7: delete_point(); break;
        case 8: save_trip(); break;
        case 9: load_trip(); break;
        case 10: apply_datas(); break;
        case 11: set_row_trip((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: set_new_airport_takeoff((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: set_new_airport_land((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: set_on_the_map(); break;
        case 15: setCurCoords((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 16: reinit_list_widget(); break;
        case 17: add_new((*reinterpret_cast< Model_Struct::BillPoint(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void TRIP_POINTS_DLG::draw_trip_point(float _t1, float _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TRIP_POINTS_DLG::reinit_datas()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void TRIP_POINTS_DLG::stop_motion()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
