/****************************************************************************
** Meta object code from reading C++ file 'main_menu.h'
**
** Created: Thu 15. May 15:03:20 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../main_menu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'main_menu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MAIN_MENU[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x0a,
      24,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MAIN_MENU[] = {
    "MAIN_MENU\0\0clickStart()\0clickStop()\0"
};

const QMetaObject MAIN_MENU::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MAIN_MENU,
      qt_meta_data_MAIN_MENU, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MAIN_MENU::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MAIN_MENU::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MAIN_MENU::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MAIN_MENU))
        return static_cast<void*>(const_cast< MAIN_MENU*>(this));
    return QWidget::qt_metacast(_clname);
}

int MAIN_MENU::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: clickStart(); break;
        case 1: clickStop(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
