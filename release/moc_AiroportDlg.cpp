/****************************************************************************
** Meta object code from reading C++ file 'AiroportDlg.h'
**
** Created: Thu 15. May 15:03:11 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/AiroportDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AiroportDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EDIT_AIRPORT[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      31,   23,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      62,   23,   13,   13, 0x0a,
      92,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EDIT_AIRPORT[] = {
    "EDIT_AIRPORT\0\0reinit()\0airport\0"
    "add_new(Model_Struct::Airport)\0"
    "start(Model_Struct::Airport*)\0"
    "save_param()\0"
};

const QMetaObject EDIT_AIRPORT::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_EDIT_AIRPORT,
      qt_meta_data_EDIT_AIRPORT, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EDIT_AIRPORT::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EDIT_AIRPORT::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EDIT_AIRPORT::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EDIT_AIRPORT))
        return static_cast<void*>(const_cast< EDIT_AIRPORT*>(this));
    return QDialog::qt_metacast(_clname);
}

int EDIT_AIRPORT::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: reinit(); break;
        case 1: add_new((*reinterpret_cast< Model_Struct::Airport(*)>(_a[1]))); break;
        case 2: start((*reinterpret_cast< Model_Struct::Airport*(*)>(_a[1]))); break;
        case 3: save_param(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void EDIT_AIRPORT::reinit()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void EDIT_AIRPORT::add_new(Model_Struct::Airport _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_EDIT_AIRPORT_DLG[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x0a,
      26,   17,   17,   17, 0x0a,
      41,   17,   17,   17, 0x0a,
      55,   17,   17,   17, 0x0a,
      65,   17,   17,   17, 0x0a,
      97,   86,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EDIT_AIRPORT_DLG[] = {
    "EDIT_AIRPORT_DLG\0\0start()\0delete_param()\0"
    "edit_params()\0add_new()\0reinit_list_widget()\0"
    "tmpAirport\0add_new(Model_Struct::Airport)\0"
};

const QMetaObject EDIT_AIRPORT_DLG::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_EDIT_AIRPORT_DLG,
      qt_meta_data_EDIT_AIRPORT_DLG, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EDIT_AIRPORT_DLG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EDIT_AIRPORT_DLG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EDIT_AIRPORT_DLG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EDIT_AIRPORT_DLG))
        return static_cast<void*>(const_cast< EDIT_AIRPORT_DLG*>(this));
    return QDialog::qt_metacast(_clname);
}

int EDIT_AIRPORT_DLG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: start(); break;
        case 1: delete_param(); break;
        case 2: edit_params(); break;
        case 3: add_new(); break;
        case 4: reinit_list_widget(); break;
        case 5: add_new((*reinterpret_cast< Model_Struct::Airport(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
