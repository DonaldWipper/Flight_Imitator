/****************************************************************************
** Meta object code from reading C++ file 'ListTripPointsDlg.h'
**
** Created: Thu 15. May 15:03:06 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/ListTripPointsDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ListTripPointsDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EDIT_TRIP_POINT_DLG[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,
      34,   30,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      84,   67,   20,   20, 0x0a,
     123,   20,   20,   20, 0x0a,
     136,   20,   20,   20, 0x0a,
     153,   20,   20,   20, 0x0a,
     177,  171,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EDIT_TRIP_POINT_DLG[] = {
    "EDIT_TRIP_POINT_DLG\0\0reinit()\0pnt\0"
    "add_new(Model_Struct::BillPoint)\0"
    "ptrPoint2,V_max2\0"
    "start(Model_Struct::BillPoint*,double)\0"
    "save_param()\0setRoundMotion()\0"
    "setLinearMotion()\0state\0setEnableSpeedSet(int)\0"
};

const QMetaObject EDIT_TRIP_POINT_DLG::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_EDIT_TRIP_POINT_DLG,
      qt_meta_data_EDIT_TRIP_POINT_DLG, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EDIT_TRIP_POINT_DLG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EDIT_TRIP_POINT_DLG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EDIT_TRIP_POINT_DLG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EDIT_TRIP_POINT_DLG))
        return static_cast<void*>(const_cast< EDIT_TRIP_POINT_DLG*>(this));
    return QDialog::qt_metacast(_clname);
}

int EDIT_TRIP_POINT_DLG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: reinit(); break;
        case 1: add_new((*reinterpret_cast< Model_Struct::BillPoint(*)>(_a[1]))); break;
        case 2: start((*reinterpret_cast< Model_Struct::BillPoint*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 3: save_param(); break;
        case 4: setRoundMotion(); break;
        case 5: setLinearMotion(); break;
        case 6: setEnableSpeedSet((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void EDIT_TRIP_POINT_DLG::reinit()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void EDIT_TRIP_POINT_DLG::add_new(Model_Struct::BillPoint _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
