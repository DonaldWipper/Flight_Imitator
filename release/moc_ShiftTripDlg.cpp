/****************************************************************************
** Meta object code from reading C++ file 'ShiftTripDlg.h'
**
** Created: Thu 15. May 15:03:36 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/ShiftTripDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ShiftTripDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SHIFT_TRIP_DLG[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      30,   15,   15,   15, 0x0a,
      42,   15,   15,   15, 0x0a,
      57,   15,   15,   15, 0x0a,
      65,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SHIFT_TRIP_DLG[] = {
    "SHIFT_TRIP_DLG\0\0changeCount()\0setParams()\0"
    "changeParams()\0apply()\0start_dialog()\0"
};

const QMetaObject SHIFT_TRIP_DLG::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SHIFT_TRIP_DLG,
      qt_meta_data_SHIFT_TRIP_DLG, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SHIFT_TRIP_DLG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SHIFT_TRIP_DLG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SHIFT_TRIP_DLG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SHIFT_TRIP_DLG))
        return static_cast<void*>(const_cast< SHIFT_TRIP_DLG*>(this));
    return QDialog::qt_metacast(_clname);
}

int SHIFT_TRIP_DLG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: changeCount(); break;
        case 1: setParams(); break;
        case 2: changeParams(); break;
        case 3: apply(); break;
        case 4: start_dialog(); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
