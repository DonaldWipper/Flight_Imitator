/****************************************************************************
** Meta object code from reading C++ file 'planes_params_edit.h'
**
** Created: Thu 15. May 15:02:48 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/planes_params_edit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'planes_params_edit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PARAMS_EDITOR[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      63,   58,   14,   14, 0x0a,
      91,   14,   14,   14, 0x0a,
     106,   14,   14,   14, 0x0a,
     128,  120,   14,   14, 0x0a,
     160,   14,   14,   14, 0x0a,
     170,   14,   14,   14, 0x0a,
     200,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PARAMS_EDITOR[] = {
    "PARAMS_EDITOR\0\0"
    "send_sgnl_dlg_info(Model_Struct::PlaneTTD)\0"
    "item\0setNumRow(QListWidgetItem*)\0"
    "delete_param()\0edit_params()\0plnData\0"
    "add_new(Model_Struct::PlaneTTD)\0"
    "add_new()\0emit_signal_for_next_dialog()\0"
    "start()\0"
};

const QMetaObject PARAMS_EDITOR::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PARAMS_EDITOR,
      qt_meta_data_PARAMS_EDITOR, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PARAMS_EDITOR::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PARAMS_EDITOR::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PARAMS_EDITOR::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PARAMS_EDITOR))
        return static_cast<void*>(const_cast< PARAMS_EDITOR*>(this));
    return QDialog::qt_metacast(_clname);
}

int PARAMS_EDITOR::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: send_sgnl_dlg_info((*reinterpret_cast< Model_Struct::PlaneTTD(*)>(_a[1]))); break;
        case 1: setNumRow((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 2: delete_param(); break;
        case 3: edit_params(); break;
        case 4: add_new((*reinterpret_cast< Model_Struct::PlaneTTD(*)>(_a[1]))); break;
        case 5: add_new(); break;
        case 6: emit_signal_for_next_dialog(); break;
        case 7: start(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void PARAMS_EDITOR::send_sgnl_dlg_info(Model_Struct::PlaneTTD _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
