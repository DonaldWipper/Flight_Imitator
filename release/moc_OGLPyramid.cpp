/****************************************************************************
** Meta object code from reading C++ file 'OGLPyramid.h'
**
** Created: Thu 15. May 15:02:33 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../OGLPyramid.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OGLPyramid.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OGLPyramid[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x05,
      45,   12,   11,   11, 0x05,
      74,   72,   11,   11, 0x05,
     122,   72,   11,   11, 0x05,
     148,   72,   11,   11, 0x05,
     183,  174,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     215,  204,   11,   11, 0x0a,
     247,  239,   11,   11, 0x0a,
     278,  274,   11,   11, 0x0a,
     307,   11,   11,   11, 0x0a,
     326,  321,   11,   11, 0x0a,
     360,  348,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OGLPyramid[] = {
    "OGLPyramid\0\0str\0info_planes_changed(QString)\0"
    "info_time_changed(QString)\0,\0"
    "send_coords(TimeInfo,std::vector<TargetParGeo>)\0"
    "changeCoords(float,float)\0"
    "newTripPoint(float,float)\0num_trip\0"
    "change_row_trip(int)\0_infoZones\0"
    "change_sectors(InpInfo)\0CurTime\0"
    "updateTimeState(TimeInfo&)\0a,b\0"
    "accept_new_trip(float,float)\0recalculate()\0"
    "mode\0set_scenary_mode(int)\0cnt1,ctr2,r\0"
    "setForbCirc(ASh::GeoGraph::GeoCoord,ASh::GeoGraph::GeoCoord,double)\0"
};

const QMetaObject OGLPyramid::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_OGLPyramid,
      qt_meta_data_OGLPyramid, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OGLPyramid::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OGLPyramid::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OGLPyramid::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OGLPyramid))
        return static_cast<void*>(const_cast< OGLPyramid*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int OGLPyramid::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: info_planes_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: info_time_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: send_coords((*reinterpret_cast< const TimeInfo(*)>(_a[1])),(*reinterpret_cast< const std::vector<TargetParGeo>(*)>(_a[2]))); break;
        case 3: changeCoords((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 4: newTripPoint((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 5: change_row_trip((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: change_sectors((*reinterpret_cast< InpInfo(*)>(_a[1]))); break;
        case 7: updateTimeState((*reinterpret_cast< TimeInfo(*)>(_a[1]))); break;
        case 8: accept_new_trip((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 9: recalculate(); break;
        case 10: set_scenary_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: setForbCirc((*reinterpret_cast< ASh::GeoGraph::GeoCoord(*)>(_a[1])),(*reinterpret_cast< ASh::GeoGraph::GeoCoord(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void OGLPyramid::info_planes_changed(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void OGLPyramid::info_time_changed(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void OGLPyramid::send_coords(const TimeInfo & _t1, const std::vector<TargetParGeo> & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void OGLPyramid::changeCoords(float _t1, float _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void OGLPyramid::newTripPoint(float _t1, float _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void OGLPyramid::change_row_trip(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
