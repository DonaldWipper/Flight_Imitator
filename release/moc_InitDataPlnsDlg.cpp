/****************************************************************************
** Meta object code from reading C++ file 'InitDataPlnsDlg.h'
**
** Created: Thu 15. May 15:03:01 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../DIALOGS/InitDataPlnsDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'InitDataPlnsDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_INIT_PLNS_DLG[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      30,   14,   14,   14, 0x05,
      44,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      68,   14,   14,   14, 0x0a,
      76,   14,   14,   14, 0x0a,
      99,   14,   14,   14, 0x0a,
     118,   14,   14,   14, 0x0a,
     133,   14,   14,   14, 0x0a,
     148,   14,   14,   14, 0x0a,
     166,   14,   14,   14, 0x0a,
     190,  182,   14,   14, 0x0a,
     215,   14,   14,   14, 0x0a,
     230,   14,   14,   14, 0x0a,
     244,   14,   14,   14, 0x0a,
     266,  258,   14,   14, 0x0a,
     294,   14,   14,   14, 0x0a,
     317,   14,   14,   14, 0x0a,
     341,  335,   14,   14, 0x0a,
     362,  335,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_INIT_PLNS_DLG[] = {
    "INIT_PLNS_DLG\0\0refresh_task()\0"
    "stop_motion()\0signal_change_mode(int)\0"
    "start()\0new_init_flight_data()\0"
    "load_trip_dialog()\0load_scenary()\0"
    "save_scenary()\0refresh_scenary()\0"
    "add_new_plane()\0num_par\0"
    "set_new_plane_param(int)\0delete_plane()\0"
    "change_Tbeg()\0change_maxH()\0row,col\0"
    "change_table_param(int,int)\0"
    "dublicate_last_plane()\0start_shift_dlg()\0"
    "state\0change_all_mode(int)\0"
    "change_mode_params(int)\0"
};

const QMetaObject INIT_PLNS_DLG::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_INIT_PLNS_DLG,
      qt_meta_data_INIT_PLNS_DLG, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &INIT_PLNS_DLG::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *INIT_PLNS_DLG::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *INIT_PLNS_DLG::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_INIT_PLNS_DLG))
        return static_cast<void*>(const_cast< INIT_PLNS_DLG*>(this));
    return QDialog::qt_metacast(_clname);
}

int INIT_PLNS_DLG::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: refresh_task(); break;
        case 1: stop_motion(); break;
        case 2: signal_change_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: start(); break;
        case 4: new_init_flight_data(); break;
        case 5: load_trip_dialog(); break;
        case 6: load_scenary(); break;
        case 7: save_scenary(); break;
        case 8: refresh_scenary(); break;
        case 9: add_new_plane(); break;
        case 10: set_new_plane_param((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: delete_plane(); break;
        case 12: change_Tbeg(); break;
        case 13: change_maxH(); break;
        case 14: change_table_param((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: dublicate_last_plane(); break;
        case 16: start_shift_dlg(); break;
        case 17: change_all_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: change_mode_params((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void INIT_PLNS_DLG::refresh_task()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void INIT_PLNS_DLG::stop_motion()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void INIT_PLNS_DLG::signal_change_mode(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
