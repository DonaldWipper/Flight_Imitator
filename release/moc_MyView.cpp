/****************************************************************************
** Meta object code from reading C++ file 'MyView.h'
**
** Created: Thu 15. May 15:02:38 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MyView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MyView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MyView[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x0a,
      16,    7,    7,    7, 0x0a,
      30,    7,    7,    7, 0x0a,
      42,   38,    7,    7, 0x0a,
      57,    7,    7,    7, 0x0a,
      64,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MyView[] = {
    "MyView\0\0start()\0recalculate()\0pause()\0"
    "str\0pause(QString)\0stop()\0_continue()\0"
};

const QMetaObject MyView::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_MyView,
      qt_meta_data_MyView, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MyView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MyView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MyView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MyView))
        return static_cast<void*>(const_cast< MyView*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int MyView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: start(); break;
        case 1: recalculate(); break;
        case 2: pause(); break;
        case 3: pause((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: stop(); break;
        case 5: _continue(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
