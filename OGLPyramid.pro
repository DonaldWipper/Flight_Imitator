TEMPLATE     = app


##### BOOST ####
Boost_ROOT = D:\Libraries\boost_1_47\

INCLUDEPATH += $${Boost_ROOT}



DEPENDPATH  += $${Boost_ROOT}

LIBS +=  $${Boost_ROOT}\lib\mingw\debug\libboost_date_time-mgw46-mt-d-1_47.a

##### BOOST ####




QT          += opengl network
HEADERS      = OGLPyramid.h \
    plane.h \
    MyView.h \
    drawing_map.h \
    FLIGHT_CALCULATION/AMZGO_exchange.hpp \
    FLIGHT_CALCULATION/AllProc.hpp \
    FLIGHT_CALCULATION/flight_plan.hpp \
    FLIGHT_CALCULATION/Geograph.hpp \
    FLIGHT_CALCULATION/Geograph.hpp \
    NETWORK_CONNECTION/time_reciever_client.h \
    planes_params_edit.h \
    DIALOGS/planes_params_edit.h \
    DIALOGS/PlaneParametersDialog.h \
    planes_info_object.h \
    DIALOGS/InitDataPlnsDlg.h \
    DIALOGS/ListTripPointsDlg.h \
    DIALOGS/AiroportDlg.h \
    NETWORK_CONNECTION/data_sender_server.h \
    main_menu.h \
    DIALOGS/PortsDlg.h \
    DIALOGS/TripPointsDlg.h \
    ../Users/utiralov/Desktop/glx.h \
    AMZGO_network_exchange.h \
    AMZGO_network_exchange.h \
    FLIGHT_CALCULATION/AMZGO_network_exchange.h \
    WIDGETS/GeoCoordsWidget.h \
    DIALOGS/ShiftTripDlg.h
SOURCES	     = OGLPyramid.cpp \
               main.cpp \
    plane.cpp \
    drawing_map.cpp \
    FLIGHT_CALCULATION/flight_plan.cpp \
    FLIGHT_CALCULATION/Geograph.cpp \
    NETWORK_CONNECTION/time_reciever_client.cpp \
    DIALOGS/planes_params_edit.cpp \
    DIALOGS/PlaneParametersDialog.cpp \
    planes_info_object.cpp \
    DIALOGS/InitDataPlnsDlg.cpp \
    DIALOGS/ListTripPointsDlg.cpp \
    DIALOGS/AiroportDlg.cpp \
    NETWORK_CONNECTION/data_sender_server.cpp \
    main_menu.cpp \
    DIALOGS/PortsDlg.cpp \
    DIALOGS/TipPoints.cpp \
    WIDGETS/GeoCoordsWidget.cpp \
    DIALOGS/ShiftTripDlg.cpp
win32:TARGET = ../Flight_imitator

OTHER_FILES += \
    NETWORK_CONNECTION/time_reciever_client.�pp

RESOURCES += \
    resources.qrc
