#include "plane.h"



#ifndef CALLBACK
#define CALLBACK
#endif




/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+��������������� �������, ������ ��� ���������� ��������� ��������� +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


void CALLBACK tessErrorCB2(GLenum errorCode)
{
    const GLubyte *errorStr;
    errorStr = gluErrorString(errorCode);
    std::cerr << "[ERROR]: " << errorStr << std::endl;
}

void CALLBACK tessBeginCB2(GLenum which)
{
    glBegin(which);
}


void CALLBACK tessEndCB2()
{
    glEnd();
}


void CALLBACK tessVertexCB2(const GLvoid *data)
{
    // cast back to double type
    const GLdouble *ptr = (const GLdouble*)data;
    glVertex3dv(ptr);
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


PLANE::PLANE() {
    Fi = 0.0;
    La = 0.0;
    H = 0.0;
    prev_Fi = 0.0;
    prev_La = 0.0;
    prev_H = 0.0;
    float r, g, b;
    /*r = (rand() % 10) / 10.0;
    g = (rand() % 10) / 10.0;
    b = (rand() % 10) / 10.0;*/


    r = 0;
    g = 0;
    b = 0;
    color[0] = r;
    color[1] = g;
    color[2] = b;


    rot_v[0] = 0.0;
    rot_v[1] = 0.0;
    rot_v[1] = 0.0;
    rot_angle = 0.0;
    Tkt = 0;
    Nkt = 0;
    scale_plane = 0.01;

    plane_size = 0.05;
    shiftX = 0.0; // OpenGL ��������
    shiftY = 0.0;
    shiftZ = 0.0;
    num_point = 0;
    is_in_group = false;
    CurTime = boost::posix_time::second_clock::local_time();
}



PLANE::PLANE(float _Fi, float _La, float _H) {
    Fi = _Fi;
    La = _La;
    H = _H;

    prev_Fi = 0.0;
    prev_La = 0.0;
    prev_H = 0.0;

    scale_plane = 1;

    float r, g, b;
    r = (rand() % 10) / 10.0;
    g = (rand() % 10) / 10.0;
    b = (rand() % 10) / 10.0;

    color[0] = r;
    color[1] = g;
    color[2] = b;



    rot_v[0] = 0.0;
    rot_v[1] = 0.0;
    rot_v[1] = 0.0;

    rot_angle = 0.0;
    is_in_group = false;
    Tkt = 0;
    Nkt = 0;
    CurTime = boost::posix_time::second_clock::local_time();
}



void PLANE::set_color_3f(float r, float g, float b) {

    color[0] = r;
    color[1] = g;
    color[2] = b;
}


void PLANE::set_random_color() {
     float r, g, b;
    r = (rand() % 10) / 10.0;
    g = (rand() % 10) / 10.0;
    b = (rand() % 10) / 10.0;
    color[0] = r;
    color[1] = g;
    color[2] = b;
}

void PLANE::set_coords(float _Fi, float _La, float _H) {
    prev_Fi = Fi;
    prev_La = La;
    prev_H = H;
    Fi = _Fi;
    La = _La;
    H = _H;

}



void PLANE::get_vector_and_angle() {
    dir_v[0] = Fi - prev_Fi;
    dir_v[1] = La - prev_La;
    dir_v[2] = 0;
    rot_v[1] = 0;
    rot_v[2] = dir_v[0] / sqrt(pow(dir_v[0], 2) + pow(dir_v[2], 2));
    rot_v[0] = -dir_v[2] / dir_v[0] * rot_v[2];
    rot_angle = acos(dir_v[1] / sqrt(pow(dir_v[0], 2) + pow(dir_v[1], 2) + pow(dir_v[2], 2)));
}


void PLANE::fill_PZDN_frag ( )  {
     Model_Struct::FillBillFragments(PZDN);
}




/*bool PLANE::GetPlanesGeoPar(const boost::posix_time::ptime& CurTime2) {
   if(Nkt == 1) {
       boost::posix_time::time_duration td = CurTime2 - CurTime;
       Tkt = td.total_milliseconds() / 1000.;
   }


   CurTime = CurTime2;
   Plane.TargetType = PZDN.PlanePerfCharact.TargetType;

   //Ec�� ���� ������, �� ������� �� ����� �� ������
   if(Nkt == 0) {
       return false;
   }


   //������ false, ���� ��� �� �������� ��� ��� ����
   if((Nkt * Tkt < PZDN.T_beg) || (Nkt * Tkt > PZDN.T_end)) {
       return false;
   }

   if (Model_Struct::GetPlanesParfromAirBill(PZDN, Nkt * Tkt, Plane)) {

       Plane.id = PZDN.id;
       Plane.pl_time = CurTime;
       Plane.Speed *= 3.6;
   }
   return true;
}*/



bool PLANE::GetPlanesGeoPar(const boost::posix_time::ptime& CurTime2) {

   CurTime = CurTime2; // C�������� ������� �����
   Plane.TargetType = PZDN.PlanePerfCharact.TargetType;

   //Ec�� ���� ������, �� ������� �� ����� �� ������ � ���������� ���������� �����, ��� ���������
   if(Nkt == 0) {
       PZDN.T_beg_ptime = CurTime;
       return false;
   }

   boost::posix_time::time_duration td = CurTime2 - PZDN.T_beg_ptime;
   double curTime = td.total_milliseconds() / 1000.;

   //������ false, ���� ��� �� �������� ��� ��� ����
   if((curTime < PZDN.T_beg) || (curTime > PZDN.T_end)) {
       return false;
   }

   if (Model_Struct::GetPlanesParfromAirBill(PZDN, curTime, Plane)) {

       Plane.id = PZDN.id;
       Plane.pl_time = CurTime;
       Plane.Speed *= 3.6;
   }
   return true;
}

void PLANE::set_plane_size(float size) {
    plane_size = size;

}






void PLANE::draw_plane_pic(GLuint id, float shift_plan_x, float shift_plan_y, float shift_plan_z) {
    get_vector_and_angle();
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glPushMatrix();
    glTranslatef(shift_plan_x, shift_plan_y, shift_plan_z);
    glTranslatef(0.0, 0.0, 850);
     glScalef(plane_size / 9.0,  plane_size / 9.0, plane_size / 9.0);
    glRotatef(- 180.0 / 3.14 * rot_angle, rot_v[0], rot_v[1], rot_v[2]);
    glCallList(id);
    glPopMatrix();

}



GLuint PLANE::make_plane_vertex_list() {
    std::ifstream in("FILES/model_plane.txt");
    GLuint id = glGenLists(1);   // create a display list
    if(!id) return 0;            // failed to create a list, return 0

    GLUtesselator *tess = gluNewTess(); // create a tessellator
    if(!tess) return 0 ;  // failed to create tessellation object, return 0

    glNewList(id, GL_COMPILE);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);


    gluTessCallback(tess, GLU_TESS_BEGIN, (void (__stdcall*)(void))tessBeginCB2);
    gluTessCallback(tess, GLU_TESS_END,(void (__stdcall*)(void))tessEndCB2);
    gluTessCallback(tess, GLU_TESS_ERROR, (void (__stdcall*)(void))tessErrorCB2);
    gluTessCallback(tess, GLU_TESS_VERTEX, (void (__stdcall*)(void))tessVertexCB2);

    GLdouble **vertex = new GLdouble*[70];
    for(int i = 0; i < 70; i++) {
        vertex[i] = new GLdouble[3];
    }
    int i = 0;

    float minY = 100, maxY = -100;
   gluTessBeginPolygon(tess, 0);
        gluTessBeginContour(tess);
        while(!in.eof()) {
            GLdouble a, b, c;
            c = 0;
            in >> a >> b;
            vertex[i][0] = a;
            vertex[i][1] = b;
            vertex[i][2] = 0;
            gluTessVertex(tess, vertex[i], vertex[i]);
            i++;
        }
        gluTessEndContour(tess);
    gluTessEndPolygon(tess);



    glBegin(GL_POLYGON);
     for(int i = 0; i < 100; i++) {
         GLfloat h = 0.5 / 100;
         GLfloat a, b;
         a = -4.5 + i * h;
         b = 3 + sqrt((1 - pow(a + 4, 2) / 0.25) );
         glVertex3f(a, b, 0);
         if(b < minY) minY = b;
         if(b > maxY) maxY = b;
     }

     for(int i = 0; i < 100; i++) {
         GLfloat  h = 0.5 / 100;
         GLfloat a, b;
         a = -3.5 - i * h;
         b = 3 + sqrt( (1 - pow(a + 4, 2) / 0.25));
         glVertex3f(a, b, 0);
         if(b < minY) minY = b;
         if(b > maxY) maxY = b;
     }

     glEnd();


     glBegin(GL_POLYGON);
      for(int i = 0; i < 100; i++) {
          GLfloat h = 0.5 / 100;
          GLfloat a, b;
          a = 3.5 + i * h;
          b = 3 + sqrt((1 - pow(a - 4, 2) / 0.25) );
          glVertex3f(a, b, 0);
          if(b < minY) minY = b;
          if(b > maxY) maxY = b;
      }

      for(int i = 0; i < 100; i++) {
          GLfloat  h = 0.5 / 100;
          GLfloat a, b;
          a = 4.5 - i * h;
          b = 3 + sqrt( (1 - pow(a - 4, 2) / 0.25));
          glVertex3f(a, b, 0);
          if(b < minY) minY = b;
          if(b > maxY) maxY = b;
      }

      glEnd();


   glBegin(GL_POLYGON);
    for(int i = 0; i < 100; i++) {
        GLfloat h = 0.5 / 100;
        GLfloat a, b;
        a = -6.5 + i * h;
        b = 2 + sqrt((1 - pow(a + 6, 2) / 0.25) );
        glVertex3f(a, b, 0);
        if(b < minY) minY = b;
        if(b > maxY) maxY = b;
    }

    for(int i = 0; i < 100; i++) {
        GLfloat  h = 0.5 / 100;
        GLfloat a, b;
        a = -5.5 - i * h;
        b = 2 + sqrt( (1 - pow(a + 6, 2) / 0.25));
        glVertex3f(a, b, 0);
        if(b < minY) minY = b;
        if(b > maxY) maxY = b;
    }

    glEnd();



    glBegin(GL_POLYGON);
     for(int i = 0; i < 100; i++) {
         GLfloat h = 0.5 / 100;
         GLfloat a, b;
         a = 5.5 + i * h;
         b = 2 + sqrt((1 - pow(a - 6, 2) / 0.25) );
         glVertex3f(a, b, 0);
         if(b < minY) minY = b;
         if(b > maxY) maxY = b;
     }

     for(int i = 0; i < 100; i++) {
         GLfloat  h = 0.5 / 100;
         GLfloat a, b;
         a = 6.5 - i * h;
         b = 2 + sqrt( (1 - pow(a - 6, 2) / 0.25));
         glVertex3f(a, b, 0);
         if(b < minY) minY = b;
         if(b > maxY) maxY = b;
     }
      glEnd();



    glBegin(GL_POLYGON);
    for(int i = 0; i < 100; i++) {
        GLfloat h = 1.5 / 100;
        GLfloat a, b;
        a = -1.5 + i * h;
        b = 6 + sqrt(9 * (1 - pow(a, 2) / 2.25));
        glVertex3f(a, b, 0);
        if(b < minY) minY = b;
        if(b > maxY) maxY = b;
    }

    for(int i = 0; i < 100; i++) {
        GLfloat  h = 1.5 / 100;
        GLfloat a, b;
        a = 1.5 - i * h;
        b = 6 + sqrt(9 * (1 - pow(a, 2) / 2.25));
        glVertex3f(a, b, 0);
        if(b < minY) minY = b;
        if(b > maxY) maxY = b;
    }
    qDebug() << "MAX " << minY << " " << maxY;
    glEnd();
    gluDeleteTess(tess);
    glEnd();




    glEndList();
    return id;
}





