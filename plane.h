#ifndef _plane_h_
#define _plane_h_



#include <iostream>
#include <gl/GLU.h>
#include <gl/GL.h>
#include <QGLWidget>


#include <math.h>
#include<vector>

#include <string>
#include <QtGui>
#include"FLIGHT_CALCULATION/flight_plan.hpp"



using namespace std;

/*******************************************************************************
*   ����� �������, ������� ����� ������� �������������� ���������� ������� � *
*   ������� ������ �������,                                                    *
*   ���������� ������ �������,                                                 *
*   �������� ���������� ������������ ���,                                      *
*   ������� ���� �������� � ������, ����� ��������� �� ����������� � ����������*
*   ������������ �������� �������,                                             *
*   �������� ���������� �� ������                                              *
********************************************************************************/



class geo_coords {
public:
    float x;
    float y;
    float h;
    string time;

    geo_coords() {
        x = 0;
        y = 0;
        h = 0;
    }

    geo_coords(float _x, float _y, float _h) {
        x = _x;
        y = _y;
        h = _h;
    }
};





class PLANE {

    /*���������� �� ���������� ����(��� �������� ���������� ��������) */
    float prev_Fi;
    float prev_La;
    float prev_H;



    float  rot_v[3];    // ���������� ������� ��������
    float  rot_angle;   // ���� ��������
    float  dir_v[3];    //������ ����������� ����������

    double R;           //������ �������� � ��������� ���������� �����





public:
    float color[3];     //���� �������� RGB
    float Fi;           //
    float La;           //
    float H;            //������
    float Spd;          //C�������

    float plane_size;


    PLANE();
    PLANE(float Fi, float La, float H);


    float scale_plane;   //�������, ������ ��� ��������� �������

    double Tkt; // C������ ����� � ��������
    double Nkt; // ����� �����

    //����� ��� ��������� ����
    vector<geo_coords> points; // ���� ����


    float shiftX; // OpenGL ��������
    float shiftY;
    float shiftZ;

    int num_point; // ����� ������� ������������ �����


    double shiftGroupD;    //��������� �������� � ������
    double shiftGroupAz;   //������ �������� � ������



    bool is_in_group;   // ������������� �������������� ������

    boost::posix_time::ptime CurTime;

    Model_Struct::AirParGeo Plane;

    Model_Struct::Airbill PZDN;                  // ������� �������
    std::vector<Model_Struct::AirParGeo> BigMas; // ����� ����� ������������ �������
    bool GetPlanesGeoPar(const boost::posix_time::ptime& CurTime); // ���������� ����� �����
    void fill_PZDN_frag () ; // ��������� ��������� �������




    GLuint make_plane_vertex_list();    //������� ����� ������� �������� � ��������� �� ������
    void draw_plane_pic(GLuint id, float shift_plan_x, float shift_plan_y, float shift_plan_z);
    void set_color_3f(float r, float g, float b);

    void set_random_color();

    void set_coords(float Fi, float La, float H);
    void get_vector_and_angle();
    void set_plane_size(float size);

};

#endif

