#pragma once

#ifndef ALLPROC_HPP
#define ALLPROC_HPP

namespace ASh { namespace Proc {

	//========================================================================================
	//                          ���������� ���� �����
	//========================================================================================
	template <typename T>
	int GetSign(const T &x)
	{
		if (x > 0)
			return 1;
		else if (x < 0)
			return -1;
		else
			return 0;    
	}

}}

#endif // ALLPROC_HPP
