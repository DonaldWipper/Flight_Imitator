#pragma once

#ifndef AMZGO_network_exchange_H
#define AMZGO_network_exchange_H

#include <complex>
#include <string>
#include <vector>
#include <algorithm>
#include <QDataStream>

//------------------------------------------------------------------------------
#if defined QT
    typedef qint64 __int64;
    typedef qint16 __int16;
    typedef qint8  __int8;
#endif
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
class ZNK_Geometry
{
   public:
          uint   number; // ����� ���
          float  Dmin;   // ��������� ���������, [��]
          float  Dmax;   // �������� ���������, [��]
          float  Azmin;  // ��������� ������, [����]
          float  Azmax;  // �������� ������, [����]
          float  Fi_prd; //     ����� � �������
          float  La_prd; // ��������� ��� �������,[����]
          float  Fi_prm; //     ����� � �������
          float  La_prm; // ��������� ��� �������,[����]
          float color[3]; // ���� ����������

   // ����������� �� ���������
   ZNK_Geometry(void)
   {
    number=0;
    Dmin=-1;
    Dmax=-1;
    Azmin=-1;
    Azmax=-1;
    Fi_prd=0;
    La_prd=0;
    Fi_prm=0;
    La_prm=0;


   }

   // ����������� �����������
   ZNK_Geometry(const ZNK_Geometry &right)
   {
    number=right.number;
    Dmin=right.Dmin;
    Dmax=right.Dmax;
    Azmin=right.Azmin;
    Azmax=right.Azmax;
    Fi_prd=right.Fi_prd;
    La_prd=right.La_prd;
    Fi_prm=right.Fi_prm;
    La_prm=right.La_prm;

  }

   // �������� ������������
   ZNK_Geometry& operator=(const ZNK_Geometry& right)
   {
    number=right.number;
    Dmin=right.Dmin;
    Dmax=right.Dmax;
    Azmin=right.Azmin;
    Azmax=right.Azmax;
    Fi_prd=right.Fi_prd;
    La_prd=right.La_prd;
    Fi_prm=right.Fi_prm;
    La_prm=right.La_prm;

    return *this;
   }

   friend QDataStream& operator>>(QDataStream& inn, ZNK_Geometry& src)
   {
    inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

    inn >> src.number
        >> src.Dmin
        >> src.Dmax
        >> src.Azmin
        >> src.Azmax
        >> src.Fi_prd
        >> src.La_prd
        >> src.Fi_prm
        >> src.La_prm;

    return inn;
   }

   friend  QDataStream& operator<<(QDataStream& out, const ZNK_Geometry& src)
   {
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);

    out << src.number
        << src.Dmin
        << src.Dmax
        << src.Azmin
        << src.Azmax
        << src.Fi_prd
        << src.La_prd
        << src.Fi_prm
        << src.La_prm;

    return out;
   }
};
//------------------------------------------------------------------------------

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                          ����� Joiner � Urr 
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
class Urrl_par
{
   public:
          ZNK_Geometry Zone;

          float B_norm;             // ������� � ������� ���

          int    Nkn;               // ����� ������ ������������ ���������� 64,128,256,512,1024
          int    Tp;                // ������������ ������� ����������, [��]
          float  Ti;                // ������������ ��������, [��]
          int    Fd;                // �������� �������, [���]
          int    Nnkd;              // ����� ���������� ������ ��������� 1-400
          int    Nkd;               // ����� ������� ���������
          int    dT;                // ���������� ����� �������� ���������, [���]
          int    Naz;               // ����� ������� �� �������
          float  Frab;              // ���������� �������, [���]
          int    TerritoryType;     // ��� ��������� - ������������ ������
          int    PrGlk;             // ������� - ������������� ������
          int    PrGroz;            // ������� - �������� ������
          int    PrAtm;             // ������� - ����������� ������
          int    PrVzaim;           // ������� - �������� ������
          int 	 Year;              //     ���, ����� � ����
          int 	 Month;             //   ���������� ��� ������� ���������
          int 	 Day;               // ��� ������������ ������� ���������� �����
          float  k_vert;            // ������� ����������� ������������ ���������� ��� ������� ���   k_vert*EprVert+k_hor*EprGor
          float  k_hor;             // ������� ����������� �������������� ���������� ��� ������� ��� k_vert*EprVert+k_hor*EprGor
          int    dTheta;            // ������ � �������� ������ � ����� �� ���� ����� ��� ������� ��������� ��������
          int    dPhi;              // ������ � �������� ������ � ����� �� ������� ��� ������� ��������� ��������
          uint   Epr_outtype;       // ��� ����������� �������� -> 1 - ����� �� �����, ����� ������ �� ������� ��������� �������
          float  dBz;               // ������������ �������� � ����, [����]
          float  Kt[3];             // ������������ ��������� ������
          float  Ugm[3][2];         // ���� �����, [����]
          std::vector<float> MasAz; // ������ ����������� ����� �� �������
          std::vector<float> VNZ;   // ��������� ������-������ ���

   // ����������� �� ���������
   Urrl_par(void)
   {
    Zone=ZNK_Geometry();

    B_norm=0;
    Nkn=0;
    Tp=0;
    Ti=0;
    Fd=0;
    Nnkd=0;
    Nkd=0;
    dT=0;
    Naz=0;
    Frab=0;

    TerritoryType=2;
    PrGlk=1;
    PrGroz=0;
    PrAtm=1;
    PrVzaim=0;    

    Year=1970;
    Month=1;
    Day=1;

    k_vert=0.5;
    k_hor=0.5;
    dTheta=5;
    dPhi=15;
    Epr_outtype=1;

    dBz=0;

    for (int i=0;i<3;i++)
         Kt[i]=1.0;

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=0;

    MasAz.clear();

    VNZ.clear();
   }

   // ����������� �����������
   Urrl_par(const Urrl_par &right)
   {
    Zone=right.Zone;
    B_norm=right.B_norm;
    Nkn=right.Nkn;
    Tp=right.Tp;
    Ti=right.Ti;
    Fd=right.Fd;
    Nnkd=right.Nnkd;
    Nkd=right.Nkd;
    dT=right.dT;
    Naz=right.Naz;
    Frab=right.Frab;
    TerritoryType=right.TerritoryType;
    PrGlk=right.PrGlk;
    PrGroz=right.PrGroz;
    PrAtm=right.PrAtm;
    PrVzaim=right.PrVzaim;
    Year=right.Year;
    Month=right.Month;
    Day=right.Day;    
    k_vert=right.k_vert;
    k_hor=right.k_hor;
    dTheta=right.dTheta;
    dPhi=right.dPhi;
    Epr_outtype=right.Epr_outtype;

    dBz=right.dBz;

    for (int i=0;i<3;i++)
         Kt[i]=right.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=right.Ugm[i][j];

    MasAz.clear();
    std::copy(right.MasAz.begin(), right.MasAz.end(), std::back_inserter(MasAz));

    if (!right.VNZ.empty())
       {
        VNZ.clear();
        std::copy(right.VNZ.begin(),right.VNZ.end(), std::back_inserter(VNZ));
       }
   }

   // �������� ������������
   Urrl_par& operator=(const Urrl_par& right)
   {
    Zone=right.Zone;
    B_norm=right.B_norm;
    Nkn=right.Nkn;
    Tp=right.Tp;
    Ti=right.Ti;
    Fd=right.Fd;
    Nnkd=right.Nnkd;
    Nkd=right.Nkd;
    dT=right.dT;
    Naz=right.Naz;
    Frab=right.Frab;
    TerritoryType=right.TerritoryType;
    PrGlk=right.PrGlk;
    PrGroz=right.PrGroz;
    PrAtm=right.PrAtm;
    PrVzaim=right.PrVzaim;
    Year=right.Year;
    Month=right.Month;
    Day=right.Day;
    k_vert=right.k_vert;
    k_hor=right.k_hor;
    dTheta=right.dTheta;
    dPhi=right.dPhi;
    Epr_outtype=right.Epr_outtype;

    dBz=right.dBz;

    for (int i=0;i<3;i++)
         Kt[i]=right.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=right.Ugm[i][j];

    MasAz.clear();
    std::copy(right.MasAz.begin(), right.MasAz.end(), std::back_inserter(MasAz));

    if (!right.VNZ.empty())
       {
        VNZ.clear();
        std::copy(right.VNZ.begin(),right.VNZ.end(), std::back_inserter(VNZ));
       }

    return *this;
   }

   friend QDataStream& operator>>(QDataStream& inn, Urrl_par& src)
   {
    inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

    inn >> src.Zone
        >> src.B_norm
        >> src.Nkn
        >> src.Tp
        >> src.Ti
        >> src.Fd
        >> src.Nnkd
        >> src.Nkd
        >> src.dT
        >> src.Naz
        >> src.Frab
        >> src.TerritoryType
        >> src.PrGlk
        >> src.PrGroz
        >> src.PrAtm
        >> src.PrVzaim
        >> src.Year
        >> src.Month
        >> src.Day
        >> src.k_vert
        >> src.k_hor
        >> src.dTheta
        >> src.dPhi
        >> src.Epr_outtype
        >> src.dBz;

    for (int i=0;i<3;i++)
         inn >> src.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              inn >> src.Ugm[i][j];

	src.MasAz.clear();
    float Az_c=0;
    for (int i=0;i<src.Naz;i++)
        { 
		 inn >> Az_c;
		 src.MasAz.push_back(Az_c);
		}

    float Vnz_c=0;
    if (src.Nkd*src.Naz!=0)
       {
        src.VNZ.clear();
        for (int i=0; i<src.Nkd*src.Naz; ++i)
            {
             inn >> Vnz_c;
             src.VNZ.push_back(Vnz_c);
            }
       }

    return inn;
   }

   friend  QDataStream& operator<<(QDataStream& out, const Urrl_par& src)
   {
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);

    out << src.Zone
        << src.B_norm
        << src.Nkn
        << src.Tp
        << src.Ti
        << src.Fd
        << src.Nnkd
        << src.Nkd
        << src.dT
        << src.Naz
        << src.Frab
        << src.TerritoryType
        << src.PrGlk
        << src.PrGroz
        << src.PrAtm
        << src.PrVzaim
        << src.Year
        << src.Month
        << src.Day
        << src.k_vert
        << src.k_hor
        << src.dTheta
        << src.dPhi
        << src.Epr_outtype
        << src.dBz;

    for (int i=0;i<3;i++)
         out << src.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              out << src.Ugm[i][j];

    for (std::vector<float>::const_iterator it=src.MasAz.begin(), it_end=src.MasAz.end();it!=it_end;++it)
         out << (*it);

    for (std::vector<float>::const_iterator it=src.VNZ.begin(), it_end=src.VNZ.end(); it!=it_end; ++it)
         out << (*it);

    return out;
   }  
};
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                          ����� Joiner � Urr
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                       ����� Urr � Ionoserver
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//--- ������� ������ -----------------------------------------------------------
class ZNK_Calc_Par
{
   public:
          ZNK_Geometry  Zone_geom;   //
          float   Frab;              // ������� �������, [���] -> 0 - ������� ����������� �������, ����� ��� ��������� �������
          int     Nkn;               // ����� ������ ������������ ���������� 64,128,256,512,1024
          int     Tp;                // ������������ ������� ����������, [��]
          float   Ti;                // ������������ ��������, [��]
          int     Fd;                // �������� �������, ���
          int     dT;                // ���������� ����� �������� ���������, ���.
          int     Naz;               // ����� ������� �� �������          
          int     TerritoryType;     // ��� ��������� - ������������ ������
          int     PrGlk;             // ������� - ������������� ������
          int     PrGroz;            // ������� - �������� ������
          int     PrAtm;             // ������� - ����������� ������
          int     PrVzaim;           // ������� - �������� ������
          int     Target_type;       // ��� ����
          int     Year;              //
          int     Month;             //
          int     Day;               //
          float   T_cur;             // GMT �����  � ���� -> � + �/60 + ���/3600
          std::vector<float> MasAz;  // ������ ����������� ����� �� �������

   // ����������� �� ���������
   ZNK_Calc_Par(void)
   {
    Zone_geom = ZNK_Geometry();
	
    Frab=0;
    Nkn=-1;
    Tp=-1;
    Ti=-1;
    Fd=-1;
    dT=0;
    Naz=0;
    TerritoryType=2;
    PrGlk=1;
    PrGroz=0;
    PrAtm=1;
    PrVzaim=0;
    Target_type=-1;
    Year=-1;
    Month=-1;
    Day=-1;
    T_cur=-1;

    MasAz.clear();
   }

   // ����������� �����������
   ZNK_Calc_Par(const ZNK_Calc_Par &right)
   {
    Zone_geom=right.Zone_geom;
	
	Frab=right.Frab;
    Nkn=right.Nkn;
    Tp=right.Tp;
    Ti=right.Ti;
    Fd=right.Fd;
    dT=right.dT;
    Naz=right.Naz;
    TerritoryType=right.TerritoryType;
    PrGlk=right.PrGlk;
    PrGroz=right.PrGroz;
    PrAtm=right.PrAtm;
    PrVzaim=right.PrVzaim;
    Target_type=right.Target_type;
    Year=right.Year;
    Month=right.Month;
    Day=right.Day;
    T_cur=right.T_cur;

    MasAz.clear(); 
    std::copy(right.MasAz.begin(), right.MasAz.end(), std::back_inserter(MasAz));
   }

   // �������� ������������
   ZNK_Calc_Par& operator=(const ZNK_Calc_Par& right)
   {
    Zone_geom=right.Zone_geom;

	Frab=right.Frab;
    Nkn=right.Nkn;
    Tp=right.Tp;
    Ti=right.Ti;
    Fd=right.Fd;
    dT=right.dT;
    Naz=right.Naz;
    TerritoryType=right.TerritoryType;
    PrGlk=right.PrGlk;
    PrGroz=right.PrGroz;
    PrAtm=right.PrAtm;
    PrVzaim=right.PrVzaim;
    Target_type=right.Target_type;
    Year=right.Year;
    Month=right.Month;
    Day=right.Day;
    T_cur=right.T_cur;

    MasAz.clear();
    std::copy(right.MasAz.begin(), right.MasAz.end(), std::back_inserter(MasAz));

    return *this;
   }

   friend QDataStream& operator>>(QDataStream& inn, ZNK_Calc_Par& src)
   {
    inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

    inn >> src.Zone_geom
		>> src.Frab
        >> src.Nkn
        >> src.Tp
        >> src.Ti
        >> src.Fd
        >> src.dT
        >> src.Naz
        >> src.TerritoryType
        >> src.PrGlk
        >> src.PrGroz
        >> src.PrAtm
        >> src.PrVzaim
        >> src.Target_type
        >> src.Year
        >> src.Month
        >> src.Day
        >> src.T_cur;
    
	src.MasAz.clear();
    float Az_c=0;
    for (int i=0;i<src.Naz;i++)
        { 
		 inn >> Az_c;
		 src.MasAz.push_back(Az_c);
		}

    return inn;
   }

   friend  QDataStream& operator<<(QDataStream& out, const ZNK_Calc_Par& src)
   {
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);

    out << src.Zone_geom
		<< src.Frab
        << src.Nkn
        << src.Tp
        << src.Ti
        << src.Fd
        << src.dT
        << src.Naz
        << src.TerritoryType
        << src.PrGlk
        << src.PrGroz
        << src.PrAtm
        << src.PrVzaim
        << src.Target_type
        << src.Year
        << src.Month
        << src.Day
        << src.T_cur;

    for (std::vector<float>::const_iterator it=src.MasAz.begin(), it_end=src.MasAz.end();it!=it_end;++it)
         out << (*it);

    return out;
   }
};
//--- ������� ������ -----------------------------------------------------------

//--- �������� ������ ----------------------------------------------------------
class ZNK_Work_Par
{
   public:
          uint   znk_number;        // ����� ���
          float  Frab;              // ����������� �������, [���]
          float  Gr_min;            // �����������  ��������� ��������� ���� �� ����������� �������, [��]
          float  Gr_max;            // ������������ ��������� ��������� ���� �� ����������� �������, [��]
          int    Nnkd;              // ����� ���������� ������ ��������� 1-400
          int    Nkd;               // ����� ������� ���������
          int    N_VNZ;             // ����� ���������
          std::vector<float> VNZ;   // ��������� ������-������ ���
          float  dBz;               // ������������ �������� � ����, [����]
          float  Kt[3];             // ����������� ��������� ������
          float  Ugm[3][2];         // ���� �����, [����]

   // ����������� �� ���������
   ZNK_Work_Par(void)
   {
    znk_number=0;
    Frab=0;
    Gr_min=0;
    Gr_max=0;
    Nnkd=0;
    Nkd=0;
	
    N_VNZ=0;
    VNZ.clear();

    //---
    dBz=0;

    for (int i=0;i<3;i++)
         Kt[i]=1.0;

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=0;
    //---
   }

   // ����������� �����������
   ZNK_Work_Par(const ZNK_Work_Par &right)
   {
    znk_number=right.znk_number;
    Frab=right.Frab;
    Gr_min=right.Gr_min;
    Gr_max=right.Gr_max;
    Nnkd=right.Nnkd;
    Nkd=right.Nkd;

    //---
    N_VNZ=right.N_VNZ;

    if (N_VNZ!=0)
       {
        VNZ.clear();
        std::copy(right.VNZ.begin(), right.VNZ.end(), std::back_inserter(VNZ));
       }
    //---

    //---
    dBz=right.dBz;

    for (int i=0;i<3;i++)
         Kt[i]=right.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=right.Ugm[i][j];
    //---
   }

   // �������� ������������
   ZNK_Work_Par& operator=(const ZNK_Work_Par& right)
   {
    znk_number=right.znk_number;
    Frab=right.Frab;
    Gr_min=right.Gr_min;
    Gr_max=right.Gr_max;
    Nnkd=right.Nnkd;
    Nkd=right.Nkd;

    //---
    N_VNZ=right.N_VNZ;

    if (N_VNZ!=0)
       {
        VNZ.clear();
        std::copy(right.VNZ.begin(), right.VNZ.end(), std::back_inserter(VNZ));
       }
    //---

    //---
    dBz=right.dBz;

    for (int i=0;i<3;i++)
         Kt[i]=right.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              Ugm[i][j]=right.Ugm[i][j];
    //---

    return *this;
   }

   friend QDataStream& operator>>(QDataStream& inn, ZNK_Work_Par& src)
   {
    inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

    inn >> src.znk_number >> src.Frab >> src.Gr_min >> src.Gr_max >> src.Nnkd >> src.Nkd;

    //---
    inn >> src.N_VNZ;

    if (src.N_VNZ!=0)
       {
        float Vnz_c=0;
        src.VNZ.clear();

        for (int i=0; i<src.N_VNZ; ++i)
            {
             inn >> Vnz_c;
             src.VNZ.push_back(Vnz_c);
            }
       }
    //---

    //---
    inn >> src.dBz;

    for (int i=0;i<3;i++)
         inn >> src.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              inn >> src.Ugm[i][j];
    //---

    return inn;
   }

   friend QDataStream& operator<<(QDataStream& out, const ZNK_Work_Par& src)
   {
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);

    out << src.znk_number << src.Frab << src.Gr_min << src.Gr_max << src.Nnkd << src.Nkd;

    //---
    out << src.N_VNZ;

    for (std::vector<float>::const_iterator it=src.VNZ.begin(), it_end=src.VNZ.end(); it!=it_end; ++it)
         out << (*it);
    //---

    //---
    out << src.dBz;

    for (int i=0;i<3;i++)
         out << src.Kt[i];

    for (int i=0;i<3;i++)
         for (int j=0;j<2;j++)
              out << src.Ugm[i][j];
    //---

    return out;
   }
};
//--- �������� ������ ----------------------------------------------------------
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                       ����� Urr � Ionoserver
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                      ����� Urr � ���������� �����
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//--- ������� ������ -----------------------------------------------------------
// ��������� ������� ����������
class TimeInfo
{
    public:
           uint Year;   //
           uint Month;  //
           uint Day;    //
           uint msDay;  // ����� ����������� � ������ ���

    // ����������� �� ���������
    TimeInfo(void)
    {
     Year=1970;
     Month=1;
     Day=1;
     msDay=0;
    }

    // ����������� �����������
    TimeInfo(const TimeInfo &right)
    {
     Year=right.Year;
     Month=right.Month;
     Day=right.Day;
     msDay=right.msDay;
    }

    // �������� ������������
    TimeInfo& operator=(const TimeInfo& right)
    {
     Year=right.Year;
     Month=right.Month;
     Day=right.Day;
     msDay=right.msDay;

     return *this;
    }

    friend QDataStream& operator>>(QDataStream& inn, TimeInfo& src)
    {
     inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

     inn >> src.Year >> src.Month >> src.Day >> src.msDay;

     return inn;
    }

    friend QDataStream& operator<<(QDataStream& out, const TimeInfo& src)
    {
     out.setFloatingPointPrecision(QDataStream::SinglePrecision);

     out << src.Year << src.Month << src.Day << src.msDay;

     return out;
    }
};

class InpInfo
{
    public:
           TimeInfo T_inf;
           uint N_Zones;
           std::vector<ZNK_Geometry> Zones;

    // ����������� �� ���������
    InpInfo(void)
    {
     T_inf = TimeInfo();

     N_Zones=0;
     Zones.clear();
    }

    // ����������� �����������
    InpInfo(const InpInfo &right)
    {
     T_inf=right.T_inf;

     N_Zones=right.N_Zones;

     Zones.clear();
     if (N_Zones!=0)
         std::copy(right.Zones.begin(),right.Zones.end(),std::back_inserter(Zones));
    }

    // �������� ������������
    InpInfo& operator=(const InpInfo& right)
    {
        T_inf=right.T_inf;

        N_Zones=right.N_Zones;

        Zones.clear();
        if (N_Zones!=0)
            std::copy(right.Zones.begin(),right.Zones.end(),std::back_inserter(Zones));

     return *this;
    }

    friend QDataStream& operator>>(QDataStream& inn, InpInfo& src)
    {
     inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

     inn >> src.T_inf >> src.N_Zones;

     src.Zones.clear();
     for (uint i=0;i<src.N_Zones;i++)
         {
          ZNK_Geometry gm;
          inn >> gm;
          src.Zones.push_back(gm);
         }

     return inn;
    }

    friend QDataStream& operator<<(QDataStream& out, const InpInfo& src)
    {
     out.setFloatingPointPrecision(QDataStream::SinglePrecision);

     out << src.T_inf << src.N_Zones;

     for (std::vector<ZNK_Geometry>::const_iterator it=src.Zones.begin(), it_end=src.Zones.end();it!=it_end;++it)
          out << (*it);

     return out;
    }
};
//--- ������� ������ -----------------------------------------------------------

//--- �������� ������ ----------------------------------------------------------
// ��������� ���������� �� ������ ��������
class TargetParGeo
{
   public:
          std::string TargetID;          // ��� ��� ������� �� ������ �����
          std::string TargetType;        // ��� ���� ��� ������ ���������� ���
          float Target_Pos_Fi;           // ��������������
          float Target_Pos_La;           // ���������� ����, [����]
          float Altitude;                // ������ ������ ����, [�]
          float Course;                  // ���� ����, [����]
          float Speed;                   // ������ �������� ����, [��/�]
          float Accel;                   // ���������, [�/�^2]

   // ����������� �� ���������
   TargetParGeo(void)
   {
    TargetID = std::string("NO_DATA");
    TargetType=std::string("Boeing737");
    Target_Pos_Fi=0;
    Target_Pos_La=0;
    Altitude = 0;
    Course = 0;
    Speed = 0;
    Accel = 0;
   }

    // ����������� �����������
    TargetParGeo(const TargetParGeo &right)
    {
     TargetID = right.TargetID;
     TargetType=right.TargetType;
     Target_Pos_Fi=right.Target_Pos_Fi;
     Target_Pos_La=right.Target_Pos_La;
     Altitude = right.Altitude;
     Course = right.Course;
     Speed = right.Speed;
     Accel = right.Accel;
    }

    // �������� ������������
    TargetParGeo& operator=(const TargetParGeo& right)
    {
     TargetID = right.TargetID;
     TargetType=right.TargetType;
     Target_Pos_Fi=right.Target_Pos_Fi;
     Target_Pos_La=right.Target_Pos_La;
     Altitude = right.Altitude;
     Course = right.Course;
     Speed = right.Speed;
     Accel = right.Accel;

     return *this;
    }

    friend QDataStream& operator>>(QDataStream& inn,  TargetParGeo& src)
    {
     QString id_q, TargetType_q;

     inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

     inn >> id_q
         >> TargetType_q
         >> src.Target_Pos_Fi
         >> src.Target_Pos_La
         >> src.Altitude
         >> src.Course
         >> src.Speed
         >> src.Accel;

     src.TargetID = id_q.toStdString();
     src.TargetType = TargetType_q.toStdString();

     return inn;
    }

    friend  QDataStream& operator<<(QDataStream& out, const TargetParGeo& src)
    {
     out.setFloatingPointPrecision(QDataStream::SinglePrecision);

     out << QString::fromStdString(src.TargetID)
         << QString::fromStdString(src.TargetType)
         << src.Target_Pos_Fi
         << src.Target_Pos_La
         << src.Altitude
         << src.Course
         << src.Speed
         << src.Accel;

     return out;
    }
};

// ��������� ���������� ���������� �� Joiner
class TargetsInfo
{
    public:
           TimeInfo TaktTimePar;                // ����� �����
           uint Planes_count;                   // ����� ����� � �����
           std::vector<TargetParGeo> pTaktData; // ������ ����� -> ���� ����� ��� �� ������ ����� �� ����������

    // ����������� �� ���������
    TargetsInfo(void)
    {
     Planes_count=0;
     pTaktData.clear();
    }

    // ����������� �����������
    TargetsInfo(const TargetsInfo &right)
    {
     TaktTimePar=right.TaktTimePar;
     Planes_count=right.Planes_count;

     pTaktData.clear();
     std::copy(right.pTaktData.begin(),right.pTaktData.end(),std::back_inserter(pTaktData));
    }

    // �������� ������������
    TargetsInfo& operator=(const TargetsInfo& right)
    {
     TaktTimePar=right.TaktTimePar;
     Planes_count=right.Planes_count;

     pTaktData.clear();
     std::copy(right.pTaktData.begin(),right.pTaktData.end(),std::back_inserter(pTaktData));

     return *this;
    }

    friend QDataStream& operator>>(QDataStream& inn, TargetsInfo& src)
    {
     inn.setFloatingPointPrecision(QDataStream::SinglePrecision);

     inn >> src.TaktTimePar >> src.Planes_count;

     if (src.Planes_count!=0)
        {
         TargetParGeo curPl;
         src.pTaktData.clear();

         for (uint it=0;it<src.Planes_count; ++it)
             {
              inn >> curPl;
              src.pTaktData.push_back(curPl);
             }
        }

     return inn;
    }

    friend QDataStream& operator<<(QDataStream& out, const TargetsInfo& src)
    {
     out.setFloatingPointPrecision(QDataStream::SinglePrecision);

     out << src.TaktTimePar << src.Planes_count;

     if (!src.pTaktData.empty())
         for (std::vector<TargetParGeo>::const_iterator it=src.pTaktData.begin(), it_end=src.pTaktData.end();it!=it_end; ++it)
              out << (*it);

     return out;
    }
};
//--- �������� ������ ----------------------------------------------------------
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                      ����� Urr �  ���������� �����
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                         ����� Joiner � ����
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//------------------------------------------------------------------------------
// �������
struct ExchangeInfo
{
    // ��������� ��������� ������� ������.
    struct Header
    {
     long    CW1;             // ����������� ����� 1.
     long    CW2;             // ����������� ����� 2.
     long    Time;            // ����� ������ �������, � ���, � 00:00:00 GMT, January 1, 1970
     long    Reserve1;        // � �������� ���������� ��� �� 1-���������������, 0-������ � ���������.
     long    Reserve2;        // �� ������������.
     long    Type;            // ��� �������.
     long    Length;          // ����� �������(� ������) � ������
    } header;                 // ���������.

    friend QDataStream& operator>>(QDataStream& inn,  ExchangeInfo& src)
    {
     qint32 CW1, CW2, Time, Reserve1, Reserve2, Type, Length;

     inn >> CW1
         >> CW2
         >> Time
         >> Reserve1
         >> Reserve2
         >> Type
         >> Length;

     src.header.CW1 = CW1;
     src.header.CW2 = CW2;
     src.header.Time = Time;
     src.header.Reserve1 = Reserve1;
     src.header.Reserve2 = Reserve2;
     src.header.Type = Type;
     src.header.Length = Length;

     return inn;
    }

    friend  QDataStream& operator<<(QDataStream& out, const ExchangeInfo& src)
    {
     qint32 CW1, CW2, Time, Reserve1, Reserve2, Type, Length;

     CW1=src.header.CW1;
     CW2=src.header.CW2;
     Time=src.header.Time;
     Reserve1=src.header.Reserve1;
     Reserve2=src.header.Reserve2;
     Type=src.header.Type;
     Length=src.header.Length;

     out << CW1
         << CW2
         << Time
         << Reserve1
         << Reserve2
         << Type
         << Length;

     return out;
    }
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ��������� ������������ �������.
struct URR
{
    int         reserve1[7];  // !< �� ������������.
    int         NR;           // ����� ������ ������ 1,2 ( 1 - ���������, 2 - ��� )
    int         VR;           // ��� ������ 1-3 ( 1 - ���, 2 - ����, 3 - ����������� )
    __int16     Tags;         // ��������( ����������� ��� )  1 - ������� �������� �� ���
                              //                              2 - ������� ������ ���
                              //                              3 - ������� ������ ����
                              //                              4 - ������� ������ ���
                              //                              5 - ������� ���������� ���������� ���
                              //
    __int16     kims;         // ������� ������ � ���� (0 - ��� ����, 1 - ������������ � ����).
    int         Tvr;          // ������������ ���� ������, [�].
    int         reserve0;     // !< �� ������������.
    int         Zok;          // !< ����� ���.
    int         Nmzf;         // ����� ����� ����������� ������ 1-4
    int         Npep;         // ����� �������� ��� 1-4
    int         FNa;          // ������� ������ ��������� ������ ���������, ��� 5000-30000
    int         FKa;          //
    int         FNat;         // ������� �������� ��������� ������ ���������, ��� 5000-30000
    int         FKat;         //
    int         FNaty;        // ����������� ������� �������� ��������� ������ ���������, ��� 5000-30000
    int         FKaty;        //
    int         Fr;           // !< ������� �������, ���.
    int         Kfr;          // ���������� ������� ������ � ������ ��������� 1-20
    int         PrI;          // ������� ��������� �������� 0,1
    int         Bprd;         // ������ ���������, ���� -8,0,8
    int         Tp;           // ������������ ������� ����������, �� 20, 25, 50, 100
    int         Ti;           // ������������ ��������, � �������� �� 8,16,32,64
    int         Fd;           // �������� �������, ��� 5,10,20
    int         Bmin;         // ������� ���� �� �������, ���� -30-30( ������� = 0.1 )
    int         Bmax;         //
    int         Dmin;         // ������� ���� �� ���������, �� 0-3000
    int         Dmax;         //
    int         FNd;          // ������� ��������� ������� ������, ���
    int         FKd;          //
    int         Bny;          // ����������� ������� ���� �� �������, ���� -30-30( ������� = 0.1 )
    int         Bky;          //
    int         Rny;          // ����������� ������� ���� �� ���������, �� 0-3000
    int         Rky;          //
    int         FNdy;         // ����������� ������� ��������� ������� ������, ��� 5000-30000
    int         FKdy;         //
    int         SDt;          // !< ����� ��������� ������� �� ��������, ���.
    int         Nnkd;         // ����� ���������� ������ ��������� 1-400
    int         Nkd;          // !< ����� ������� ���������.
    int         dT;           // !< ���������� ����� �������� ���������, ���.
    int         Nkn;          // !< ����� ������ ������������ ���������� 64,128,256,512,1024.
    int         SP;           // !< ������� ���������� ������ �������������� ���������� 0,25,50.
    int         Nnn;          // !< ����� ������ �������������� ���������� 1,2,3.
    int         Nku;          // ����� ������� ��������� 0,2
    int         SDf;          // ����� ��������� ������� �� �������, �� 0-100
    int         AM;           // ��� ����������� ���������
    int         ND;           // ������ �������� ���
    float       dBz;          // ������������ �������� � ����.
    float       Kt[3];        // ����������� ��������� ������.
    float       Ugm[3][2];    //
    int         Naz;          // ����� ������������ �������
    int         reserve2[59]; // !< �� ������������.

    friend QDataStream& operator>>(QDataStream& inn,  URR& src)
    {
     for (int i=0;i<7;i++)
          inn >> src.reserve1[i];

     inn >> src.NR
         >> src.VR
         >> src.Tags
         >> src.kims         
         >> src.Tvr
         >> src.reserve0
         >> src.Zok
         >> src.Nmzf
         >> src.Npep
         >> src.FNa
         >> src.FKa
         >> src.FNat
         >> src.FKat
         >> src.FNaty
         >> src.FKaty
         >> src.Fr
         >> src.Kfr
         >> src.PrI
         >> src.Bprd
         >> src.Tp
         >> src.Ti
         >> src.Fd
         >> src.Bmin
         >> src.Bmax
         >> src.Dmin
         >> src.Dmax
         >> src.FNd
         >> src.FKd
         >> src.Bny
         >> src.Bky
         >> src.Rny
         >> src.Rky
         >> src.FNdy
         >> src.FKdy
         >> src.SDt
         >> src.Nnkd
         >> src.Nkd
         >> src.dT
         >> src.Nkn
         >> src.SP
         >> src.Nnn
         >> src.Nku
         >> src.SDf
         >> src.AM
         >> src.ND
         >> src.dBz;

     for (int i=0;i<3;i++)
          inn >> src.Kt[i];

     for (int i=0;i<3;i++)
          for (int j=0;j<2;j++)
               inn >> src.Ugm[i][j];

     inn >> src.Naz;

     for (int i=0;i<59;i++)
          inn >> src.reserve2[i];

     return inn;
    }

    friend  QDataStream& operator<<(QDataStream& out, const URR& src)
    {
     for (int i=0;i<7;i++)
          out << src.reserve1[i];

     out  << src.NR
          << src.VR
          << src.Tags
          << src.kims
          << src.Tvr
          << src.reserve0
          << src.Zok
          << src.Nmzf
          << src.Npep
          << src.FNa
          << src.FKa
          << src.FNat
          << src.FKat
          << src.FNaty
          << src.FKaty
          << src.Fr
          << src.Kfr
          << src.PrI
          << src.Bprd
          << src.Tp
          << src.Ti
          << src.Fd
          << src.Bmin
          << src.Bmax
          << src.Dmin
          << src.Dmax
          << src.FNd
          << src.FKd
          << src.Bny
          << src.Bky
          << src.Rny
          << src.Rky
          << src.FNdy
          << src.FKdy
          << src.SDt
          << src.Nnkd
          << src.Nkd
          << src.dT
          << src.Nkn
          << src.SP
          << src.Nnn
          << src.Nku
          << src.SDf
          << src.AM
          << src.ND
          << src.dBz;

     for (int i=0;i<3;i++)
          out << src.Kt[i];

     for (int i=0;i<3;i++)
          for (int j=0;j<2;j++)
               out << src.Ugm[i][j];

     out << src.Naz;

     for (int i=0;i<59;i++)
          out << src.reserve2[i];

     return out;
    }
};

/*
    // C�������� ������� ������� ���������� ��������� ��.
    ExchangeInfo  passp;                                                    // �������
    URR           urr;                                                      // ����������� ������
    unsigned int  time;                                                     // �����, [���] � 00:00:00 GMT, January 1, 1970
    unsigned int  ms;                                                       // ������������ ������� �������
    float         azimuths[AzimuthsCount];                                  // ����������� ��������, [����]
    std::complex<float> buffer[FiltersCount*AzimuthsCount*DistancesCount];  // ������ ������.
*/
class POPZ_Takt
{
    public:
           ExchangeInfo  pssp;                         // �������
           URR  urr;                                   // ����������� ������
           uint time;                                  // �����, [���] � 00:00:00 GMT, January 1, 1970
           uint ms;                                    // ������������ ������� �������
           std::vector<float> mas_Az;                  // ����������� ��������, [����]
           std::vector <std::complex<float> > buffer;  // ������ ������

    friend  QDataStream& operator<<(QDataStream& out, const POPZ_Takt& src)
    {
     out.setFloatingPointPrecision(QDataStream::SinglePrecision);
     out.setByteOrder(QDataStream::LittleEndian);

     //--- ����� ---------------
     out << src.pssp << src.urr << src.time << src.ms;

     for (std::vector<float>::const_iterator it=src.mas_Az.begin(), it_end=src.mas_Az.end();it!=it_end; ++it)
          out << (*it);

     for (std::vector< std::complex<float> >::const_iterator it=src.buffer.begin(), it_end=src.buffer.end();it!=it_end; ++it)
          out << (*it).real() << (*it).imag();

     return out;
    }
};
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
//                          ����� Joiner � ����
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

#endif
