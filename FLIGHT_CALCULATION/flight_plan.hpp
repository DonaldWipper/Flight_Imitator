#pragma once

#ifndef FLIGHT_PLAN_HPP
#define FLIGHT_PLAN_HPP

#include <cmath>
#include <fstream>
#include <QDebug>
#include <vector>
#include <iterator>
#include <iomanip>
#include <algorithm>
#include <fstream>


#include <string>

#include "AMZGO_exchange.hpp"
#include "AMZGO_network_exchange.h"
#include "Geograph.hpp"




namespace Model_Struct
{

	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	//==============================================================================
	//                   ��������� -> ����� ��������� �������
	//==============================================================================
	struct BillPoint
	{
		ASh::GeoGraph::GeoCoord P_geo;  // ����������
		int PointType;                  // ��� ����� ->
		std::string name; //(+����) ������� � �������� �����, ���������� ��� ����������
		   // 4 - �������� ��������,
		   // 6 - ����.
		   //double R_rot;                  // ������ ����� -> ������� ������ �� ���� ������� �����
		   // ����� ����� ���� ������ �������� int Rot_sign ���� ������� -> �� ������� 1 ��� ������ -1

                int N_rot;                    // ����� ������ ->           ������ �� �����
                bool isNeedAcc;               // ����� �� ����� ������� ���� ����� ������ ���������?
                double V_end;
                //false - �� �����
                //true - �����


		// ����������� �� ���������
		BillPoint(void)
		{
                        name = "noname";
			P_geo = ASh::GeoGraph::GeoCoord(0,0);
                        PointType = 4;
                        N_rot = 0;
                        isNeedAcc = false;
                        V_end = 300;
		}

		// ����������� �����������
		BillPoint(const BillPoint &right)
		{
                        name = right.name;
                        P_geo = right.P_geo;
			PointType = right.PointType;
			N_rot = right.N_rot;
                        isNeedAcc = right.isNeedAcc;
                        V_end = right.V_end;
		}

		// �������� ������������
		BillPoint& operator=(const BillPoint& right)
		{
                        name = right.name;
                        P_geo = right.P_geo;
			PointType = right.PointType;
			N_rot = right.N_rot;
                        isNeedAcc = right.isNeedAcc;
                        V_end = right.V_end;
			return *this;
		}


                friend std::ofstream& operator<< (std::ofstream& output, const BillPoint& x) {

                    output << x.name << " ";
                    output << x.P_geo.La() << " ";
                    output << x.P_geo.Fi() << " ";
                    output << x.N_rot << " ";
                    output << x.PointType << " ";
                    output << x.isNeedAcc << " ";
                    output << x.V_end << " " << std::endl;;
                    return output;
                }



                friend std::ifstream& operator >> (std::ifstream& input, BillPoint& x) {
                    double Fi, La;
                    input >> x.name ;
                    input >> La;
                    x.P_geo.setLa(La);
                    input >> Fi;
                    x.P_geo.setFi(Fi);
                    input >> x.N_rot;
                    input >> x.PointType;
                    input >> x.isNeedAcc;
                    input >> x.V_end;
                    return input;
                }


                //����� ����� �� ������-�� �����������
                void shift(double R, double Az, ASh::GeoGraph::GeoCoord &point_prev) {
                   ASh::GeoGraph::LocationCoord loc =   ASh::GeoGraph::DAHT(point_prev, P_geo);
                   double AZ =  -Az + loc.Az();
                   P_geo = ASh::GeoGraph::AKY(P_geo, ASh::GeoGraph::LocationCoord(R, AZ));

                }

	};

	//==============================================================================
	//                ��������� -> �������� ��������� �������
	//==============================================================================
	struct BillFragm
	{
		int FragmType;           // ��� ��������� ->
		//                 1  - ������ �� ���,
		//                 2  - ����� �� �������� ��,
		//                 3  - ������ �� ������������ ����� � ���������� ��������� V��. �� �����������, 
		//                 4  - ������ �� ����������� ��������,
		//                 5  - �������� ��������,
		//                 6  - �������,
		//                 7  - ����,
		//                 8  - ���������� �� �������� �������, ��� ���� ��������� �� ����� � ��� �� ������
		//                 9  - ��������
		//                 10 - ���������� �� ���
		//                 11 - �������� � ������������ ������ �� ������ ������� ����� 
                //                 12 - ������ � �������� ��������� �� ������������ �������, ������������� 2-�� �������




		ASh::GeoGraph::GeoCoord St_Point;    // ���������� ������ ���������
		ASh::GeoGraph::GeoCoord End_Point;   // ���������� ����� ���������

		double T_st, T_end;      // ����� � ��� � ������ � ����� ���������
		double Az_st, Az_end;    // ������� � ������ � ����� ���������
		double V_st, V_end;      // �������� � ������ � ����� ���������
		double H_st, H_end; // ������ � ������ � ����� ���������

		double Ug_pov;           // ���� ��������                                      -> ������������� ��������
		double Rast_pov;         // ���������� ������� ��� ������ ��������             ->    ��� ���������
		double TMT1, TMT2;       //                                                    ->       ��������


		// ����������� �� ���������
		BillFragm()
		{
			FragmType=0;
			St_Point.setFi(0);
			St_Point.setLa(0);

			End_Point.setFi(0);
			End_Point.setLa(0);

			T_st=0;
			T_end=0;

			Az_st=0;
			Az_end=0;

			V_st=0;
			V_end=0;

			H_st = 0;
			H_end = 0;

			Ug_pov=0;
			Rast_pov=0;

			TMT1=0;
			TMT2=0;
		}

		// ����������� �����������
		BillFragm(const BillFragm &right)
		{
			FragmType=right.FragmType;

			St_Point=right.St_Point;
			End_Point=right.End_Point;

			T_st=right.T_st;
			T_end=right.T_end;

			Az_st=right.Az_st;
			Az_end=right.Az_end;

			V_st=right.V_st;
			V_end=right.V_end;

			H_st = right.H_st;
			H_end = right.H_end;

			Ug_pov=right.Ug_pov;
			Rast_pov=right.Rast_pov;

			TMT1=right.TMT1;
			TMT2=right.TMT2;
		}

		// �������� ������������
		BillFragm& operator=(const BillFragm& right)
		{
			FragmType=right.FragmType;

			St_Point=right.St_Point;
			End_Point=right.End_Point;

			T_st=right.T_st;
			T_end=right.T_end;

			Az_st=right.Az_st;
			Az_end=right.Az_end;

			V_st=right.V_st;
			V_end=right.V_end;

			H_st = right.H_st;
			H_end = right.H_end;

			Ug_pov=right.Ug_pov;
			Rast_pov=right.Rast_pov;

			TMT1=right.TMT1;
			TMT2=right.TMT2;

			return *this;
		}

		// ���������� �������� ������ ��� ���������� ��������� ->  �������� �������� ����� ���������� � �������� � �������� � default. -> ���������� ��������
		// ���������� ��� a.shift()
		void shift(void)
		{
			St_Point=End_Point;
			T_st=T_end;
			Az_st=Az_end;
			V_st=V_end;
			H_st = H_end;
			End_Point.setFi(0);
			End_Point.setLa(0);
			T_end=0;
			Az_end=0;
			H_end =0;
			V_end=0;
		}

                //�������� ��������, ���� �������� ������ �������
                void shift_group(double R, double Az) {
                   double AZ =  -Az + Az_st;
                   St_Point = ASh::GeoGraph::AKY(St_Point, ASh::GeoGraph::LocationCoord(R, AZ));
                   End_Point = ASh::GeoGraph::AKY(End_Point, ASh::GeoGraph::LocationCoord(R, AZ));
                }
	};



        struct Trip {
            std::vector<BillPoint> tripPoints;  //���������� �����
            std::string airTakeOff;             //�������� ������
            std::string airLand;                //�������� �������
            int N_airTakeOff;
            int N_airLand;                      //���������� ���������
            double V_max;

            Trip() {
                airTakeOff = "noname";  //������ �������� �� ���������
                airLand = "noname";     //������ �������� �� ���������
                N_airTakeOff = 0;
                N_airLand = 0;
                V_max = 0;
            }

            friend std::ofstream& operator<< (std::ofstream& output, const Trip& x) {
                output << x.tripPoints.size() << std::endl;
                for(uint i = 0; i < x.tripPoints.size(); i++) {
                    output << x.tripPoints[i];
                }
                output << x.airTakeOff << " " << x.N_airTakeOff << std::endl;
                output << x.airLand << " " << x.N_airLand << std::endl;

                return output;
            }



            friend std::ifstream& operator >> (std::ifstream& input, Trip& x) {
                int size;
                input >> size;
                for(int i = 0; i < size; i++) {
                    BillPoint tmpPoint;
                    input >> tmpPoint;
                    x.tripPoints.push_back(tmpPoint);
                }
                input >> x.airTakeOff;
                qDebug() << QString::fromStdString(x.airTakeOff);
                input >> x.N_airTakeOff;
                input >> x.airLand;
                qDebug() << QString::fromStdString(x.airLand);
                input >> x.N_airLand;
                return input;
            }


            // ����������� �����������
            Trip(const Trip &right)
            {
                airTakeOff = right.airTakeOff;
                airLand = right.airLand;
                N_airTakeOff = right.N_airTakeOff;
                N_airLand = right.N_airLand;
                tripPoints.clear();
                tripPoints = right.tripPoints;
                V_max = right.V_max;


            }

            // �������� ������������
            Trip& operator=(const Trip& right)
            {
                airTakeOff = right.airTakeOff;
                airLand = right.airLand;
                N_airTakeOff = right.N_airTakeOff;
                N_airLand = right.N_airLand;
                tripPoints.clear();
                tripPoints = right.tripPoints;
                V_max = right.V_max;
                return *this;
            }


        };

        //==============================================================================
        //         ��������� -> ������� ������� ��������
        //==============================================================================



        struct FlightTask {

            double H_max;
            double H_vzl;
            double T_beg;
            int N_PlaneParam;
            std::string tripName;
            int mode; // ����� ������� ��������� �������
            //0 - �� ������� � ��������
            //1 - ��� ������ � �������
            int cnt_planes_group;  // ���������� �������������� ���������
                                   // �������� � ������
             //1 - ���� ��������� �������
             //> 1 - ���� ������
            std::vector< std::pair<double, double> > shift;





            FlightTask() {
                H_max = 5000;
                H_vzl = 450;

                N_PlaneParam = 0;
                tripName = "noname";

                T_beg = 0.0;
                cnt_planes_group = 0;
                mode = 0;
            }



            friend std::ofstream& operator<< (std::ofstream& output, const FlightTask& x) {
                output << x.N_PlaneParam << std::endl;
                output << x.H_max << std::endl;
                output << x.H_vzl << std::endl;
                output << x.T_beg << std::endl;
                output << x.tripName << std::endl;
                output << x.mode << std::endl;
                output << x.cnt_planes_group << std::endl;
                for(int i = 0; i < x.cnt_planes_group; i++) {
                    output << x.shift[i].first << " " << x.shift[i].second << std::endl;
                }
                return output;
            }



            friend std::ifstream& operator >> (std::ifstream& input, FlightTask& x) {
                x.shift.clear();
                input >> x.N_PlaneParam;
                input >> x.H_max;
                input >> x.H_vzl;
                input >> x.T_beg;
                input >> x.tripName;
                input >> x.mode;
                input >> x.cnt_planes_group;
                for(int i = 0; i < x.cnt_planes_group; i++) {
                    std::pair<double, double> k;
                    input >> k.first >> k.second;
                    x.shift.push_back(k);
                    qDebug() << x.shift[0].first << " " << x.shift[0].second;
                }
                return input;
            }


            // ����������� �����������
            FlightTask(const FlightTask &right)
            {
                shift.clear();
                N_PlaneParam = right.N_PlaneParam;
                H_max = right.H_max;
                H_vzl = right.H_vzl;
                T_beg = right.T_beg;
                tripName = right.tripName;
                mode = right.mode;
                cnt_planes_group = right.cnt_planes_group;
                for(int i = 0; i < right.cnt_planes_group; i++) {
                    std::pair<double, double> k;
                    k.first = right.shift[i].first;
                    k.second = right.shift[i].second;
                    shift.push_back(k);
                }


            }

            // �������� ������������
            FlightTask& operator=(const FlightTask& right)
            {
                shift.clear();
                N_PlaneParam = right.N_PlaneParam;
                H_max = right.H_max;
                H_vzl = right.H_vzl;
                T_beg = right.T_beg;
                tripName = right.tripName;
                mode = right.mode;
                cnt_planes_group = right.cnt_planes_group;
                for(int i = 0; i < right.cnt_planes_group; i++) {
                    std::pair<double, double> k;
                    k.first = right.shift[i].first;
                    k.second = right.shift[i].second;
                    shift.push_back(k);
                }
                return *this;
            }

        };

        struct Airport {
            std::string name;
            ASh::GeoGraph::GeoCoord P_geo;
            double Az;

            Airport() {
                name = "Noname";
                P_geo.setFi(0.0);
                P_geo.setLa(0.0);
                Az = 0.0;
            }

            friend std::ifstream& operator >> (std::ifstream& input, Airport& x) {
                double Fi, La;
                input >> x.name;
                input >> Fi;
                x.P_geo.setFi(Fi);
                input >> La;
                x.P_geo.setLa(La);
                input >> x.Az;
                return input;
            }

            friend std::ofstream& operator << (std::ofstream& output,const Airport& x) {
                double Fi = x.P_geo.Fi(), La = x.P_geo.La();
                output << x.name << " ";
                output << std::fixed << std::setprecision(7) << Fi << " ";
                output << std::fixed << std::setprecision(7) << La << " ";
                output << x.Az << " " << std::endl;
                return output;
            }

            friend bool operator < (const Airport& x, const Airport& y) {
                return (x.name < y.name);
            }

            friend bool operator == (const Airport& x, const Airport& y) {
                return (x.name == y.name);
            }


        };





	//==============================================================================
	//         ��������� -> �������-����������� �������������� ��������
	//==============================================================================
	struct PlaneTTD
	{
		std::string TargetType;                     // ��� �������� -> ��� ������ ���������� ���

		double V_otr;                               // �������� ������ �� ��� �/���
		double V_climb;                             // ���������������� � ����� �/���
		double V_evol;                              // ����������� �������� �/���
		double V_cruise;                            // ����������� �������� �/���
		double V_descend;                           // (+����) ����������������� �/cer 
		

		double acc_dog;                             // ��������� ��� ������ -> ��� ��������� �� ����������� ��������
		double acc_krug;                            // ��������� ��� �������� �������� � �������� -> ���������� ������������������� ��������� ��� ����������� �������� �� ���������� � ���������� ���������
		double acc_pos;                             // ��������� ��� ������� ->


                double acc_cust;                            // ���������, ����� ������� �������� ��������
                double V_cust;                              // ��������, �� ������� ����� ���������
                double V_max;                               // ������������ ��������, > �� ���������� ������

		double L_razb;                              // ����� ������� �� ���
                double Hmax;                                // ������������ ������, �� ������� ����� ��������� �������
		//---

		// ����������� �� ���������
		PlaneTTD()
		{
			TargetType=std::string("XYZ");

			V_otr=0;
			V_evol=0;
			V_cruise=0;
			V_climb=0;
			V_descend = 0;
                        V_max = 0;
			

			acc_dog=0;
			acc_krug=0;
			acc_pos=0;


                        //------------
                        acc_cust = 0.1;
                        V_cust = 300;
                        //------------

			L_razb=0;
                        Hmax = 6000;

		}



                friend std::ifstream& operator >> (std::ifstream& input, PlaneTTD& x) {
                    input >> x.TargetType;
                    input >> x.L_razb;
                    input >> x.V_otr;
                    input >> x.V_evol;
                    input >> x.V_cruise;
                    input >> x.V_climb;
                    input >> x.V_descend;
                    input >> x.V_max;


                    input >> x.acc_dog;
                    input >> x.acc_krug;
                    input >> x.acc_pos;
                    input >> x.Hmax;
                    return input;
                }

                friend std::ofstream& operator << (std::ofstream& output,  const PlaneTTD& x) {
                    output << x.TargetType << std::endl;
                    output << x.L_razb << std::endl;
                    output << x.V_otr << std::endl;
                    output << x.V_evol << std::endl;
                    output << x.V_cruise << std::endl;
                    output << x.V_climb << std::endl;
                    output << x.V_descend << std::endl;
                    output << x.V_max << std::endl;


                    output << x.acc_dog << std::endl;
                    output << x.acc_krug << std::endl;
                    output << x.acc_pos << std::endl;
                    output << x.Hmax << std::endl;

                    return output;
                }


	};

	//==============================================================================
	//                    ��������� -> �������� �������
	//==============================================================================
	class Airbill
	{
	
	
    public:
		std::string id;                             // ��� ������� ��� ������� �� ������ ���������
		PlaneTTD PlanePerfCharact;                  // Plane performance characteristics

		ASh::GeoGraph::GeoCoord Airdrome_vzl;       // ���������� ��������� ������
		double Airdrome_AzVzl;                      // ����������(������) ���

		ASh::GeoGraph::GeoCoord Airdrome_pos;       // ���������� ��������� �������
		double Airdrome_AzPos;                      // ����������(������) ���

		boost::posix_time::ptime T_beg_ptime;                               // ����� ������ ������  -> ������ ������������� ����������� �� 0

                double T_beg;
                double T_end;
		
		double H_vzl;                               // ������ ��������� ������
		double H_max;                               // ������������ ������ �� ������� ����������� ������

		std::vector<BillPoint> MarshTochk;          // ������ ����� ��������� �������

		int Cur_MT;                                 // ������� �����

		std::vector<BillFragm> MarshFragm;          // ������ ���������� ���� �� ���� �������� �� ����/��� ��������/ ��� ����� ��� ����� + ���� �������� ��� ���� c �������������� Cur_MT.


                                                        //  ***************************************
                double R;                               //  *  ��� ��������� ���������� �����     *
                ASh::GeoGraph::GeoCoord centre1;        //  *  ������������ ��� ����������,       *
                ASh::GeoGraph::GeoCoord centre2;        //  *  ������, ������� �� ������ �������� *
                                                        //  *  ��������� ���������� �����         *
                                                        //  ***************************************

                int mode; //����� ������� ��������
                //0 - ������� � ���������������� ���������, ��������� � �������
                //1 - ��� ��������������� ��������


                //int CharakterGr;                          // ?????? �������� �������������� ������  ->  ������������� �������������� � ������ �.�. �� �����
		//�����, ������� ����� ��������� ������� ������� �� ���������
        Airbill();

         void  init_flight_task_from_file(const char* name);
         void  init_random_flight_task();
         void  set_plane_params(Model_Struct::PlaneTTD plane);
         void  set_flight_task(Model_Struct::FlightTask task,
                                       Model_Struct::Airport airTakeoff,
                                       Model_Struct::Airport airLand,
                                       std::vector<Model_Struct::BillPoint> trip);
	};

	//==============================================================================

	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	void Fill_AccelerationFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Acceleration(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_BreakingFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Breaking(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_VPPRazgonFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_VPPRazgon(const double& Ttek, const BillFragm& FR1, AirParGeo &Air);

	void Fill_TakeoffFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Takeoff(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_UpMaxHighFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_UpMaxHigh(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_DescentFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Descent(const double& Ttek, const BillFragm& FR1, AirParGeo &Air);

        void Fill_VPPLandingFragm(Airbill  &PZDN, BillFragm &FR1);
	void GetPar_VPPLanding(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_RotationFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Rotation(float Ttek, BillFragm Fr, AirParGeo &Air);
	void MakingRotation(BillFragm &FR1, Airbill &PZDN, const ASh::GeoGraph::GeoCoord &PP);

	void Fill_LinearMotionFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_LinearMotion(float Ttek, BillFragm Fr, AirParGeo &Air);

	void Fill_CirclingFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_Circling(float Ttek, BillFragm Fr, AirParGeo &Air);

	//����� ��������. �������� � ������������ ������ �� ������ �� � ���������� ��������.

	void Fill_DescentFromMaxFragm(const Airbill& PZDN, BillFragm &FR1);
	void GetPar_DescentFromMax(const double& Ttek, const BillFragm& FR1, AirParGeo &Air);


        //�������� ������ �� �����c�� V_cust � ���������� acc_cust

        void Fill_FixAccelerationFragm(const Airbill& PZDN, BillFragm &FR1);
        void GetPar_FixAcceleration(float Ttek, BillFragm Fr, AirParGeo &Air);






	void FillBillFragments(Airbill &PZDN);
	bool GetPlanesParfromAirBill(const Airbill& PZDN, const double& Tcur, AirParGeo &Plane);
	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	bool SaveData(const std::string &SavePath, const std::vector<AirParGeo> &BigMas);
	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*



	//(+����)
	//������� ��� ���������� ���������� ����������� ��������� ���������
	

        double get_distance_breaking(const Airbill& PZDN, const BillFragm& FR1); // ����������
	double get_distance_descent_from_Hmax (const Airbill& PZDN); //C������� � ������������ ������ �� ������ ��
	double get_distance_descent(const Airbill& PZDN); // �������� � ������ ������� ����� �� ���(� ����������� ���������� �� �����������)

        //������ ��������� ���� ����������� - ��������, � ������� ������ ������� ��������� ���������� �����
        void getForbiddenAreas(double V, double a_centre,
                                 ASh::GeoGraph::GeoCoord point_prev,
                                 ASh::GeoGraph::GeoCoord point,
                                 ASh::GeoGraph::GeoCoord &centre1,
                                 ASh::GeoGraph::GeoCoord &centre2,
                                 double &R);

        //�������� �� ����� � ����������� �������
        bool isInForbiddenAreas(ASh::GeoGraph::GeoCoord &centre1,
                                ASh::GeoGraph::GeoCoord &centre2,
                                double R,
                                const ASh::GeoGraph::GeoCoord &point);
}

#endif // FLIGHT_PLAN_HPP
