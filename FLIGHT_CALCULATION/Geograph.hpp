#pragma once

#ifndef GEOGRAPH_HPP
#define GEOGRAPH_HPP


#define _USE_MATH_DEFINES

//==========================================
#include <math.h>
#include <fstream>
#include <cmath>
#include "AllProc.hpp"

//==========================================

//==========================================










namespace ASh { namespace GeoGraph {

	//++++++ GeoCoord ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	class GeoCoord {
	private:
		double mLa;
		double mFi;
		
	public:
		// ����������� �� ���������
		explicit GeoCoord(const double& _La=0, const double& _Fi=0): mLa(_La), mFi(_Fi) {};
		// ����������� �����������
		GeoCoord(const GeoCoord& right) {
			mLa=right.mLa;
			mFi=right.mFi;
			
		}
		// �������� ������������
		GeoCoord& operator=(const GeoCoord& right) {
			mLa=right.mLa;
			mFi=right.mFi;
			return *this;
		}

		// ������ � ������
		double La() const { return mLa; }
		double Fi() const { return mFi; }
		

		//
		void setFi(const double& Fi_c) {mFi=Fi_c;}
		void setLa(const double& La_c) {mLa=La_c;}
		

		//
		bool operator== (const GeoCoord& P) { return (mLa==P.mLa)&&(mFi==P.mFi); }

		bool operator!= (const GeoCoord& P) { return !(*this==P); }
	};
	//++++++ GeoCoord ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++ LocationCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	class LocationCoord
	{
	private:
		double mDal;
		double mAz;

	public:
		// ����������� �� ���������
		explicit LocationCoord(const double& _Dal=0.0, const double& _Az=0.0): mDal(_Dal), mAz(_Az){};

		// ����������� �����������
		LocationCoord(const LocationCoord& right)
		{
			mDal=right.mDal;
			mAz =right.mAz;
		}

		// �������� ������������
		LocationCoord& operator=(const LocationCoord& right)
		{
			mDal=right.mDal;
			mAz =right.mAz;

			return *this;
		}

		// ������ � ������
		double Dal() const { return mDal; }
		double  Az() const { return mAz; }

		//
		void setDal(const double& Dal_c) {mDal=Dal_c;}
		void setAz(const double& Az_c) {mAz=Az_c;}

		//
		bool operator== (const LocationCoord& P) { return (mDal==P.mDal)&&(mAz==P.mAz); }

		bool operator!= (const LocationCoord& P) { return !(*this==P); }
	};
	//++++++ LocationCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++ DecartCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	class DecartCoord
	{
	private:
		double mX;
		double mY;

	public:
		// ����������� �� ���������
		explicit DecartCoord(const double& _X=0.0, const double& _Y=0.0): mX(_X), mY(_Y){};

		// ����������� �����������
		DecartCoord(const DecartCoord& right)
		{
			mX=right.mX;
			mY=right.mY;
		}

		// �������� ������������
		DecartCoord& operator=(const DecartCoord& right)
		{
			mX=right.mX;
			mY=right.mY;

			return *this;
		}

		// ������ � ������
		double X() const { return mX; }
		double Y() const { return mY; }

		//
		void setX(const double& X_c) {mX=X_c;}
		void setY(const double& Y_c) {mY=Y_c;}

		//
		bool operator== (const DecartCoord& P) { return (mX==P.mX)&&(mY==P.mY); }

		bool operator!= (const DecartCoord& P) { return !(*this==P); }
	};
	//++++++ DecartCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	double GetRz();

	GeoCoord AKY(const GeoCoord& P_FiLa, const LocationCoord& P_DalAz);

	LocationCoord DAHT(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2);

        double CalcDistance(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2);

	double CalcAzimuth(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2);

	GeoCoord XYToFiLa(const DecartCoord& P_XY, const GeoCoord& P_station);

	DecartCoord FiLaToXY(const GeoCoord& P_FiLa, const GeoCoord& P_station);

	GeoCoord GeoGraphToGeoMag(const GeoCoord& P_FiLa);

	GeoCoord GeoMagToGeoGraph(const GeoCoord& P_FiLa);

	//==========================================
}}

#endif //GEOGRAPH_HPP
