#-------------------------------------------------
#
# Project created by QtCreator 2013-02-20T10:13:21
#
#-------------------------------------------------



##### BOOST ####
Boost_ROOT = C:\Libraries\boost_1_47_0\

INCLUDEPATH += $${Boost_ROOT}
DEPENDPATH  += $${Boost_ROOT}

LIBS +=  $${Boost_ROOT}\lib\mingw\libboost_date_time-mgw44-mt-d-1_47.a
##### BOOST ####


QT       += core

QT       -= gui

TARGET = planes_count
CONFIG   += console
CONFIG   -= app_bundle







TEMPLATE = app


SOURCES += \
    flight_plan.cpp \
    main.cpp \
    Geograph.cpp

HEADERS += \
    flight_plan.hpp \
    AMZGO_exchange.hpp \
    AllProc.hpp \
    Geograph.hpp
