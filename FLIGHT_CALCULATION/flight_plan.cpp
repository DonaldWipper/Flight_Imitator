

#include "flight_plan.hpp"


namespace Model_Struct
{
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//��������� �� ����������
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    //����������� �� ���������, �������� �������� �������
    Airbill::Airbill() {
        mode = 0;
        PlaneTTD Boeing;
        Boeing.L_razb = 2310;
        Boeing.V_otr = 68;
        Boeing.V_evol = 90;
        Boeing.V_cruise = 250;
        Boeing.V_climb = 12.5;
        Boeing.V_descend = 12.5;

        Boeing.acc_dog = 0.7;
        Boeing.acc_krug = 2.;
        Boeing.acc_pos = 0.7;
        Boeing.TargetType = std::string("Boeing");
         //---  �������-����������� �������������� �������� ---

        ASh::GeoGraph::GeoCoord Berlin,  Bidgos;


        Berlin.setFi(52. + 31./60. +  4./3600.);
        Berlin.setLa(13. + 22./60. + 47./3600.);
        Bidgos.setFi(53.+7./60.+36./3600.);
        Bidgos.setLa(18.+1./60.+ 3./3600.);

        H_max = 5000; //������������� ������������ ������ �������
        H_vzl = 450;   // ������ ��������� ������
        T_beg = 0;     // ����� ������ ������  -> ������ ������������� ����������� �� 0

        id = std::string("A2");  // ��� ������� ��� ������� �� ������ ���������
        PlanePerfCharact = Boeing; // ���������� c��������� ��������������

        // �������� ������
        Airdrome_vzl = Berlin;
        Airdrome_AzVzl = 180;

        // �������� �������
        Airdrome_pos = Bidgos;
        Airdrome_AzPos = 180;
    }


    //������������� ��� ���� ��� ��������� �������
    void Airbill::set_plane_params(Model_Struct::PlaneTTD plane) {
        PlanePerfCharact = plane;
    }


    //������������� ��������� ��������� �������
    //�������������� ��������� �������, �������� ������, �������� �������, ���������� �����

    void Airbill::set_flight_task(Model_Struct::FlightTask task,
                                  Model_Struct::Airport airTakeoff,
                                  Model_Struct::Airport airLand,
                                  std::vector<Model_Struct::BillPoint> trip) {




        H_max = task.H_max;
        H_vzl = task.H_vzl;
        T_beg = task.T_beg;


        //�������� ������
        Airdrome_vzl = airTakeoff.P_geo;
        Airdrome_AzVzl = airTakeoff.Az;

        // �������� �������
        Airdrome_pos = airLand.P_geo;
        Airdrome_AzPos = airLand.Az;

        MarshTochk.clear();

        for(int i = 0; i < trip.size(); i++) {
            MarshTochk.push_back(trip[i]);
        }
    }






   int prev_type_FR;
   int cur_type_FR;

	
	// (+����) ������� ��� ��������� ������ � ����������� � ���� ��������� � ����������� �� ��� ����
	std::string get_fragment_string(int type_frag) {
		std::string str_frag = "Unknown"; 

		switch (type_frag)
		{
			case 0: break;
			case 1: str_frag =  "������";  return str_frag; break; 
			case 2: str_frag =  "����� ������ � �������� ��";  return str_frag; break;
			case 3: str_frag =  "������ �� ������������ ������";  return str_frag; break;
			case 4: str_frag =  "������ �� ����������� ��������";  return str_frag; break;
			case 5: str_frag =  "�������� ��������";  return str_frag;  break;
			case 6: str_frag =  "�������";  return str_frag;  break;
			case 7: str_frag =  "�������� ��������";  return str_frag;  break;
			case 8: str_frag =  "���������� ��������";  return str_frag;  break;
			case 9: str_frag =  "C�������";  return str_frag;  break;
			case 10: str_frag =  "�������";  return str_frag;  break;
			case 11: str_frag =  "�������� � ������������ ������ �� ������ ��"; return str_frag; break;  
                        default: str_frag = "Empty"; return str_frag;
		}  
		return str_frag;
	}
	
	//(+����) 
	//����� ���������� ������� �������� ������ ��� �������� ��������
	//��������� � �������� ��������� ������� �������
	//C������� �������� V_cruise ->V_evol
        double get_distance_breaking(const Airbill& PZDN, const BillFragm& FR1) {
		//������������ ������������ �������� ����� 0
		//����� ���������� ������������ ���������� ��� ����������, 
		//� ����� ��������� � �������� ���������
		double dT, d;
                dT = std::abs((FR1.V_st - PZDN.PlanePerfCharact.V_evol) / PZDN.PlanePerfCharact.acc_dog);
                d =(FR1.V_st * dT  -  PZDN.PlanePerfCharact.acc_dog * pow(dT,2) / 2.) / 1000.;
		return d;
	}



	//(+����)
	//����� ����������, ������� �������� ������ ��� �������� 
	//������������ ������ -> ������ ������� �����, �������� ������ ���� �� ��� ��������� ������
	//V_evol 
	//
	double get_distance_descent_from_Hmax (const Airbill& PZDN) {
		double dT, d;
		dT = (PZDN.H_max - PZDN.H_vzl) / PZDN.PlanePerfCharact.V_descend; // {c}
                d = PZDN.PlanePerfCharact.V_evol * dT / 1000;
		return d;
	}





	//(+����)
	//����� ����������, ������� �������� ������ ��� ��������
	//������ ������ ����� -> 0, �������� ������ �����������
	//� V_evol �� �������� �������, �� �� � ������ ��� �������� ������, ����, ��������, ��� �����������
        //������, ��� �������, ��� ����������� ������ ���� ������ �� ����
	double get_distance_descent(const Airbill& PZDN) {
		double dT, d;
		dT= PZDN.H_vzl / PZDN.PlanePerfCharact.V_descend;
		double acc = std::abs((PZDN.PlanePerfCharact.V_evol - PZDN.PlanePerfCharact.V_otr) / dT);
		d = (PZDN.PlanePerfCharact.V_evol * dT - acc * pow(dT, 2.) / 2.) / 1000.; //{km}
		return d;
	}


        double get_margin(ASh::GeoGraph::GeoCoord geo, const Airbill& PZDN, const BillFragm& FR1) {
            double z;
            double R = (FR1.V_st * FR1.V_st) / (PZDN.PlanePerfCharact.acc_krug * 1000.);
            double angle;
              ASh::GeoGraph::LocationCoord local = DAHT(FR1.St_Point, geo);
            if((local.Az() >= FR1.Az_st) && (local.Az() <= FR1.Az_st + 180.0)) {
               angle = 90;

           } else {
               angle = -90;

           }

            ASh::GeoGraph::GeoCoord point_centre = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(R, FR1.Az_st + angle));
            double D = CalcDistance(point_centre, geo);
            local = DAHT(geo, point_centre);
            double alpha = (local.Az() - FR1.Az_st) * M_PI / 180.0;
            z = D * cos(alpha) + sqrt(  pow(D * cos(alpha), 2)
                                        -  ( pow(D, 2) - pow(R ,2 ) )  );


            point_centre = ASh::GeoGraph::AKY(point_centre, ASh::GeoGraph::LocationCoord(z, FR1.Az_st + 180));
            qDebug() << CalcDistance(point_centre, geo) << " " << R << " " << "������ ���� �����";

            return z;
        }



          void getForbiddenAreas(double V, double a_centre,
                                 ASh::GeoGraph::GeoCoord point_prev,
                                 ASh::GeoGraph::GeoCoord point,
                                 ASh::GeoGraph::GeoCoord &centre1,
                                 ASh::GeoGraph::GeoCoord &centre2,
                                 double &R) {

            double Az = DAHT(point_prev, point).Az();
            R = pow(V, 2) / (a_centre * 1000);
            centre1 = ASh::GeoGraph::AKY(point, ASh::GeoGraph::LocationCoord(R, Az + 90.0));
            centre2 = ASh::GeoGraph::AKY(point, ASh::GeoGraph::LocationCoord(R, Az - 90.0));


        }




        bool is_inside(ASh::GeoGraph::GeoCoord geo, Airbill& PZDN, const BillFragm& FR1) {

            double angle;
              ASh::GeoGraph::LocationCoord local = DAHT(FR1.St_Point, geo);
            if((local.Az() >= FR1.Az_st) && (local.Az() <= FR1.Az_st + 180.0)) {
               angle = 90;
            } else {
               angle = -90;
            }

            double R = (FR1.V_st * FR1.V_st) / (PZDN.PlanePerfCharact.acc_krug * 1000.);
           PZDN.R = R;
           ASh::GeoGraph::GeoCoord point_centre = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(R, FR1.Az_st + angle));
           ASh::GeoGraph::GeoCoord point_centre2 = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(R, FR1.Az_st - angle));
           PZDN.centre1 = point_centre;
           PZDN.centre2 = point_centre2;

           double D = CalcDistance(point_centre, geo);
           qDebug() << D << " " << R << "��� ��� �������";

            if(D < R) {
                return true;
            } else {
                return false;
            }
        }


        


	//========================================================================
	//                  ��������� �������� ������ �� ���
	//========================================================================
	void Fill_VPPRazgonFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

		FR1.T_st = PZDN.T_beg;
		FR1.V_st = 0;
		FR1.Az_st = PZDN.Airdrome_AzVzl;
		FR1.St_Point = PZDN.Airdrome_vzl;
		FR1.V_end = PZDN.PlanePerfCharact.V_otr;
		FR1.Az_end = PZDN.Airdrome_AzVzl;

		dT = 2. * PZDN.PlanePerfCharact.L_razb / PZDN.PlanePerfCharact.V_otr;

		FR1.T_end = FR1.T_st+dT;

		// ����������
		D = PZDN.PlanePerfCharact.L_razb / 1000.; // ��� ��� L_razb � � ��������� � ��

		X = D * sin(FR1.Az_st * M_PI/180);
		Y = D * cos(FR1.Az_st * M_PI/180);

		FR1.End_Point = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y), FR1.St_Point);

		FR1.H_st = 0.0;
                FR1.H_end = 0.0;

		FR1.FragmType = 1;
	}

	
	//========================================================================
	//                  ��������� �������� �����
	//========================================================================
	void Fill_TakeoffFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

                FR1.V_end = PZDN.PlanePerfCharact.V_evol; //{m/c}

		dT=PZDN.H_vzl/PZDN.PlanePerfCharact.V_climb;

		double acc=(FR1.V_end-FR1.V_st)/dT;

		D=(FR1.V_st*dT+acc*pow(dT,2.)/2.)/1000.; //{km}

		// ����������
		X=D*sin(FR1.Az_st*M_PI/180);
		Y=D*cos(FR1.Az_st*M_PI/180);

		FR1.End_Point=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);

		FR1.T_end=FR1.T_st+dT;
		FR1.Az_end=FR1.Az_st;

		// ������ ������ � ����� �����
		FR1.H_st = 0.0;
                FR1.H_end = PZDN.H_vzl;

		FR1.FragmType=2;
	}


	
	//========================================================================
	//                  ��������� �������� ������ �� ������������ ������
	//========================================================================
	void Fill_UpMaxHighFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

		//�������� �� ����������� �� ��������
		FR1.V_end=PZDN.PlanePerfCharact.V_evol;

		//������� ����� ������� �� ������������ ������ �� ����� ������� �����
		dT = (PZDN.H_max - PZDN.H_vzl) / PZDN.PlanePerfCharact.V_climb; // {c}
		
		//���� �� ����������� ����� ����� ��������� � ���������� ���������
		D = PZDN.PlanePerfCharact.V_evol * dT / 1000;
		

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		FR1.End_Point = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);

		FR1.T_end = FR1.T_st+dT;
		FR1.Az_end = FR1.Az_st;

		// ������ ������ � ����� �����
		FR1.H_st = PZDN.H_vzl;
                FR1.H_end = PZDN.H_max;

		FR1.FragmType = 3;
	}


        //================================================================================================
        //              �������� ������� ��� ���������� � ������������ ���������� �� ���������� ��������
        //================================================================================================
        void Fill_FixAccelerationFragm(const Airbill& PZDN, BillFragm &FR1)
        {
            float dT, D;
            ASh::GeoGraph::LocationCoord P_cur;

            //C������� �� ��������� ���������� ��������� ���������, ���� ����� ��� ������� ����� ��������



            // ������ �� V_st �� V_cust
            //FR1.V_end = PZDN.PlanePerfCharact.V_cust; //{�/���}


            //�������� �����, �� ������� �� ��������� ������ ��������
            dT = std::abs((FR1.V_end - FR1.V_st) / PZDN.PlanePerfCharact.acc_cust);


            double a_temp;
            //���������� ��������� � ��� ��� ����������
            if(FR1.V_end < FR1.V_st) {
                a_temp = -PZDN.PlanePerfCharact.acc_cust;
            } else {
                a_temp = PZDN.PlanePerfCharact.acc_cust;
            }


            FR1.TMT1 = a_temp;

            //��������� ���������, ������� �� ������� �� ��� �����
            D = (FR1.V_st * dT + a_temp * pow(dT,2) / 2.) / 1000.;
            FR1.End_Point = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D, FR1.Az_st));

            FR1.T_end = FR1.T_st + dT;


            P_cur = ASh::GeoGraph::DAHT(FR1.End_Point, FR1.St_Point);  // �������� ������
            FR1.Az_end = P_cur.Az() + 180;       // ������ ������

            if (FR1.Az_end > 360)
                    FR1.Az_end -= 360;

            if (FR1.Az_end < 0)
                    FR1.Az_end += 360;

            //�� ���� ��������� ������ �� ��������
            FR1.H_end = FR1.H_st;

            FR1.FragmType = 12;
        }





	//========================================================================
	//                  ��������� �������� ������ �� ����������� ��������
	//========================================================================
	void Fill_AccelerationFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		float dT, D;
		ASh::GeoGraph::LocationCoord P_cur;

		// ������ �� V_st �� ����������� ��������
		FR1.V_end = PZDN.PlanePerfCharact.V_cruise; //{�/���}

		dT = std::abs((FR1.V_end - FR1.V_st) / PZDN.PlanePerfCharact.acc_dog);


				

		FR1.T_end = FR1.T_st + dT;

		D = (FR1.V_st * dT + PZDN.PlanePerfCharact.acc_dog * pow(dT,2) / 2.) / 1000.;

		FR1.End_Point = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));

		P_cur = ASh::GeoGraph::DAHT(FR1.End_Point, FR1.St_Point);  // �������� ������

		FR1.Az_end = P_cur.Az() + 180;       // ������ ������

		if (FR1.Az_end > 360)
			FR1.Az_end -= 360;

		if (FR1.Az_end < 0)
			FR1.Az_end += 360;

		//�� ���� ��������� ������ �� ��������
		FR1.H_end = FR1.H_st;

		FR1.FragmType = 4;
	}

	
	


	//========================================================================
	//             ��������� �������� �������������� ��������
	//========================================================================
	void Fill_LinearMotionFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT;
		ASh::GeoGraph::LocationCoord P_geo;

		// ��� ����������� ������� ������ �� ���� ������� ���� ���������� ���������� ����� ����� ������� ���������
                P_geo = ASh::GeoGraph::DAHT(FR1.St_Point, FR1.End_Point);
                FR1.Az_st = P_geo.Az();

		// ����� ������ �� ���� �������
                dT = (P_geo.Dal() / FR1.V_st) * 1000.;
                qDebug()  << dT << " ����� �������";
                FR1.T_end = FR1.T_st+dT;

		//����� ���������� �� ����� � ��� �� ������
		FR1.H_end = FR1.H_st;
		
		P_geo=ASh::GeoGraph::DAHT(FR1.End_Point, FR1.St_Point);
                FR1.Az_end=P_geo.Az() + 180;

                if (FR1.Az_end > 360)
			FR1.Az_end-=360;

		if (FR1.Az_end<0)
			FR1.Az_end+=360;

                qDebug() << "� ��� ������ � ������� �� �� ����� ���� ������ " << FR1.Az_end;
		// �������� ���������
                FR1.V_end = FR1.V_st ;

		FR1.FragmType = 5;
	}



	//========================================================================
	//                 ��������� �������� �������
	//========================================================================
	void Fill_RotationFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D, Rr, Rpov, Ugg, dX, dY;

		FR1.V_end=FR1.V_st;

		Ugg=FR1.Ug_pov;

		if (Ugg<-180)
			Ugg+=360;
		else if (Ugg>180)
			Ugg-=360;


		Rpov=pow(FR1.V_st,2.)/(PZDN.PlanePerfCharact.acc_krug*1000.);    // ������ ���������� ������� ��������� ������� ��� ����������� �������� �� ����������
		// �� ��������� FR1.V_st � ������������������� ���������� Sam.acc_krug

		FR1.Az_end=(FR1.Az_st+Ugg);

		FR1.H_end = FR1.H_st;


		if (FR1.Az_end>360)
			FR1.Az_end-=360;

		if (FR1.Az_end<0)
			FR1.Az_end+=360;


		Rr=2*Rpov*std::abs(sin(((Ugg/2.)*M_PI/180)));  // ������ ����� �����

		// ����������
		dX=Rr*sin((FR1.Az_st+Ugg/2.)*M_PI/180);
		dY=Rr*cos((FR1.Az_st+Ugg/2.)*M_PI/180);

		FR1.End_Point=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(dX,dY),FR1.St_Point); // ����� ���������� ���������� ����� ��� �� ������ ���� ��� � ������� ������������

		FR1.Ug_pov=Ugg;

		D=std::abs(Ugg*Rpov*M_PI/180);   // ����� ���� � �� - ���������� ������� �������� �������
		dT = (D / FR1.V_st) * 1000.;           // ����� � c�� ����������� ��� ������� D

		FR1.T_end = FR1.T_st + dT;

		FR1.FragmType = 6;

		// ���������� � ������������� �������� ���������
		FR1.TMT1 = PZDN.PlanePerfCharact.acc_krug;
	}


	//========================================================================
	//                  ��������� �������� ����
	//========================================================================
	void Fill_CirclingFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		int n_kr;
		double dT, D, Rpov;

		//Rpov=PZDN.MarshTochk[PZDN.Cur_MT].R_rot; // <- ������ �������� �������� ������������ ��� �������� ��� ������ ������� ��� � ����� ������

		Rpov=pow(FR1.V_st,2.)/(PZDN.PlanePerfCharact.acc_krug*1000.);    // ������ ���������� ������� ��������� ������� ��� ����������� �������� �� ����������
		// �� ��������� FR1.V_st � ������������������� ���������� Sam.acc_krug

		n_kr=PZDN.MarshTochk[PZDN.Cur_MT].N_rot; // <- ����� ������

		D=2*M_PI*Rpov*n_kr;                      // ��
		dT=(D/FR1.V_st)*1000.;


		
		//����� ���������� �� ����� � ��� �� ������
		FR1.H_end = FR1.H_st;


		FR1.T_end=FR1.T_st+dT;

		FR1.Az_end=FR1.Az_st;

		FR1.End_Point=FR1.St_Point;

		FR1.V_end=FR1.V_st;

		FR1.FragmType = 7;

		FR1.TMT1=Rpov;
		FR1.TMT2=n_kr;
	}

		

	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	//========================================================================
	//                 ��������� �������� ����������
	//========================================================================
	 
	void Fill_BreakingFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		float dT, D;
		ASh::GeoGraph::LocationCoord P_rl;

		// ���������� c ����������� �� ����������� ��������
		FR1.V_end = PZDN.PlanePerfCharact.V_evol; //{m/c}

		// ����������� ����� � ������ ���������
		dT=std::abs((FR1.V_st-FR1.V_end)/PZDN.PlanePerfCharact.acc_dog);

                FR1.H_end = FR1.H_st;

		FR1.T_end = FR1.T_st+dT;

		// ���������� ����������
		D=(FR1.V_st * dT - PZDN.PlanePerfCharact.acc_dog * pow(dT, 2) / 2.) / 1000.; // ���� - ��� ��� ��������� ����� ���� +

		P_rl.setDal(D);
		P_rl.setAz(FR1.Az_st);

		FR1.End_Point=ASh::GeoGraph::AKY(FR1.St_Point, P_rl);

		FR1.Az_end=FR1.Az_st;  // ������� ��� ������ �� ������ ��� ����������

		FR1.FragmType = 8;
	}



	//(+����)
	void Fill_DescentFromMaxFragm(const Airbill& PZDN, BillFragm &FR1) {
        double dT, D;
		double X, Y;


		//�������� �� ����������� �� ��������
		FR1.V_end = PZDN.PlanePerfCharact.V_evol;

		//������� ����� �������� � ������������ ������ �� ������ ������� �����
		dT = (PZDN.H_max - PZDN.H_vzl) / PZDN.PlanePerfCharact.V_descend; // {c}
		
		//���� �� ����������� ����� ����� ��������� � ���������� ���������
		D = PZDN.PlanePerfCharact.V_evol * dT / 1000;
		

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		FR1.End_Point = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);

		FR1.T_end = FR1.T_st  + dT;
		FR1.Az_end = FR1.Az_st;

		// ������ ������ � ����� �����
		FR1.H_st = PZDN.H_max;
                FR1.H_end = PZDN.H_vzl;

		FR1.FragmType = 11;
	}
 



		

	//========================================================================
	//                ��������� �������� ��������
	//========================================================================
	//(change ����)
	//������ �� ����������� �����
	void Fill_DescentFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

		FR1.V_st = PZDN.PlanePerfCharact.V_evol; 
		FR1.V_end = PZDN.PlanePerfCharact.V_otr;            // �.�. ���������� �� �������� ������

		FR1.Az_end = PZDN.Airdrome_AzPos;

		//����� ��������
		dT =  PZDN.H_vzl / PZDN.PlanePerfCharact.V_descend;

		double acc = std::abs((FR1.V_st  - FR1.V_end) / dT);
 		
		// ����� ��������
		//dT = std::abs((FR1.V_end - FR1.V_st) / PZDN.PlanePerfCharact.acc_pos);

		FR1.T_end = FR1.T_st + dT;

		D = (FR1.V_st * dT  - acc * pow(dT, 2.) / 2.) / 1000.; //{km}

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		//�� �������� ���������� ������

		
                FR1.H_end = 0.0;
		FR1.End_Point=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);

		FR1.FragmType = 9;
	}

	/*void Fill_DescentFragm(const Airbill& PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

		FR1.V_end=PZDN.PlanePerfCharact.V_otr;            // �.�. ���������� �� �������� ������

		FR1.Az_end=PZDN.Airdrome_AzPos;

		// ����� ��������
		dT=std::abs((FR1.V_end - FR1.V_st) / PZDN.PlanePerfCharact.acc_pos);
		FR1.T_end=FR1.T_st + dT;

		D=(FR1.V_st*dT - PZDN.PlanePerfCharact.acc_pos*pow(dT, 2.)/2.)/1000.; //{km}

		// ����������
		X=D*sin(FR1.Az_st*M_PI/180);
		Y=D*cos(FR1.Az_st*M_PI/180);

		FR1.End_Point=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);

		FR1.FragmType = 9;
	}*/


	//========================================================================
	//                ��������� �������� �������� �������� �� �����
	//========================================================================
        void Fill_VPPLandingFragm(Airbill &PZDN, BillFragm &FR1)
	{
		double dT, D;
		double X, Y;

		dT= 2. * PZDN.PlanePerfCharact.L_razb / PZDN.PlanePerfCharact.V_otr;

		FR1.T_end = FR1.T_st + dT;
                PZDN.T_end = FR1.T_end;

		FR1.H_st = FR1.H_end = 0.0;

		FR1.V_end = 0;                     // ���������

		FR1.Az_end = FR1.Az_st;

		D = PZDN.PlanePerfCharact.L_razb;  // ���������� ���������� �� T

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		FR1.End_Point = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y), FR1.St_Point);

		FR1.FragmType = 10;
	}


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//����� ���������� ����������
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@











	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� �����
	//========================================================================
	void GetPar_Takeoff(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, D;
		double X, Y;

		double acc = (FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st);

                Air.Accel = acc;                           // ���������
                dT = Ttek - FR1.T_st;                        // ����� �� ������ ��������� �� �������� �������
		Air.Speed = FR1.V_st + acc * dT;               // ��������

		D=(FR1.V_st*dT+acc*pow(dT,2.)/2.)/1000.; // ���������� ���������� �� T

		Air.type_FR = FR1.FragmType;

		// ����������
		X = D * sin(FR1.Az_st * M_PI/180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		Air.PlaneC = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y), FR1.St_Point);
		//������� ������ �������
		Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);

		Air.Course = FR1.Az_st;       // ����
	}



   
	 //========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ���������  ������ �� ���
	//========================================================================
	void GetPar_VPPRazgon(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, D;
		double X, Y;

		double acc = (FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st);

		Air.Accel = acc;                // ���������



		dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������

		Air.Speed = acc * dT;             // ��������

		Air.type_FR = FR1.FragmType;

		D = acc * pow(dT, 2.) / (2 * 1000.);   // ���������� ���������� �� T

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

		Air.PlaneC = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y), FR1.St_Point);
		//������� ��������� �� �����, �.� ��� ������ ����� 0
		Air.Altitude = 0.0;

		Air.Course=FR1.Az_st;         // ����
	}

	
	
	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� ������
	//========================================================================
	void  GetPar_UpMaxHigh(float Ttek, BillFragm FR1, AirParGeo &Air)
	{
		float dT, D;

		ASh::GeoGraph::GeoCoord P_geo;
		ASh::GeoGraph::LocationCoord P_loc;

		
		dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������
		Air.Speed = FR1.V_st;             // ��������
                Air.Accel = 0;                    // ��������� ��� ������ ������������ ������

		D = FR1.V_st * dT / 1000.;  // ���������� ���������� �� dT


		Air.type_FR = FR1.FragmType;

		// �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
		P_geo = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));
		Air.PlaneC = P_geo; 

		// ������� ������ �� ������� �� ���������
		
		Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);
		
		// �������� ������ �� ����� � ������� ��������� ������� �� �������� ����� ���������
		P_loc = ASh::GeoGraph::DAHT(Air.PlaneC, FR1.End_Point);
		Air.Course=P_loc.Az();          // ����
	}



	

	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� ������
	//========================================================================
	void GetPar_Acceleration(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		float dT, D;

		ASh::GeoGraph::GeoCoord P_geo;
		ASh::GeoGraph::LocationCoord P_loc;

		double acc = (FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st);
		
		Air.Accel = acc;                // ���������

		dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������
		Air.Speed=FR1.V_st+acc*dT;    // ��������

		D=(FR1.V_st * dT+acc * pow(dT,2)/2.)/1000.;  // ���������� ���������� �� dT
		Air.type_FR = FR1.FragmType;
		
		// �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
		P_geo=ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));
		Air.PlaneC = P_geo; 

		// ������� ������ �� ������� �� ���������

		Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);

		// �������� ������ �� ����� � ������� ��������� ������� �� �������� ����� ���������
		P_loc=ASh::GeoGraph::DAHT(Air.PlaneC, FR1.End_Point);
		Air.Course=P_loc.Az();          // ����
	}


	

	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� ����������
	//========================================================================
	void GetPar_Breaking(const double &Ttek, const BillFragm &FR1, AirParGeo &Air)
	{
		float dT, D;
		ASh::GeoGraph::LocationCoord P_rl;
		ASh::GeoGraph::GeoCoord P_geo;
                double acc=(FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st);
		Air.Accel=acc;                          // ���������

		dT=Ttek-FR1.T_st;                       // ����� �� ������ ��������� �� �������� �������
		Air.Speed=FR1.V_st+acc*dT;              // �������� -> ���� + ��� ��� ��������� ����� ���� -

		D = (FR1.V_st * dT+acc * pow(dT,2)/2.)/1000.;// ���������� ���������� �� dT
		 
		P_rl.setDal(D);
		P_rl.setAz(FR1.Az_st);
		Air.type_FR = FR1.FragmType;
		
		
		// �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
		Air.PlaneC=ASh::GeoGraph::AKY(FR1.St_Point, P_rl);
        P_geo=ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));

		Air.PlaneC=P_geo;
		Air.Altitude = FR1.H_st;

		Air.Course=FR1.Az_st;                   // ����
	}


        //========================================================================
        // ��������� ��� ���������� �� ���������
        //========================================================================
        void GetPar_FixAcceleration(const double &Ttek, const BillFragm &FR1, AirParGeo &Air) {
            float dT, D;
            double acc;

            ASh::GeoGraph::GeoCoord P_geo;
            ASh::GeoGraph::LocationCoord P_loc;


            Air.Accel = FR1.TMT1;                // ���������

            acc = Air.Accel;
            dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������
            Air.Speed = FR1.V_st + acc * dT;    // ��������

            D = (FR1.V_st * dT+acc * pow(dT,2)/2.)/1000.;  // ���������� ���������� �� dT
            Air.type_FR = FR1.FragmType;

            // �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
            P_geo=ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));
            Air.PlaneC = P_geo;

            // ������� ������ �� ������� �� ���������

            Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);

            // �������� ������ �� ����� � ������� ��������� ������� �� �������� ����� ���������
            P_loc=ASh::GeoGraph::DAHT(Air.PlaneC, FR1.End_Point);
            Air.Course=P_loc.Az();          // ����

        }
	
	
	

	

	





	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� �������������� ��������
	//========================================================================
	void GetPar_LinearMotion(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, D;


		ASh::GeoGraph::GeoCoord P_geo;
		ASh::GeoGraph::LocationCoord P_loc;

                dT = Ttek - FR1.T_st;          // ����� �� ������ ��������� �� �������� �������
                D = FR1.V_end * dT / 1000.;       // ����� �� ������ ��������� �� �������� �������

		// �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
		P_geo=ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D,FR1.Az_st));
		Air.PlaneC=P_geo;
		Air.Altitude = FR1.H_st;
		// �������� ������ �� ����� � ������� ��������� ������� �� �������� ����� ���������
		P_loc=ASh::GeoGraph::DAHT(Air.PlaneC, FR1.End_Point);
		Air.Course=P_loc.Az();      // ����11
		Air.type_FR = FR1.FragmType;
		Air.Speed=FR1.V_end;      // ��������
		Air.Accel=0;              // ���������
	}

	
	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� �������
	//========================================================================
	void GetPar_Rotation(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, Ugg, Rpov, Rr, dX, dY;
		int znak=0;
		ASh::GeoGraph::GeoCoord P_geo;
		double acc=FR1.TMT1;
		
		Air.Accel=acc;                               // ���������
		 
		dT=Ttek-FR1.T_st;                            // ����� �� ������ ��������� �� �������� �������
		Rpov=pow(FR1.V_st,2.)/(acc*1000.);           // ������ ��������

		//znak=ASh::Proc::ZnR(FR1.Ug_pov);
		znak=ASh::Proc::GetSign(FR1.Ug_pov); // �� �������� ���

		if ((FR1.Ug_pov>180) || (FR1.Ug_pov<-180))
			Ugg=-znak*FR1.V_st*dT/(Rpov*1000.);        // � ��������
		else
			Ugg= znak*FR1.V_st*dT/(Rpov*1000.);

		Ugg *= 180.0 / M_PI;  // ��������� � �������

		
		

		// ����������
		Rr=2*Rpov*std::abs(sin((Ugg/2.)*M_PI/180));

		dX=Rr*sin((FR1.Az_st+Ugg/2.)*M_PI/180);
		dY=Rr*cos((FR1.Az_st+Ugg/2.)*M_PI/180);

		Air.PlaneC=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(dX,dY), FR1.St_Point);



		//������� �� ����� ������ ������ ��������� �������
	    Air.type_FR = FR1.FragmType;

		//Air.PlaneC=P_geo;
		Air.Altitude = FR1.H_st;

		
		Air.Speed=FR1.V_st;                          // ��������

		Air.Course=FR1.Az_st+Ugg;                    // ����

		if (Air.Course>360)
			Air.Course-=360;
		if (Air.Course<0)
			Air.Course+=360;
	}

	

	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� ����
	//========================================================================
	void GetPar_Circling(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		int znak=1;
		float dT, Ugg, Rpov, Rr, dX, dY;

		dT=Ttek-FR1.T_st;               // ����� �� ������ ��������� �� �������� �������

		Rpov=FR1.TMT1;                  // ������ ��������
		Ugg=FR1.V_st*dT/(Rpov*1000.);   // � ��������

		Ugg*=180.0/M_PI;  // ��������� � �������

		Rr=2*Rpov*std::abs(sin((Ugg/2.)*M_PI/180));

                int n = floor(Ugg / 360);
                if (Ugg>360) {
                    Ugg -= 360 * n;
                }
                 //      znak=-1;

		dX=Rr*sin((FR1.Az_st+Ugg/2.)*M_PI/180);
		dY=znak*Rr*cos((FR1.Az_st+Ugg/2.)*M_PI/180);

		// ����������
		Air.PlaneC=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(dX,dY),FR1.St_Point);
		Air.Altitude = FR1.H_st;
		
		
		Air.Speed=FR1.V_st;             // ��������

		Air.Course=FR1.Az_st+Ugg;       // ����
		Air.type_FR = FR1.FragmType;
		if (Air.Course>360)
			Air.Course-=360;
		if (Air.Course<0)
			Air.Course+=360;
	}


	// ������������ ��������� � 
        //�������� �� ������������ ������, ��� � �������� �������

	void GetPar_DescentFromMax(const double& Ttek, const BillFragm& FR1, AirParGeo &Air) {
        double dT, D;

		
		ASh::GeoGraph::GeoCoord P_geo;
		ASh::GeoGraph::LocationCoord P_loc;

		
		dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������
		Air.Speed = FR1.V_st;             // ��������

		D = FR1.V_st * dT / 1000.;  // ���������� ���������� �� dT


		Air.type_FR = FR1.FragmType;

		// �������� ���������� ����� � ������� ��������� ������� �� ������� � ���������� �� ������ ���������
		P_geo = ASh::GeoGraph::AKY(FR1.St_Point, ASh::GeoGraph::LocationCoord(D, FR1.Az_st));
		Air.PlaneC = P_geo; 

		// ������� ������ �� ������� �� ���������
		
		Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);
		
		P_loc=ASh::GeoGraph::DAHT(Air.PlaneC, FR1.End_Point);
		Air.Course=P_loc.Az();          // ����
                Air.Accel = 0;
	} 

	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � ��������� ��������
	//========================================================================
	void GetPar_Descent(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, D;
		double X, Y;

		double acc = std::abs((FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st));

		Air.Accel = acc;                  // ���������

		dT = Ttek - FR1.T_st;               // ����� �� ������ ��������� �� �������� �������
		Air.Speed = FR1.V_st - acc * dT;      // ��������

		D = (FR1.V_st * dT - acc * pow(dT, 2.) / 2.) / 1000.; // ���������� ���������� �� T

		// ����������
		X = D*sin(FR1.Az_st*M_PI/180);
		Y = D*cos(FR1.Az_st*M_PI/180);
		Air.type_FR = FR1.FragmType;
		Air.PlaneC=ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);
		Air.Altitude = FR1.H_st + (FR1.H_end - FR1.H_st) * dT / (FR1.T_end - FR1.T_st);
		Air.Course=FR1.Az_st;           // ����
	}

	

	//========================================================================
	// ��������� �������� ���������, ��������� �.�.� �� �������� � �������� �������� �� �����
	//========================================================================
	void GetPar_VPPLanding(const double& Ttek, const BillFragm& FR1, AirParGeo &Air)
	{
		double dT, D;
		double X, Y;

		double acc = (FR1.V_end - FR1.V_st) / (FR1.T_end - FR1.T_st);

		Air.Accel = acc;                // ���������

		dT = Ttek - FR1.T_st;             // ����� �� ������ ��������� �� �������� �������

                Air.Speed = FR1.V_st+acc * dT;    // ��������
		Air.type_FR = FR1.FragmType;

		D = (FR1.V_st * dT + acc * pow(dT, 2.) / 2) / 1000.; // ����� �� ������ ��������� �� �������� �������

		// ����������
		X = D * sin(FR1.Az_st * M_PI / 180);
		Y = D * cos(FR1.Az_st * M_PI / 180);

                Air.PlaneC = ASh::GeoGraph::XYToFiLa(ASh::GeoGraph::DecartCoord(X,Y),FR1.St_Point);
		Air.Altitude = 0.0;
                Air.Course = FR1.Az_st;         // ����
	}





        /**********************************************************************
         *        ��� ����������� ������ ����������� ��������                 *
         **********************************************************************/

        void  MakingRotation2(BillFragm &FR1, Airbill &PZDN, const ASh::GeoGraph::GeoCoord &PP) {
            //��������� ���������� ������



            ASh::GeoGraph::LocationCoord  P_rl = ASh::GeoGraph::DAHT
                 (FR1.St_Point,PP);

            FR1.Rast_pov = P_rl.Dal();
            double delAz=P_rl.Az()-FR1.Az_st;
            double angle;

            if(delAz > 0)
                angle = 90;
            else
                angle = -90;


            double R = pow(FR1.V_st, 2.) / (PZDN.PlanePerfCharact.acc_krug * 1000.);


            ASh::GeoGraph::GeoCoord P_c = ASh::GeoGraph::AKY(FR1.St_Point,
                                                             ASh::GeoGraph::LocationCoord(R, FR1.Az_st + angle));


            ASh::GeoGraph::LocationCoord  P_rl2 = ASh::GeoGraph::DAHT
                 (P_c,PP);
            double D = CalcDistance(P_c, PP);

            double betta = asin(R / D);
            qDebug() << "���� �������� " << betta;

            delAz = betta * 180.0 / M_PI + P_rl2.Az() - FR1.Az_st;

            FR1.Ug_pov=delAz;
            Fill_RotationFragm(PZDN, FR1);
            qDebug() << "C ���� �������� �� ����� �� �������� " << FR1.Az_end;
            PZDN.MarshFragm.push_back(FR1); // ������
            FR1.shift();



        }












	//========================================================================
	// ������� ���������� ���� ����� ������� � ������ ��������� � ������ PP
	//            � ��� ������������� �������� ��������� ���
	//========================================================================
        void MakingRotation(BillFragm &FR1, Airbill &PZDN, const ASh::GeoGraph::GeoCoord &PP)
	{
		ASh::GeoGraph::LocationCoord P_rl;

                double delAz=0;
                int i = 0;

                // ���� ������ �.�. ����� ������� ���������������� �������� ������ 0, �� ������ �� ����� ��� ��������� �.�. �������� �� �������� ����� ������ ���������
		// � ������������� � ��� ������ � ���� ����� ��������� ��������.
		do
		{
                        i++;
                        P_rl=ASh::GeoGraph::DAHT
                             (FR1.St_Point,PP);

			FR1.Rast_pov=P_rl.Dal();

			// �������� ������������� ��������
			delAz=P_rl.Az()-FR1.Az_st;


                        if (std::abs(delAz)> 0.001)
			{
				FR1.Ug_pov=delAz;
				Fill_RotationFragm(PZDN,FR1);

				PZDN.MarshFragm.push_back(FR1); // ������
				FR1.shift();                    // ���������� �������� ������ ��� ����������
			}
			else
				break;
		}
		while(1);
	}

	//========================================================================
	//              ��������� ������ ���������� ��� ��������� �������
	//========================================================================
	void FillBillFragments(Airbill &PZDN)
	{
		BillFragm FR1;

		ASh::GeoGraph::LocationCoord P_rl;



		//**********************  ������ �� ��� � �����  ********************
		// ������ �� ���
                if(PZDN.mode == 0) { //���� �� � ������, ��� �� �������
                    Fill_VPPRazgonFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1);     // ������
                    FR1.shift();                        // ���������� �������� ������ ��� ����������

                    // ����� � ����������� �������� �� �������� �������
                    Fill_TakeoffFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1);     // ������
                    FR1.shift();                        // ���������� �������� ������ ��� ����������
                    //**********************  ������ �� ��� � �����  ********************
               }


		// ���� ������ ����� ������ � ����� ������� �.�. ������ ���������� ����� ����
		if (PZDN.MarshTochk.empty())
		{
                        if(PZDN.mode == 0) {
                            //---
                            // ���������� ����� �� �� �������� ������� ������� � ������ ��� �������������
                            MakingRotation(FR1, PZDN, PZDN.Airdrome_pos);
			
                            //����������� �� ������������ ������
                            Fill_UpMaxHighFragm(PZDN, FR1);
                            PZDN.MarshFragm.push_back(FR1); // ������
                            FR1.shift();

                            // ������ �� ����������� ��������
                            Fill_AccelerationFragm(PZDN,FR1);
                            PZDN.MarshFragm.push_back(FR1); // ������
                            FR1.shift();                    // ���������� �������� ������ ��� ����������
                        } else {
                            FR1.St_Point = PZDN.Airdrome_vzl;
                            FR1.V_st = PZDN.PlanePerfCharact.V_cruise;
                            FR1.T_st = PZDN.T_beg;
                        }
		} // 
		else
		{
                        if(PZDN.mode == 0) {
                            //---
                            // ���������� ����� �� ������� �� ������ ����� �������� � ������ ��� �������������
                            MakingRotation(FR1, PZDN, PZDN.MarshTochk[0].P_geo);
                            //---

                            //����������� �� ������������ ������
                            Fill_UpMaxHighFragm(PZDN, FR1);
                            PZDN.MarshFragm.push_back(FR1); // ������
                            FR1.shift();


                            // ������ �� ����������� ��������
                            Fill_AccelerationFragm(PZDN,FR1);
                            PZDN.MarshFragm.push_back(FR1); // ������
                            FR1.shift();                    // ���������� �������� ������ ��� ����������
                        } else {
                            FR1.St_Point = PZDN.Airdrome_vzl;
                            FR1.V_st = PZDN.PlanePerfCharact.V_cruise;
                            FR1.T_st = PZDN.T_beg;
                            FR1.H_st = PZDN.H_max;
                        }

			//******************  �������� �� ���������� ������  ****************
			std::vector<BillPoint>::iterator mthk;

                        for (mthk=PZDN.MarshTochk.begin(); mthk!=PZDN.MarshTochk.end(); ++mthk)
			{
                                PZDN.Cur_MT = int(std::distance(PZDN.MarshTochk.begin(),mthk));

				switch ((*mthk).PointType)
				{
				case 4:  // ������������� ��������
					{

                                               if( ((*(mthk-1)).isNeedAcc == true) && (mthk != PZDN.MarshTochk.begin())){
                                                   FR1.V_end = (*(mthk-1)).V_end;
                                                   if(FR1.V_st != FR1.V_end) {   //���� ����� ��������� ��� �����������
                                                       Fill_FixAccelerationFragm(PZDN, FR1);
                                                       PZDN.MarshFragm.push_back(FR1); // ������
                                                       FR1.shift();
                                                   }
                                               }

                                               if (is_inside((*(mthk)).P_geo, PZDN,  FR1)) {
                                                   qDebug() << get_margin((*(mthk)).P_geo, PZDN,  FR1);

                                               }

                                                //---
                                                // ���������� ����� �� ������� �� ����� �������� � ������ ��� �������������
                                                if(PZDN.mode == 1) {
                                                    if(mthk != PZDN.MarshTochk.begin()) {
                                                      MakingRotation(FR1, PZDN, (*mthk).P_geo);
                                                    }
                                                } else {
                                                   MakingRotation(FR1, PZDN, (*mthk).P_geo);
                                                }
                                                //---

                                                // � ��������� Fill_LinearMotionFragm ���� ������� ��������� ���������� �������� �����
                                                FR1.End_Point=(*mthk).P_geo;


                                                Fill_LinearMotionFragm(PZDN,FR1);
                                                PZDN.MarshFragm.push_back(FR1); // ������
                                                FR1.shift();                    // ���������� �������� ������ ��� ����������

                                                break;
					}
				case 6:  // ����
					{
						// ���� ������� �������� �� ����� �� ��� ��� ������ �������, �������� ����� �� ������� � ��� ����� ��������� ����
						//---
						// ���������� ����� �� ������� �� ����� �������� � ������ ��� �������������
                                                MakingRotation(FR1, PZDN, (*mthk).P_geo);
						//---

						// � ��������� Fill_LinearMotionFragm ���� ������� ��������� ���������� �������� �����
						FR1.End_Point=(*mthk).P_geo;

						Fill_LinearMotionFragm(PZDN,FR1);
						PZDN.MarshFragm.push_back(FR1); // ������
						FR1.shift();                    // ���������� �������� ������ ��� ����������

						// ������
						Fill_CirclingFragm(PZDN, FR1);
						PZDN.MarshFragm.push_back(FR1); // ������
						FR1.shift();                    // ���������� �������� ������ ��� ����������

						break;
					}

otherwise: break;
				} //
			} // i
			//******************  �������� �� ���������� ������  ****************
		}

		//**********************  ����� �� ��������� �������  ********************
		//---
		// ���������� ����� �� �� �������� ������� ������� � ������ ��� �������������
                if(PZDN.mode != 1) {
                    MakingRotation(FR1, PZDN, PZDN.Airdrome_pos);
                }
                else {
                    if (PZDN.MarshTochk.size() != 0) {
                        MakingRotation(FR1, PZDN, PZDN.Airdrome_pos);
                    } else {
                        qDebug() << "SDGSGS ";
                    }
                }
		//---

		// ���������� �� ������� ����� �� ��������� �������
		P_rl=ASh::GeoGraph::DAHT(FR1.St_Point, PZDN.Airdrome_pos);

		// �������� delD=60 �� �� ������� ���� �� ����� -> ����� ���������� �� �����. ���� ����������� ��������� ������ �� ���� ��� ���� �����������, ���������
//		double delD=60;
                if(PZDN.mode == 0) {
                    double d1, d2, d3;
                    d1 = get_distance_breaking(PZDN, FR1);
                    d2 = get_distance_descent(PZDN);
                    d3 = get_distance_descent_from_Hmax (PZDN);
		

                    //P_rl.Dal-=delD;
                    //(+����) �������� �������� ������ ����������� ��
                    //���������� � ��������
                    P_rl.setDal(P_rl.Dal() - d1  - d2 - d3);

                    // � ��������� Fill_LinearMotionFragm ���� ������� ��������� ���������� �������� �����
                    FR1.End_Point=ASh::GeoGraph::AKY(FR1.St_Point, P_rl);
                    Fill_LinearMotionFragm(PZDN,FR1);
                    PZDN.MarshFragm.push_back(FR1); // ������
                    FR1.shift();                    // ���������� �������� ������ ��� ����������

                    // ���������� �� �������� �������
                    Fill_BreakingFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1); // ������
                    FR1.shift();                    // ���������� �������� ������ ��� ����������

                    //  //---
                    //  // ����� ����� �� ��������� ������ ��������� ������� ���� �������� ��� ����
                    //  // ���������� ����� �� �� �������� ������� ������� � ������ ��� �������������
                    //  MakingRotation(FR1, PZDN, PZDN.Airdrome_pos);
                    //  //---

                    // ��������
                    Fill_DescentFromMaxFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1);
                    FR1.shift();                   // ���������� �������� ������ ��� ����������


                    // ��������
                    Fill_DescentFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1);
                    FR1.shift();                   // ���������� �������� ������ ��� ����������
                    //  //---

                    //  // �������� �� ���
                    Fill_VPPLandingFragm(PZDN, FR1);
                    PZDN.MarshFragm.push_back(FR1);
                    FR1.shift();
                    //  Fill_VPPLandingFragm(PZDN, FR1);
                    //  prom_masFr.push_back(FR1); // ������ fr1 � masFr
                    //**********************  ����� �� ��������� �������  ********************
                } else {
                    FR1.End_Point=ASh::GeoGraph::AKY(FR1.St_Point, P_rl);
                    Fill_LinearMotionFragm(PZDN,FR1);
                    PZDN.MarshFragm.push_back(FR1); // ������
                    PZDN.T_end = FR1.T_end;

                }
               qDebug() << "������ ������� ���������� " << PZDN.MarshFragm.size();
	}

	//========================================================================
	//  �� ������� ���������� ��������� ������� ��� ��������� ������� �������� ��������� ��� ��������
	//========================================================================
	bool GetPlanesParfromAirBill(const Airbill& PZDN, const double& Tcur, AirParGeo &Plane)
	{
		bool data_existed=false;

		// ������ � ������� ���������� ��������� ������� ���������� -> ����� ������ ��������� �.�. �������� ����� 1��� ��������� ��������� � ��������� 2��� �.�.�.
		std::vector<double> Tmas;
		std::vector<double>::iterator it_t;
		int CutInd=0;

		std::vector<BillFragm>::const_iterator it_mp;
		BillFragm CurFragm;

		// ���������� ���������� ��������� ������� ����������
		for (it_mp=PZDN.MarshFragm.begin();it_mp!=PZDN.MarshFragm.end();++it_mp)
			Tmas.push_back( (*it_mp).T_st );

		// ��������� �������� ����� ���������� ��������� -> ��� ��� ��� ��� �����
		it_mp = PZDN.MarshFragm.begin()+(PZDN.MarshFragm.size()-1);
		Tmas.push_back((*it_mp).T_end);

		// ���� ���� �������� ����� Tcur
		it_t=std::find_if(Tmas.begin(), Tmas.end(), bind2nd(std::greater<double>(), Tcur));

		if ( it_t != Tmas.begin() && it_t!=Tmas.end() )
		{

                        CutInd=std::distance(Tmas.begin(),it_t);

			CutInd--;   // ������ ��������� ���������� �� 1 �� it_t

			CurFragm=PZDN.MarshFragm.at(CutInd);

			switch (CurFragm.FragmType)
			{
			case 0: break;
			case 1: GetPar_VPPRazgon(Tcur, CurFragm, Plane);    data_existed=true;  break; 
			case 2: GetPar_Takeoff(Tcur, CurFragm, Plane);      data_existed=true;  break;
			case 3: GetPar_UpMaxHigh(Tcur, CurFragm, Plane);    data_existed=true;  break;
			case 4: GetPar_Acceleration(Tcur, CurFragm, Plane); data_existed=true;  break;
                        case 5: GetPar_LinearMotion(Tcur, CurFragm, Plane); data_existed=true;  break;
			case 6: GetPar_Rotation(Tcur, CurFragm, Plane);     data_existed=true;  break;
			case 7: GetPar_Circling(Tcur, CurFragm, Plane);     data_existed=true;  break;
			case 8: GetPar_Breaking(Tcur, CurFragm, Plane);     data_existed=true;  break;
			case 9: GetPar_Descent(Tcur, CurFragm, Plane);      data_existed=true;  break;
			case 10: GetPar_VPPLanding(Tcur, CurFragm, Plane);  data_existed=true;  break;
			case 11: GetPar_DescentFromMax(Tcur, CurFragm, Plane);  data_existed=true;  break;
                        case 12: GetPar_FixAcceleration(Tcur, CurFragm, Plane); data_existed=true;  break;
otherwise: return false; break;
			}  
		}

		return data_existed;
	}

	//==============================================================================
	//
	//==============================================================================
	bool SaveData(const std::string& SavePath, const std::vector<Model_Struct::AirParGeo>& BigMas)
	{
		std::ofstream out(SavePath.c_str());

		std::ofstream out2("geo_coord.txt");
		std::ofstream out3("highs.txt");
		std::ofstream out4("speeds.txt");
		std::ofstream out5("type_frags.txt");

		std::string prev_time;

		int cur_type_FR;
		int prev_type_FR;

		if (!out)
			return false;
		out << "-----------------------------------------------------------------------------------------------------------------------------" << std::endl;
		out << "| ID_��������� |  ID_�����  | ������(����) | �������(����) | ������(�)  |����(����)  |��������(��/�)|     �����(��:��:��)    |" << std::endl;
		out << "-----------------------------------------------------------------------------------------------------------------------------" << std::endl;
		std::vector<Model_Struct::AirParGeo>::const_iterator it;
		int i  = 0;

		// ���������� ����� �� �������. �� ����� ���� � ����� �� ������� ���������
		double Tnk = 5;

		prev_type_FR = (BigMas[0]).type_FR;
		prev_time = boost::posix_time::to_simple_string( (BigMas[0]).pl_time.time_of_day()); 
		for (it = BigMas.begin(); it!=BigMas.end(); ++it) {
			
			out << std::fixed << std::setprecision(4) << std::setw(15) << std::left << "| Boeing " <<
			"|" << std::setw(12) << std::left << (*it).id <<
			"|" << std::setw(14) << std::left << (*it).PlaneC.Fi() <<
			"|" << std::setw(15) << std::left << (*it).PlaneC.La() <<
			"|" << std::setw(12) << std::left << (*it).Altitude <<
			"|" << std::setw(12) << std::left << (*it).Course << 
			"|" << std::setw(14) << std::left << (*it).Speed <<
			"|" << std::setw(24) << std::left << boost::posix_time::to_simple_string( (*it).pl_time.time_of_day()) << "|" << std::endl;
		     out << "-----------------------------------------------------------------------------------------------------------------------------" << std::endl;

			 cur_type_FR = (*it).type_FR;
			
			 if((cur_type_FR != prev_type_FR) || (it == BigMas.end() - 1)) {
				 out5 << get_fragment_string(prev_type_FR) << " " << prev_time << " -- " << boost::posix_time::to_simple_string( (*it).pl_time.time_of_day()) << std::endl;
				 prev_time = boost::posix_time::to_simple_string( (*it).pl_time.time_of_day()); 

			 }
			 prev_type_FR = cur_type_FR;
			 out2 << (*it).PlaneC.Fi() << " " << (*it).PlaneC.La() << " " << (*it).Altitude << std::endl; 
			 out3 << i * Tnk << " " << (*it).Altitude << std::endl; 
			 out4 << i * Tnk << " " << (*it).Speed << std::endl; 
             i++;
		}	
		out.close();
		out2.close();
		out3.close();
		out4.close();

		return true;
	}
	//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

}
