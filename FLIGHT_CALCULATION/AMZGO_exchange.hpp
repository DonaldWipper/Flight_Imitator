#pragma once

#ifndef AMZGO_EXCHANGE_HPP
#define AMZGO_EXCHANGE_HPP

#include <vector>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "Geograph.hpp"

#include <QtGui>

namespace Model_Struct
{

        //==============================================================================
        //                       ������� ��������
        //==============================================================================

        struct AirParGeo
        {
                std::string id;                     // ��� ������� ��� ������� �� ������ ���������
                std::string TargetType;           // ��� �������� ��� ������ ���������� ���
                ASh::GeoGraph::GeoCoord PlaneC;     // �������������� ���������� ��������
                double Altitude;                    // ������ -> �
                double Course;                      // ����
                double Speed;                       // ������ �������� -> ��/�
                double Accel;                       // ��������� -> �/�^2
                int type_FR;                        // (+ ����) �������-�� ��� ���������� ��� ���������� ���� ���������
                boost::posix_time::ptime pl_time;   // �����

                // ����������� �� ���������
                AirParGeo()
                {
                        id = std::string("NO_DATA");
                        TargetType = std::string("NO_DATA");
                        //std::string TargetType;
                        PlaneC=ASh::GeoGraph::GeoCoord(0,0);
                        Altitude = 0;
                        Course = 0;
                        type_FR = 0;
                        Speed = 0;
                        Accel = 0;
                        pl_time = boost::posix_time::second_clock::local_time();
                }

                // ����������� �����������
                AirParGeo(const AirParGeo &right)
                {
                        id = right.id;
                        TargetType = right.TargetType;
                        //std::string TargetType;
                        PlaneC = right.PlaneC;
                        Altitude = right.Altitude;
                        Course = right.Course;
                        type_FR = right.type_FR;
                        Speed = right.Speed;
                        Accel = right.Accel;
                        pl_time = right.pl_time;
                }

                // �������� ������������
                AirParGeo& operator=(const AirParGeo& right)
                {
                        id = right.id;
                        TargetType = right.TargetType;
                        //std::string TargetType;
                        PlaneC = right.PlaneC;
                        Altitude = right.Altitude;
                        Course = right.Course;
                        Speed = right.Speed;
                        Accel = right.Accel;
                        type_FR = right.type_FR;
                        pl_time = right.pl_time;
                        return *this;
                }




                friend QDataStream& operator>>(QDataStream& inn,  AirParGeo& src) {

                     QString id_q, TargetType_q;//, time_q;
                     double Fi, La;
                     inn >> id_q;
                     inn >> TargetType_q;
                     inn >> Fi;
                     src.PlaneC.setFi(Fi);
                     inn >> La;
                     src.PlaneC.setLa(La);
                     inn >> src.Altitude;
                     inn >> src.Course;
                     inn >> src.Speed;
                     inn >> src.Accel;

                     src.id = id_q.toStdString();
                     src.TargetType = TargetType_q.toStdString();
                     return inn;
                }



                friend  QDataStream& operator<<(QDataStream& out, const AirParGeo& src)
                {
                     QString id_q = QString::fromStdString(src.id);
                     QString TargetType_q = QString::fromStdString(src.TargetType);

                     out << id_q
                         << TargetType_q
                         << src.PlaneC.Fi()
                         << src.PlaneC.La()
                         << src.Altitude
                         << src.Course
                         << src.Speed
                         << src.Accel;
                         return out;
                     }
        };





        //------------------------------------------------------------------------------------------------------------//

        //(+ Dima 13.08.13) ZONA
        class ZNK_Geometry
        {
           public:
                  uint    number;        // ����� ���
                  double  Dmin;          // ��������� ���������, [��]
                  double  Dmax;          // �������� ���������, [��]
                  double  Azmin;         // ��������� ������, [����]
                  double  Azmax;         // �������� ������, [����]
                  double  Fi_prd;        //     ����� � �������
                  double  La_prd;        // ��������� ��� �������,[����]
                  double  Fi_prm;        //     ����� � �������
                  double  La_prm;        // ��������� ��� �������,[����]

           // ����������� �� ���������
           ZNK_Geometry(void)
           {
            number=0;
            Dmin=-1;
            Dmax=-1;
            Azmin=-1;
            Azmax=-1;
            Fi_prd=0;
            La_prd=0;
            Fi_prm=0;
            La_prm=0;
           }

           // ����������� �����������
           ZNK_Geometry(const ZNK_Geometry &right)
           {
            number=right.number;
            Dmin=right.Dmin;
            Dmax=right.Dmax;
            Azmin=right.Azmin;
            Azmax=right.Azmax;
            Fi_prd=right.Fi_prd;
            La_prd=right.La_prd;
            Fi_prm=right.Fi_prm;
            La_prm=right.La_prm;
           }

           // �������� ������������
           ZNK_Geometry& operator=(const ZNK_Geometry& right)
           {
            number=right.number;
            Dmin=right.Dmin;
            Dmax=right.Dmax;
            Azmin=right.Azmin;
            Azmax=right.Azmax;
            Fi_prd=right.Fi_prd;
            La_prd=right.La_prd;
            Fi_prm=right.Fi_prm;
            La_prm=right.La_prm;

            return *this;
           }

           friend QDataStream& operator>>(QDataStream& inn, ZNK_Geometry& src)
           {
            inn >> src.number
                >> src.Dmin
                >> src.Dmax
                >> src.Azmin
                >> src.Azmax
                >> src.Fi_prd
                >> src.La_prd
                >> src.Fi_prm
                >> src.La_prm;

            return inn;
           }

           friend  QDataStream& operator<<(QDataStream& out, const ZNK_Geometry& src)
           {
            out << src.number
                << src.Dmin
                << src.Dmax
                << src.Azmin
                << src.Azmax
                << src.Fi_prd
                << src.La_prd
                << src.Fi_prm
                << src.La_prm;

            return out;
           }
        };

        //--------------------------------------------------------------------------------------------//
        //--------------------------------------------------------------------------------------------//


        struct Time {
            uint Year;   //
            uint Month;  //
            uint Day;    //
            uint msDay;  // ����� ����������� � ������ ���

            boost::posix_time::ptime getPtime() {
                return boost::posix_time::ptime(boost::gregorian::date(Year,Month,Day),    boost::posix_time::time_duration(0,0,0)+boost::posix_time::milliseconds(msDay));
            }

            Time() {
              Year = 1991;
              Month = 4;
              Day = 12;
              msDay = 0;
           }

            bool isZERO() {
                if((Year == 0) &&
                   (Month == 0) &&
                   (Day == 0) &&
                   (msDay == 0)) {
                   return true;
                }
                return false;
            }



            // ����������� �����������
            Time(const Time &right)
            {
                Year = right.Year;
                Month = right.Month;
                Day = right.Day;
                msDay = right.msDay;
            }

            // �������� ������������
            Time& operator=(const Time& right)
            {
               Year = right.Year;
               Month = right.Month;
               Day = right.Day;
               msDay = right.msDay;
               return *this;
            }



            friend QDataStream& operator>>(QDataStream& inn,  Time& src) {

                 inn >> src.Year;
                 inn >> src.Month;
                 inn >> src.Day;
                 inn >> src.msDay;
                 return inn;
            }



            friend  QDataStream& operator<<(QDataStream& out, const Time& src)
            {
                out << src.Year;
                out << src.Month;
                out << src.Day;
                out << src.msDay;
                return out;
            }

        };



        class InpInfo
        {
            public:
                   Time T_inf;
                   uint N_Zones;
                   std::vector<ZNK_Geometry> Zones;

            // ����������� �� ���������
            InpInfo(void) {
                T_inf = Time();
                N_Zones = 0;
                Zones.clear();
            }

            //����������� �����������

            InpInfo(const InpInfo &right) {
                T_inf = right.T_inf;
                N_Zones=right.N_Zones;
                Zones.clear();

                 if (N_Zones!=0)
                     std::copy(right.Zones.begin(),right.Zones.end(),std::back_inserter(Zones));
            }

            // �������� ������������
            InpInfo& operator=(const InpInfo& right) {
                T_inf = right.T_inf;
                N_Zones = right.N_Zones;
                Zones.clear();
                if (N_Zones!=0)
                    std::copy(right.Zones.begin(),right.Zones.end(),std::back_inserter(Zones));
                return *this;
            }

            friend QDataStream& operator>>(QDataStream& inn, InpInfo& src)
            {
                inn >> src.T_inf >> src.N_Zones;
                src.Zones.clear();
            for (uint i=0;i<src.N_Zones;i++)
                 {
                  ZNK_Geometry gm;
                  inn >> gm;
                  src.Zones.push_back(gm);
                 }

             return inn;
            }

            friend QDataStream& operator<<(QDataStream& out, const InpInfo& src)
            {
             out << src.T_inf << src.N_Zones;

             for (std::vector<ZNK_Geometry>::const_iterator it=src.Zones.begin(), it_end=src.Zones.end();it!=it_end;++it)
                  out << (*it);

             return out;
            }
        };

}

#endif
