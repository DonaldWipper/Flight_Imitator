


#include "Geograph.hpp"
//==============================================================================





namespace ASh { namespace GeoGraph {

	//++++++ GeoCoord ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//++++++ GeoCoord ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++ LocationCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//++++++ LocationCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++ DecartCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//++++++ DecartCoord +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	const double Rz=6372.795;            // ������ �����
	const GeoCoord NMP(-70.38, 78.76);   // �������������� ���������� ��������� ������������� ������ North GEOMagnetic Pole

	// ������ �� ��� ��������
	double GetRz()
	{
		return Rz;
	}

	//==============================================================================
	//      ����������� �� �������� �����. ����������� ������ ����� � �������
	//      � ���������� �������������� ��������� ������ ����� �� ����������
	//==============================================================================
	GeoCoord AKY(const GeoCoord& P_FiLa, const LocationCoord& P_DalAz)
	{
		int zn;
		double L1, sinF, cosdL;
		double Fi1,La1;
		double Fi2,La2;
		double Az, Dal;

		//------------------------------------------------------------------------------
		Fi1=P_FiLa.Fi() * M_PI / 180;
		La1=P_FiLa.La() * M_PI / 180;
		Dal=P_DalAz.Dal();

		if (P_DalAz.Az()>360)
			Az=(P_DalAz.Az()-360)*M_PI/180;
		else if (P_DalAz.Az()<0)
			Az=(P_DalAz.Az()+360)*M_PI/180;
		else
			Az=P_DalAz.Az()*M_PI/180;
		//------------------------------------------------------------------------------
		sinF=sin(Fi1)*cos(Dal/Rz)+cos(Fi1)*sin(Dal/Rz)*cos(Az);

		if (sinF>0)
			zn=1;
		else if (sinF<0)
			zn=-1;
		else
			zn=0;

		if (1-pow(sinF,2.)>1.e-10)
			Fi2=atan(sinF/sqrt(1-pow(sinF,2.)));
		else
			Fi2=M_PI_2*zn;

		//------------------------------------------------------------------------------
		cosdL=(cos(Dal/Rz)-sin(Fi1)*sin(Fi2))/(cos(Fi1)*cos(Fi2));

		if (cosdL>0)
			zn=1;
		else if (cosdL<0)
			zn=-1;
		else
			zn=0;

		if ((Az>=0) && (Az<=M_PI))
		{
			if (1-pow(cosdL,2.)>1.e-10)
				L1=La1+M_PI_2-atan(cosdL/sqrt(1-pow(cosdL,2.)));
			else
				L1=La1+M_PI_2-M_PI_2*zn;
		}

		if ((Az>M_PI) && (Az<=2*M_PI))
		{
			if (1-pow(cosdL,2.)>1.e-10)
				L1=La1+3*M_PI_2+atan(cosdL/sqrt(1-pow(cosdL,2.)));
			else
				L1=La1+3*M_PI_2+M_PI_2*zn;
		}

		if ((L1>-M_PI) && (L1<M_PI))
			La2=L1;
		else if (L1<-M_PI)
			La2=L1+2*M_PI;
		else
			La2=L1-2*M_PI;
		//------------------------------------------------------------------------------
                Fi2 *= (180./M_PI);
                La2 *= (180./M_PI);

		return ASh::GeoGraph::GeoCoord(La2,Fi2);
	}

	//==============================================================================
	//           ������ �� �������������� ����������� ���� �����
	//              ���������� ����� ���� � ������� �� �����������
	//==============================================================================
	LocationCoord DAHT(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2)
	{
		int zB1,zB2;
		double dLa,G1,A3,cosD,cosAz;
		double Fi1, La1, Fi2, La2;
		double Dal, Az;
		//------------------------------------------------------------------------------
		Fi1=P_FiLa1.Fi()*M_PI/180;
		La1=P_FiLa1.La()*M_PI/180;

		Fi2=P_FiLa2.Fi()*M_PI/180;
		La2=P_FiLa2.La()*M_PI/180;
		//------------------------------------------------------------------------------
		G1=La2-La1;

		if (G1>=0)
			dLa=G1;
		else
			dLa=2*M_PI+G1;

		cosD=sin(Fi1)*sin(Fi2)+cos(Fi1)*cos(Fi2)*cos(dLa);

		if (cosD>0)
			zB1=1;
		else if (cosD<0)
			zB1=-1;
		else
			zB1=0;

		if (1-pow(cosD,2.)>1.e-10)
			Dal=Rz*(M_PI_2-atan(cosD/sqrt(1-pow(cosD,2))));
		else
			Dal=Rz*(M_PI_2-M_PI_2*zB1);
		//------------------------------------------------------------------------------
		if (Dal<=0.01)
			cosAz=0;
		else
			cosAz=(sin(Fi2)-sin(Fi1)*cos(Dal/Rz))/(cos(Fi1)*sin(Dal/Rz));

		if (cosAz>0)
			zB2=1;
		else if (cosAz<0)
			zB2=-1;
		else
			zB2=0;

		if (1-pow(cosAz,2.)>1.e-10)
			A3=M_PI_2-atan(cosAz/sqrt(1-pow(cosAz,2.)));
		else
			A3=M_PI_2-M_PI_2*zB2;

		if (dLa<=M_PI)
			Az=A3*180./M_PI;
		else
			Az=(2*M_PI-A3)*180./M_PI;

		return ASh::GeoGraph::LocationCoord(Dal,Az);
	}

	//==============================================================================
	//           ������ �� �������������� ����������� ���� �����
	//                ���������� ����� ���� �� �����������
	//==============================================================================
	double CalcDistance(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2)
	{
		int zB1;
		double dLa,G1,cosD;
		double Fi1, La1, Fi2, La2;
		double Dal;
		//------------------------------------------------------------------------------
		Fi1=P_FiLa1.Fi()*M_PI/180;
		La1=P_FiLa1.La()*M_PI/180;

		Fi2=P_FiLa2.Fi()*M_PI/180;
		La2=P_FiLa2.La()*M_PI/180;
		//------------------------------------------------------------------------------

		G1=La2-La1;

		if (G1>=0)
			dLa=G1;
		else
			dLa=2*M_PI+G1;

		cosD=sin(Fi1)*sin(Fi2)+cos(Fi1)*cos(Fi2)*cos(dLa);

		if (cosD>0)
			zB1=1;
		else if (cosD<0)
			zB1=-1;
		else
			zB1=0;

		if (1-pow(cosD,2.)>1.e-10)
			Dal=Rz*(M_PI_2-atan(cosD/sqrt(1-pow(cosD,2))));
		else
			Dal=Rz*(M_PI_2-M_PI_2*zB1);

		return Dal;
	}

	//==============================================================================
	//           ������ �� �������������� ����������� ���� �����
	//               ������� �� ������ �� ������ �� �����������
	//==============================================================================
	double CalcAzimuth(const GeoCoord& P_FiLa1, const GeoCoord& P_FiLa2)
	{
		int zB1,zB2;
		double dLa,G1,A3,cosD,cosAz;
		double Fi1, La1, Fi2, La2;
		double Dal, Az;
		//------------------------------------------------------------------------------
		Fi1=P_FiLa1.Fi()*M_PI/180;
		La1=P_FiLa1.La()*M_PI/180;

		Fi2=P_FiLa2.Fi()*M_PI/180;
		La2=P_FiLa2.La()*M_PI/180;
		//------------------------------------------------------------------------------
		G1=La2-La1;

		if (G1>=0)
			dLa=G1;
		else
			dLa=2*M_PI+G1;

		cosD=sin(Fi1)*sin(Fi2)+cos(Fi1)*cos(Fi2)*cos(dLa);

		if (cosD>0)
			zB1=1;
		else if (cosD<0)
			zB1=-1;
		else
			zB1=0;

		if (1-pow(cosD,2.)>1.e-10)
			Dal=Rz*(M_PI_2-atan(cosD/sqrt(1-pow(cosD,2))));
		else
			Dal=Rz*(M_PI_2-M_PI_2*zB1);
		//------------------------------------------------------------------------------
		if (Dal<=0.01)
			cosAz=0;
		else
			cosAz=(sin(Fi2)-sin(Fi1)*cos(Dal/Rz))/(cos(Fi1)*sin(Dal/Rz));

		if (cosAz>0)
			zB2=1;
		else if (cosAz<0)
			zB2=-1;
		else
			zB2=0;

		if (1-pow(cosAz,2.)>1.e-10)
			A3=M_PI_2-atan(cosAz/sqrt(1-pow(cosAz,2.)));
		else
			A3=M_PI_2-M_PI_2*zB2;

		if (dLa<=M_PI)
			Az=A3*180./M_PI;
		else
			Az=(2*M_PI-A3)*180./M_PI;

		return Az;
	}

	//==============================================================================
	//              ��������� ������������� ���������� � ��������������
	//==============================================================================
	GeoCoord XYToFiLa(const DecartCoord& P_XY, const GeoCoord& P_station)
	{
		double Fi=P_station.Fi()+(P_XY.Y()/Rz)*(180./M_PI);
		double La=P_station.La()+(P_XY.X()/Rz)*(180./M_PI)/cos(Fi*M_PI/180.);

		return ASh::GeoGraph::GeoCoord(La,Fi);
	}

	//==============================================================================
	//             ��������� �������������� ���������� � �������������
	//==============================================================================
	DecartCoord FiLaToXY(const GeoCoord& P_FiLa, const GeoCoord& P_station)
	{
		double X=(P_FiLa.La()-P_station.La())*Rz*(M_PI/180.)*cos(P_FiLa.Fi()*M_PI/180.);
		double Y=(P_FiLa.Fi()-P_station.Fi())*Rz*(M_PI/180.);

		return ASh::GeoGraph::DecartCoord(X,Y);
	}

	//========================================================================
	//  ������������ ������������ ���������� �� �������������� �����������
	//========================================================================
	GeoCoord GeoGraphToGeoMag(const GeoCoord& P_FiLa)
	{
		double M_Fi=0, M_La=0;
		double G_Fi=0, G_La=0;

		G_Fi=P_FiLa.Fi();
		G_La=P_FiLa.La();
		//-----------------------
		double NSP_Fi=90.-NMP.Fi();   // ������ ��������� ������������� ������ -> (90-(Fi~80))
		double NSP_La=NMP.La();       // ������� ��������� ������������� ������
		//-----------------------

		double sinB=cos(NSP_Fi*M_PI/180.)*sin(G_Fi*M_PI/180.)+sin(NSP_Fi*M_PI/180.)*cos(G_Fi*M_PI/180.)*cos((G_La-NSP_La)*M_PI/180.);

		if (std::abs(sinB)>1)
			sinB=ASh::Proc::GetSign(sinB);

		M_Fi=asin(sinB);            // ������������ ������

		//
		double cosL=(-sin(NSP_Fi*M_PI/180.)*sin(G_Fi*M_PI/180.)+cos(NSP_Fi*M_PI/180.)*cos(G_Fi*M_PI/180.)*cos((G_La-NSP_La)*M_PI/180.))/cos(M_Fi);

		if (std::abs(cosL)>1)
			cosL=ASh::Proc::GetSign(cosL);

		M_La=acos(cosL);        // ������������ �������

		double SLM=(cos(G_Fi*M_PI/180.)*sin((G_La-NSP_La)*M_PI/180.))/cos(M_Fi);
		if (SLM<0)
			M_La=2.*M_PI-M_La;

		//-----------------------
		M_Fi*=180./M_PI;
		M_La*=180./M_PI;

		return ASh::GeoGraph::GeoCoord(M_La, M_Fi);
	}

	//========================================================================
	//   ������������ �������������� ���������� �� ������������ �����������
	//========================================================================
	GeoCoord GeoMagToGeoGraph(const GeoCoord& P_FiLa)
	{
		double G_Fi=0, G_La=0;
		double M_Fi=0, M_La=0;

		M_Fi=P_FiLa.Fi();
		M_La=P_FiLa.La();
		//-----------------------

		double NSP_Fi=90.-NMP.Fi();  // ������ ��������� ������������� ������ -> (90-(Fi~80))
		double NSP_La=NMP.La();      // ������� ��������� ������������� ������
		//-----------------------

		double sinB=cos(NSP_Fi*M_PI/180.)*sin(M_Fi*M_PI/180.)-sin(NSP_Fi*M_PI/180.)*cos(M_Fi*M_PI/180.)*cos(M_La*M_PI/180.);

		if (std::abs(sinB)>1)
			sinB=ASh::Proc::GetSign(sinB);

		G_Fi=asin(sinB);                // �������������� ������

		//
		double cosG=(sin(NSP_Fi*M_PI/180.)*sin(M_Fi*M_PI/180.)+cos(NSP_Fi*M_PI/180.)*cos(M_Fi*M_PI/180.)*cos(M_La*M_PI/180.))/cos(G_Fi);

		if (std::abs(cosG)>1)
			cosG=ASh::Proc::GetSign(cosG);

		G_La=acos(cosG);              // �������������� �������

		double SLG=(cos(M_Fi*M_PI/180.)*sin(M_La*M_PI/180.))/cos(G_Fi);
		if (SLG<0)
			G_La=2.*M_PI-G_La;

		//-------------------
		G_Fi*=180./M_PI;

		G_La*=180./M_PI;
		G_La+=NSP_La;

		//  if (G_La<0)        // �������: �� 0 �� 360 �� ������
		//	    G_La+=360.0;

		if (G_La<-180)      // �������: �� -180 �� 180
			G_La+=2*M_PI;
		else if(G_La>180)
			G_La-=2*M_PI; // <- ? ���������

		return ASh::GeoGraph::GeoCoord(G_La, G_Fi);
	}
}}
